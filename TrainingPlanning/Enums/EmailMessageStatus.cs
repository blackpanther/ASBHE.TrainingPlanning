﻿using System.ComponentModel.DataAnnotations;

namespace AMWD.ASBHE.TrainingPlanning.Enums
{
	public enum EmailMessageStatus : byte
	{
		/// <summary>
		/// E-mail is saved and prepared to be sent.
		/// </summary>
		[Display(Name = "ausstehend")]
		Pending = 1,

		/// <summary>
		/// E-mail has been sent successfully and will be deleted due to privacy.
		/// </summary>
		[Display(Name = "versendet")]
		Sent,

		/// <summary>
		/// An error occurred while sending.
		/// </summary>
		[Display(Name = "fehlerhaft")]
		Error,

		/// <summary>
		/// E-mail has been blocked by FakeFilter API.
		/// </summary>
		[Display(Name = "blockiert")]
		Blocked
	}
}
