﻿using System;

namespace AMWD.ASBHE.TrainingPlanning.Enums
{
	[Flags]
	public enum ParticipantTrainingStatus : byte
	{
		/// <summary>
		/// Registration confirmed by user (person with sign-in).
		/// </summary>
		Confirmed = 1,

		/// <summary>
		/// The participant has received a canellation.
		/// </summary>
		Cancelled = 2,

		/// <summary>
		/// A hotel is booked.
		/// </summary>
		Booked = 4,

		// TODO
	}
}
