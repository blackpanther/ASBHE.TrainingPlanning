﻿using System.ComponentModel.DataAnnotations;

namespace AMWD.ASBHE.TrainingPlanning.Enums
{
	public enum HotelRequirement
	{
		[Display(Name = "Nein")]
		None = 1,

		[Display(Name = "Ja, zu Veranstaltungsbeginn")]
		OnFirstDay = 2,

		[Display(Name = "Ja, mit Anreise am Vorabend")]
		OnEveningBefore = 3,
	}
}
