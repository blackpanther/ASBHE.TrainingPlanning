﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AMWD.ASBHE.TrainingPlanning.Enums
{
	public enum FoodSelection
	{
		[Description("vegetarisch")]
		[Display(Name = "vegetarisch")]
		Vegetarian = 1,

		[Description("vegan")]
		[Display(Name = "vegan")]
		Vegan = 2,

		[Description("halal")]
		[Display(Name = "halāl")]
		Halal = 3,

		[Description("koscher")]
		[Display(Name = "koscher")]
		Kosher = 4,
	}
}
