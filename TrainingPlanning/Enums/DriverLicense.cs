﻿namespace AMWD.ASBHE.TrainingPlanning.Enums
{
	public enum DriverLicense : int
	{
		AM = 1,
		A1 = 2,
		A2 = 3,
		A = 4,

		B1 = 11,
		B = 12,
		BE = 13,

		C1 = 21,
		C = 22,
		C1E = 23,
		CE = 24,

		D1 = 31,
		D = 32,
		D1E = 33,
		DE = 34,

		L = 41,
		T = 42,

		FW47 = 51,
		FW75 = 52,
		FW47E = 53,
		FW75E = 54,
	}
}
