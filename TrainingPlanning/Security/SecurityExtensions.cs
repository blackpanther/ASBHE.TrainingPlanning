﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace AMWD.ASBHE.TrainingPlanning.Security
{
	public static class SecurityExtensions
	{
		public static ClaimsPrincipal GetPrincipal(this User user)
		{
			var claims = new[]
			{
				new Claim(nameof(User.Id), user.Id.ToString()),
				new Claim(nameof(User.EmailAddress), user.EmailAddress),
				new Claim(nameof(User.FirstName), user.FirstName ?? ""),
				new Claim(nameof(User.LastName), user.LastName ?? ""),
				new Claim(nameof(User.DepartmentId), user.DepartmentId?.ToString() ?? ""),
				new Claim(nameof(User.IsAdmin), user.IsAdmin.ToString()),
				new Claim(nameof(User.ImpersonatingId), user.ImpersonatingId?.ToString() ?? ""),
			};

			var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
			return new ClaimsPrincipal(identity);
		}

		public static User GetUser(this ClaimsPrincipal principal, ServerDbContext dbContext = null)
		{
			if (!int.TryParse(principal.FindFirstValue(nameof(User.Id)), out int id))
				return null;

			bool isImpersonated = int.TryParse(principal.FindFirstValue(nameof(User.ImpersonatingId)), out int impersonatingId);

			if (dbContext == null)
			{
				int? deparmentId = null;
				if (int.TryParse(principal.FindFirstValue(nameof(User.DepartmentId)), out int deptId))
					deparmentId = deptId;

				return new User
				{
					Id = id,
					EmailAddress = principal.FindFirstValue(nameof(User.EmailAddress)),
					FirstName = principal.FindFirstValue(nameof(User.FirstName)),
					LastName = principal.FindFirstValue(nameof(User.LastName)),
					DepartmentId = deparmentId,
					IsAdmin = bool.Parse(principal.FindFirstValue(nameof(User.IsAdmin))),
					ImpersonatingId = isImpersonated ? impersonatingId : null,
				};
			}
			else
			{
				if (isImpersonated &&
					!dbContext.Users.Where(u => u.IsEnabled).Where(u => u.IsAdmin).Where(u => u.Id == impersonatingId).Any())
					return null;

				var user = dbContext.Users
					.Where(u => u.Id == id)
					.Where(u => u.IsEnabled || isImpersonated)
					.FirstOrDefault();

				if (user != null)
					user.ImpersonatingId = isImpersonated ? impersonatingId : null;

				return user;
			}
		}

		public static User GetAuthUser(this HttpContext httpContext, ServerDbContext dbContext = null)
		{
			if (httpContext.User.Identity.IsAuthenticated)
				return httpContext.User.GetUser(dbContext);

			if (dbContext == null)
				return null;

			string authHeader = httpContext.Request.Headers["Authorization"].ToString();
			if (string.IsNullOrWhiteSpace(authHeader) || !authHeader.StartsWith("Basic", StringComparison.OrdinalIgnoreCase))
				return null;

			authHeader = authHeader.Replace("Basic", "", StringComparison.OrdinalIgnoreCase).Trim();
			string plain = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));

			string[] parts = plain.Split(':', StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length != 2)
				return null;

			string emailAddress = parts.First().Trim().ToLower();
			string password = parts.Last().Trim();

			var user = dbContext.Users
				.Where(u => u.IsEnabled)
				.Where(u => u.EmailAddress == emailAddress)
				.FirstOrDefault();
			if (user == null)
				return null;

			if (!PasswordHelper.VerifyPassword(password, user.PasswordHash, out bool rehashNeeded))
				return null;

			if (rehashNeeded)
			{
				try
				{
					user.PasswordHash = PasswordHelper.HashPassword(password);
					dbContext.SaveChanges();
				}
				catch
				{ }
			}

			user.IsBasicAuth = true;
			return user;
		}
	}
}
