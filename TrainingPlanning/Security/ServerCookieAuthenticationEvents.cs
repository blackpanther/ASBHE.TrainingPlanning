﻿using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace AMWD.ASBHE.TrainingPlanning.Security
{
	public class ServerCookieAuthenticationEvents : CookieAuthenticationEvents
	{
		private readonly ServerDbContext dbContext;

		public ServerCookieAuthenticationEvents(ServerDbContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public override Task ValidatePrincipal(CookieValidatePrincipalContext context)
		{
			var httpUser = context.Principal.GetUser();
			if (httpUser == null)
			{
				context.RejectPrincipal();
				return context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			}

			var dbUser = context.Principal.GetUser(dbContext);
			if (dbUser == null)
			{
				context.RejectPrincipal();
				return context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			}

			if (!httpUser.Equals(dbUser))
			{
				context.ReplacePrincipal(dbUser.GetPrincipal());
				context.ShouldRenew = true;
			}

			return Task.CompletedTask;
		}
	}
}
