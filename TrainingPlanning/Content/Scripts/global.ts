﻿class TP {
	public static Url: string;
	public static Path: string;

	public static Token: AntiforgeryToken = new AntiforgeryToken();

	public static SummernoteFonts: string[] = [
		'Noto Sans', 'Helvetica Neue', 'Arial', 'Segoe UI',
		'Consolas', 'Menlo', 'Monaco', 'Courier New',
		'Noto Serif', 'Times New Roman',
	];

	/**
	 * Initializes all global event bindings.
	 */
	public static Initialize(): void {
		TP.InitSidebar();

		TP.InitBlankLinks();
		TP.InitTogglePassword();
		TP.InitRandomPassword();
		TP.InitTooltip();
		TP.InitPopover();
		TP.InitMultiCheck();

		TP.CheckWebp();
	}

	/**
	 * Calculates the font color from a given background color to match a certain contrast level (luminance).
	 *
	 * @param backgroundColor The background color as defined on CSS.
	 * @param useAlpha A value indicating whether to use the alpha channel also (Default: `false`).
	 * @returns An rgb(a) color to use on CSS.
	 */
	public static GetFontColor(backgroundColor: string, useAlpha: boolean = false): string {
		let canvas = document.createElement('canvas');
		canvas.height = 1;
		canvas.width = 1;

		let context = canvas.getContext('2d');
		context.fillStyle = backgroundColor;
		context.fillRect(0, 0, 1, 1);

		let result = context.getImageData(0, 0, 1, 1).data;
		const r: number = result[0];
		const g: number = result[1];
		const b: number = result[2];
		let a: number = result[3];

		if (!useAlpha && a === 0)
			return 'rgb(0, 0, 0)';

		if (!useAlpha)
			a = 1;

		if (a > 1)
			a /= 255;

		const luminance = (r * 0.299 + g * 0.587 + b * 0.114) / 255;
		if (luminance <= 0.5)
			return `rgba(255, 255, 255, ${a})`;

		return `rgba(0, 0, 0, ${a})`;
	}

	/**
	 * Generates a random string using the given parameters.
	 *
	 * @see https://stackoverflow.com/a/12635919
	 *
	 * @param length The length of the random string.
	 * @param numLowerChars The number of lower characters required.
	 * @param numUpperChars The number of upper characters required.
	 * @param numDigitChars The number of digits required.
	 * @param numSpecialChars The number of special characters required.
	 * @returns A random string.
	 */
	public static GenerateRandomString(length: number, numLowerChars: number = 1, numUpperChars: number = 1, numDigitChars: number = 1, numSpecialChars: number = 1): string {
		const numRandom = length - (numLowerChars + numUpperChars + numDigitChars + numSpecialChars);
		if (numRandom < 0)
			throw Error('The `length` is too short to match all criteria');

		const lowerChars = 'abcdefghijklmnopqrstuvwxyz';
		const upperChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		const digitChars = '0123456789';
		const specialChars = '!@#$%^&*()_+{}:<>?\|[];,./~';

		const allChars = `${lowerChars}${upperChars}${digitChars}${specialChars}`;

		let str: string = '';
		str += TP.Pick(lowerChars, numLowerChars);
		str += TP.Pick(upperChars, numUpperChars);
		str += TP.Pick(digitChars, numDigitChars);
		str += TP.Pick(specialChars, numSpecialChars);
		str += TP.Pick(allChars, numRandom);

		return TP.Shuffle(str);
	}

	/**
	 * Updates the text of an existing Tooltip.
	 *
	 * @param element The jQuery element.
	 * @param text The new text.
	 */
	public static UpdateTooltip(element: JQuery<HTMLElement>, text: string): void {
		let tooltip = element.data('tooltip') as bootstrap.Tooltip;
		if (tooltip === null)
			return;

		tooltip.setContent({ '.tooltip-inner': text });
	}

	/**
	 * Indicates whether a specified string is null, empty or consists only of white-space characters.
	 *
	 * @param str The string to test.
	 * @returns true when a case matches, otherwise false.
	 */
	public static IsNullOrWhitespace(str: string): boolean {
		return str === null || str.length === 0 || str.match(/^\s+$/) !== null;
	}

	/**
	 * Returns the values of a enum.
	 *
	 * @param obj The enum.
	 * @returns A list of enum values.
	 */
	public static EnumGetValues<O extends object, K extends keyof O = keyof O>(obj: O): K[] {
		return Object.keys(obj).filter(k => Number.isNaN(+k)) as K[];
	}

	private static InitSidebar(): void {
		let hide = (event: JQuery.ClickEvent) => {
			event.preventDefault();

			$('.sidebar').removeClass('active');
			$('.main-content').off('click', hide);
		};

		$('.navbar-toggler').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();

			$('.sidebar').addClass('active');
			$('.main-content').on('click', hide);
		});
	}

	private static InitBlankLinks(): void {
		$(document).on('click', 'a', (event: JQuery.ClickEvent) => {
			const isBlank = $(event.currentTarget).hasClass('blank');
			let href = $(event.currentTarget).attr('href');
			if (href)
				href = href.trim();

			if (href === '#' || href === '' || isBlank)
				event.preventDefault();

			if (href !== '#' && href !== '' && isBlank) {
				let site = window.open(href);
				site.focus();
			}
		});
	}

	private static InitTogglePassword(): void {
		$(document).on('click', '.toggle-password', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let self = $(event.currentTarget);
			let input = self.closest('.input-group').find('input').first();

			if (self.is(':disabled') || input.is(':disabled'))
				return;

			if (input.attr('type') === 'password') {
				input.attr('type', 'text');
				self.html('<span class="fa-solid fa-fw fa-eye-slash"></span>');
			}
			else {
				input.attr('type', 'password');
				self.html('<span class="fa-solid fa-fw fa-eye"></span>');
			}
		});
	}

	private static InitRandomPassword(): void {
		$(document).on('click', '.random-password', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let self = $(event.currentTarget);
			let input = self.closest('.input-group').find('input').first();

			if (self.is(':disabled') || input.is(':disabled'))
				return;

			const password = TP.GenerateRandomString(14, 3, 3, 2, 1);
			input.val(password);

			input.trigger('input');
			input.trigger('change');
		});
	}

	private static InitTooltip(): void {
		$('.tp-tooltip').each((_, element) => {
			const tooltip = new bootstrap.Tooltip(element);
			$(element).data('tooltip', tooltip);
		});
	}

	private static InitPopover(): void {
		$('.tp-popover').each((_, element) => {
			const jqElement = $(element);

			jqElement.on('click', (event: JQuery.ClickEvent) => {
				event.preventDefault();

				const target = jqElement.data('target');
				const tpClass = jqElement.data('tp-class');
				let config: Partial<bootstrap.Popover.Options> = {};

				if (target) {
					if (jqElement.data('bs-html') as boolean) {
						config.html = true;
						config.title = $(target).find('.tp-popover-title').html() || null;
						config.content = $(target).find('.tp-popover-content').html() || null;
					}
					else {
						config.html = false;
						config.title = $(target).find('.tp-popover-title').text() || null;
						config.content = $(target).find('.tp-popover-content').text() || null;
					}
				}

				if (tpClass)
					config.customClass = tpClass;

				const popover = bootstrap.Popover.getOrCreateInstance(element, config);
				popover.toggle();

				console.log(popover);
			});
		});
	}

	private static InitMultiCheck(): void {
		$(document).on('change', '.check-all-parent', (event: JQuery.ChangeEvent) => {
			event.preventDefault();
			const table = $(event.currentTarget).closest('table') as JQuery<HTMLElement>;

			const parent = table.find('.check-all-parent');
			const children = table.find('.check-all-child');
			const buttons = table.find('.check-all-button');

			if (parent.is(':checked')) {
				children.each((_, el) => {
					let jqEl = $(el);
					if (!jqEl.prop('disabled') && !jqEl.hasClass('disabled'))
						$(el).prop('checked', true);
				});
				buttons.each((_, el) => { $(el).removeClass('disabled').prop('disabled', false); });
			}
			else {
				children.each((_, el) => {
					let jqEl = $(el);
					if (!jqEl.prop('disabled') && !jqEl.hasClass('disabled'))
						$(el).prop('checked', false);
				});
				buttons.each((_, el) => { $(el).addClass('disabled').prop('disabled', true); });
			}
		});

		$(document).on('change', '.check-all-child', (event: JQuery.ChangeEvent) => {
			event.preventDefault();
			const table = $(event.currentTarget).closest('table') as JQuery<HTMLElement>;

			const parent = table.find('.check-all-parent');
			const children = table.find('.check-all-child');
			const buttons = table.find('.check-all-button');

			let allChecked = true;
			let someChecked = false;

			children.each((_, el) => {
				if ($(el).is(':checked')) {
					someChecked = true;
				}
				else {
					allChecked = false;
				}
			});

			if (allChecked) {
				parent.prop('checked', true);
				parent.prop('indeterminate', false);
				buttons.each((_, el) => { $(el).removeClass('disabled').prop('disabled', false); });
			}
			else if (someChecked) {
				parent.prop('checked', false);
				parent.prop('indeterminate', true);
				buttons.each((_, el) => { $(el).removeClass('disabled').prop('disabled', false); });
			}
			else {
				parent.prop('checked', false);
				parent.prop('indeterminate', false);
				buttons.each((_, el) => { $(el).addClass('disabled').prop('disabled', true); });
			}
		});
	}

	// License: MIT (2021-05-01)
	// Found on https://github.com/Modernizr/Modernizr/blob/6d56d814b9682843313b16060adb25a58d83a317/feature-detects/img/webp.js
	private static CheckWebp(): void {
		let image = new Image();
		image.onerror = _ => {
			$('html').addClass('no-webp');
		};
		image.onload = _ => {
			$('html').addClass('webp');
		};
		image.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
	}

	private static Pick(str: string, count: number): string {
		let result: string = '';

		for (let i = 0; i < count; i++)
			result += str.charAt(Math.floor(Math.random() * str.length));

		return result;
	}

	private static Shuffle(str: string): string {
		if (str === undefined || str === null || str.length === 0)
			return str;

		let array: string[] = str.split('');
		let len = str.length;

		while (len-- > 0) {
			const idx = Math.floor(Math.random() * (len + 1));
			const tmp = array[idx];
			array[idx] = array[len];
			array[len] = tmp;
		}

		return array.join('');
	}
}

TP.Initialize();
