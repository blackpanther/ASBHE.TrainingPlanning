﻿enum EmailPriority {
	Low = 0,
	Normal = 1,
	High = 2
}

enum EmailStatus {
	Pending = 1,
	Sent = 2,
	Error = 3,
	Blocked = 4
}

enum FoodSelection {
	Vegetarian = 1,
	Vegan = 2,
	Halal = 3,
	Kosher = 4
}

enum HotelRequirement {
	None = 1,
	OnFirstDay = 2,
	OnEveningBefore = 3
}
