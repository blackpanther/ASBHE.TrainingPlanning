﻿/// <reference path="../../node_modules/@microsoft/signalr/dist/esm/index.d.ts" />

class WsRetryPolicy implements signalR.IRetryPolicy {
	nextRetryDelayInMilliseconds(retryContext: signalR.RetryContext): number | null {
		const waitSeconds = Math.min(2 * retryContext.previousRetryCount, 60);
		return waitSeconds * 1000;
	}
}

class WS {
	private static _webSocket: signalR.HubConnection = null;
	private static _indicator: JQuery;
	private static _pingTimerId: number;
	private static _logLevel: signalR.LogLevel;

	public static Initialize(): void {
		WS._logLevel = signalR.LogLevel.None;

		WS._webSocket = new signalR.HubConnectionBuilder()
			.withUrl(`${TP.Path}/ws/web`)
			.configureLogging(WS._logLevel)
			.withAutomaticReconnect(new WsRetryPolicy())
			.build();

		WS._indicator = $('#websocket-connection-state');

		WS.BindEvents();
		WS.StartConnection();

		WS.RegisterAwake();
	}

	public static RegisterEvent(eventName: string, callback: (...args: any[]) => void): void {
		WS._webSocket.on(eventName, callback);

		if (WS._logLevel <= signalR.LogLevel.Debug)
			console.debug('RegisterEvent', { eventName: eventName, callback: callback });
	}

	public static UnRegisterEvent(eventName: string): void {
		WS._webSocket.off(eventName);

		if (WS._logLevel <= signalR.LogLevel.Debug)
			console.debug('UnRegisterEvent', { eventName: eventName });
	}

	public static SendAsync(remoteMethodName: string, ...args: any[]): Promise<void> {
		if (WS._webSocket === null || WS._webSocket.state !== signalR.HubConnectionState.Connected)
			throw Error('WebSocket not connected');

		if (WS._logLevel <= signalR.LogLevel.Debug)
			console.debug('SendAsync', { remoteMethodName: remoteMethodName, args: args });

		return WS._webSocket.send(remoteMethodName, args);
	}

	public static InvokeAsync<T = any>(remoteMethodName: string, ...args: any[]): Promise<T> {
		if (WS._webSocket === null || WS._webSocket.state !== signalR.HubConnectionState.Connected)
			throw Error('WebSocket not connected');

		if (WS._logLevel <= signalR.LogLevel.Debug)
			console.debug('InvokeAsync', { remoteMethodName: remoteMethodName, args: args });

		return WS._webSocket.invoke(remoteMethodName, args);
	}

	private static BindEvents(): void {
		WS._webSocket.onclose((error: Error) => {
			if (error)
				console.error(error);

			clearTimeout(WS._pingTimerId);

			WS.SetStatus('text-danger');
			TP.UpdateTooltip(WS._indicator, 'getrennt');
		});

		WS._webSocket.onreconnecting((error: Error) => {
			if (error)
				console.error(error);

			clearTimeout(WS._pingTimerId);

			WS.SetStatus('text-warning');
			TP.UpdateTooltip(WS._indicator, 'verbinde...');
		});

		WS._webSocket.onreconnected((connectionId: string) => {
			console.info(`Connection ID: ${connectionId}`);

			WS.SetStatus('text-success');
			TP.UpdateTooltip(WS._indicator, 'verbunden');

			WS.CheckLatency();
		});
	}

	private static async StartConnection(): Promise<void> {
		try {
			await WS._webSocket.start();

			WS.SetStatus('text-success');
			TP.UpdateTooltip(WS._indicator, 'verbunden');

			await WS.CheckLatency();
		}
		catch (error: unknown) {
			if (error)
				console.error(error);

			WS.SetStatus('text-danger');
			TP.UpdateTooltip(WS._indicator, 'getrennt');
		}
	}

	private static async CheckLatency(): Promise<void> {
		try {
			clearTimeout(WS._pingTimerId);
			if (WS._webSocket === null || WS._webSocket.state !== signalR.HubConnectionState.Connected)
				return;

			const timerStart: number = Date.now();
			await WS._webSocket.invoke('Ping');
			const timerStop: number = Date.now();

			// calculate latency with one decimal
			const latency = Math.round((timerStop - timerStart) * 10) / 10;

			let originalText = WS._indicator.data('text') as string;
			if (!originalText) {
				if (WS._indicator.data('tooltip')._newContent === null) {
					originalText = WS._indicator.data('tooltip')._config.originalTitle;
				}
				else {
					originalText = WS._indicator.data('tooltip')._newContent['.tooltip-inner'];
				}
				WS._indicator.data('text', originalText);
			}

			TP.UpdateTooltip(WS._indicator, `${originalText} (${latency}ms)`);
		}
		catch (_) {
			// keep things quiet
		}
		finally {
			WS._pingTimerId = setTimeout(WS.CheckLatency, 5000);
		}
	}

	private static SetStatus(cssClass: string): void {
		WS._indicator
			.removeClass('text-success')
			.removeClass('text-warning')
			.removeClass('text-danger')
			.addClass(cssClass)
			.removeData('text');
	}

	private static RegisterAwake(): void {
		let lockResolver: unknown;
		if (navigator && navigator.locks && navigator.locks.request) {
			const promise = new Promise((resolve) => {
				lockResolver = resolve;
			});
			navigator.locks.request('ws_lock', { mode: 'shared' }, _ => {
				return promise;
			});
		}
	}
}

WS.Initialize();
