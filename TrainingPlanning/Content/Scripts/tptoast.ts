﻿class TPToast {
	private static _containerTemplate: string = `<div class="toast-container position-fixed p-3 top-0 start-50 translate-middle-x"></div>`;
	private static _toastTemplate: string = `<div class="toast align-items-center border-0" role="alert" aria-live="assertive" aria-atomic="true">
		<div class="d-flex">
			<div class="toast-body">
				<!-- Message Text -->
			</div>
			<button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Schließen"></button>
		</div>
	</div>`;

	private static CreateNode(html: string): Node {
		return new DOMParser().parseFromString(html, "text/html").body.childNodes[0] as HTMLElement;
	}

	private _container: Node;
	private _template: Node;

	constructor() {
		this._container = TPToast.CreateNode(TPToast._containerTemplate);
		this._template = TPToast.CreateNode(TPToast._toastTemplate);

		document.body.appendChild(this._container);
	}

	public Create(htmlText: string, bgClass: string, delayMS: number = 5000): string {
		const node = this._template.cloneNode(true) as HTMLElement;
		node.id = `tp-toast-${Date.now()}`;

		node.dataset.bsAnimation = "true";
		node.dataset.bsDelay = delayMS.toString();
		node.dataset.bsAutohide = (Number.isInteger(delayMS) && delayMS > 0).toString();

		node.classList.add(`text-bg-${bgClass}`);
		node.querySelector('.toast-body').innerHTML = htmlText;

		node.addEventListener('hidden.bs.toast', () => {
			node.remove();
		});

		this._container.appendChild(node);

		const toast = new bootstrap.Toast(node);
		toast.show();

		return node.id;
	}
}
