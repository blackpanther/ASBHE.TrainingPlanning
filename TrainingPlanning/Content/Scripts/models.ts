﻿class AntiforgeryToken {
	public FormName: string;
	public HeaderName: string;
	public Value: string;
}

class Department {
	public id: number;
	public name: string;
	public contact: string;
	public emailAddress: string;
	public phoneNumber: string;
	public street: string;
	public addressAddition: string;
	public zipCode: string;
	public city: string;
	public country: string;
}

class EmailAddress {
	public name: string;
	public address: string;
}

class EmailMessage {
	public id: string;
	public replyTo: EmailAddress;
	public recipients: EmailAddress[];
	public recipientsVisible: boolean;
	public priority: EmailPriority;
	public sendAfterDate: string;
	public subject: string;
	public message: string;
	public isHtml: boolean;
	public status: EmailStatus;
	public updatedAt: string;
}
class Participant {
	public id: number;
	public firstName: string;
	public lastName: string;
	public birthday: string;
	public placeOfBirth: string;
	public department: Department;
	public emailAddress: string;
	public phoneNumber: string;
	public street: string;
	public addressAddition: string;
	public zipCode: string;
	public city: string;
	public country: string;
	public driverLicenseClasses: string;
	public driverPermission: boolean;
	public assistanceRequired: boolean;
	public foodSelection: FoodSelection;
	public allergies: string;
	public isEnabled: boolean;
}

class RegisterViewModel {
	public trainingId: string;

	public firstName: string;
	public lastName: string;
	public emailAddress: string;
	public phone: string;

	public birthday: string;
	public placeOfBirth: string;
	public department: string;

	public street: string;
	public zipCode: string;
	public city: string;
	public addressAddition: string;
	public country: string;

	public driverLicenseClasses: string;
	public driverPermission: boolean;

	public assistanceRequired: boolean;
	public hotelRequirement: number;
	public foodSelection: number | null;
	public allergies: string;

	public assumptionOfCosts: boolean;
	public invoiceName: string;
	public invoiceStreet: string;
	public invoiceZipCode: string;
	public invoiceCity: string;

	public termsAccepted: boolean;
}

class SerilogEntry {
	public id: number;
	public timestampISO: string;
	public level: string;
	public exception: string;
	public message: string;
	public properties: object;
}

class SmtpSetting {
	public host: string;
	public port: number;
	public startTls: boolean;
	public ssl: boolean;
	public validateCertificates: boolean;
	public checkRevocationList: boolean;
	public username: string;
	public sender: string;
	public name: string;
	public subjectTag: string;
	public sendSelfOnBcc: boolean;
}

class TrainingSplit {
	public id: string;
	public trainingId: string;
	public start: string;
	public end: string;
	public name: string;
	public description: string;
}
