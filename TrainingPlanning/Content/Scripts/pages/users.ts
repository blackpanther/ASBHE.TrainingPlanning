﻿class Users {
	private static _editBaseUrl: string;
	private static _tpToast: TPToast;

	public static InitializeIndex(): void {
		Users.InitDelete();
	}

	public static InitializeEdit(baseUrl: string): void {
		Users._editBaseUrl = baseUrl;

		Users._tpToast = new TPToast();

		Users.InitGeneratePassword();
		Users.InitUserDelete();
	}

	//#region Index

	private static InitDelete(): void {
		$('.del-user').on('click', (event: JQuery.ClickEvent) => {
			if (!confirm('Soll der Benutzer gelöscht werden?'))
				event.preventDefault();
		});
	}

	//#endregion Index

	//#region Edit

	private static InitGeneratePassword(): void {
		$('#generate-password').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Soll ein neues Kennwort generiert werden?'))
				return;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', $('#generate-password').data('id') as string);

			const response = await fetch(Users._editBaseUrl, {
				method: 'POST',
				body: formData
			});

			if (response.ok) {
				Users._tpToast.Create('<span class="fa-solid fa-fw fa-check"></span> Ein neues Kennwort ist auf dem Weg', 'success', 10000);
			}
			else {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);

				Users._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Ein neues Kennwort konnte nicht generiert werden', 'danger', 10000);
			}
		});
	}

	private static InitUserDelete(): void {
		$('#delete').on('click', (event: JQuery.ClickEvent) => {
			if (!confirm('Soll der Benutzer gelöscht werden?'))
				event.preventDefault();
		});
	}

	//#endregion Edit
}
