﻿class ParticipantsUrls {
	public deleteMulti: string;
	public get: string;
	public merge: string;
}

class Participants {
	private static _urls: ParticipantsUrls;

	private static _driverClasses: string[];

	public static InitializeIndex(urls: ParticipantsUrls): void {
		Participants._urls = urls;

		Participants.BindDelete();
		Participants.BindMultiDelete();

		Participants.BindMerge();
	}

	public static InitializeEdit(): void {
		Participants.InitializeDelete();
		Participants.InitializeDriverLicense();
	}

	//#region Index

	private static BindDelete(): void {
		$('.delete').on('click', (event: JQuery.ClickEvent) => {
			if (!confirm('Soll der Teilnehmer gelöscht werden?\nEs werden dann auch alle Anmeldungen storniert!'))
				event.preventDefault();
		});
	}

	private static BindMultiDelete(): void {
		$('#multi-delete').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Sollen die Teilnehmer gelöscht werden?\nAlle exisiterenden Anmeldungen werden dann storniert!'))
				return;

			const table = $(event.currentTarget).closest('table');
			const children = table.find('.check-all-child');

			let ids: number[] = [];
			children.each((_, el) => {
				if ($(el).is(':checked')) {
					ids.push($(el).val() as number);
				}
			});

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			ids.forEach(id => formData.append('ids[]', id.toString()));

			const response = await fetch(Participants._urls.deleteMulti, {
				method: 'POST',
				body: formData
			});

			const msg = await response.text();
			if (!response.ok) {
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			location.href = msg;
		});
	}

	private static BindMerge(): void {
		$('#merge').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			let ids: number[] = [];
			$(event.currentTarget).closest('table').find('.check-all-child').each((_, el) => {
				if ($(el).is(':checked')) {
					ids.push($(el).val() as number);
				}
			});

			if (ids.length < 2)
				return;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			ids.forEach(id => formData.append('ids[]', id.toString()));

			const response = await fetch(Participants._urls.get, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			$('#merge-modal').data('ids', ids);

			const participants = await response.json() as Participant[];
			let tableRows = $('#mm-table .tp-template').children().clone(true);
			$('#mm-table .items').append(tableRows);

			tableRows.each((_, el) => {
				let row = $(el);
				let name = row.data('name');

				let first = true;

				for (const participant of participants) {
					let val: any = (participant as any)[name];
					let text: string = null;

					switch (name) {
						case 'birthday':
							const dt = luxon.DateTime.fromISO(val);
							text = dt.toFormat('dd.MM.yyyy');
							break;

						case 'department':
							if (participant.department === null) {
								val = 0;
								text = "";
							}
							else {
								val = participant.department.id;
								text = participant.department.name;
							}
							break;

						case 'driverLicenseClasses':
							text = (val as string)
								.split(',') // split array
								.map(dc => dc.trim()) // clear whitespaces
								.filter(dc => dc !== '') // remove empty entries
								.join(', '); // merge them again in good visual styling
							break;

						case 'isEnabled':
						case 'driverPermission':
						case 'assistanceRequired':
							text = val ? 'Ja' : 'Nein';
							break;

						case 'foodSelection':
							text = (window as any).FoodSelectionMapping[val];
							break;

						default:
							text = val;
							break;
					}

					let input = $('<input/>')
						.addClass('form-check-input')
						.attr('type', 'radio')
						.attr('id', `${name}_${participant.id}`)
						.attr('name', name)
						.prop('checked', first)
						.val(val);
					let label = $('<label/>')
						.addClass('form-check-label')
						.attr('for', `${name}_${participant.id}`)
						.text(text);

					let cell = $('<td/>').appendTo(row);
					cell.append($('<div/>').addClass('form-check').append(input).append(label));

					first = false;
				}
			});

			$('#merge-modal').modal('show');
		});

		$('#mm-submit').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			let participant = new Participant();
			$('#mm-table .items tr').each((_, el) => {
				const name = $(el).data('name');
				const value = $('#mm-table').find(`input[name=${name}]:checked`).val();

				let key = null;
				switch (name) {
					case 'department':
						key = 'departmentId';
						break;

					default:
						key = name;
						break;
				}

				(participant as any)[key] = value;
			});

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('idList', ($('#merge-modal').data('ids') as number[]).join(','))
			formData.append('data', JSON.stringify(participant));

			const response = await fetch(Participants._urls.merge, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			window.location.reload();
		});

		$('#merge-modal').on('hidden.bs.modal', _ => {
			$('#merge-modal').removeData('ids');
			$('#mm-table .items').children().remove();
		});
	}

	//#endregion Index

	//#region Edit

	private static InitializeDelete(): void {
		$('#delete').on('click', (event: JQuery.ClickEvent) => {
			if (!confirm('Soll der Teilnehmer gelöscht werden?\nEs werden dann auch alle Anmeldungen storniert!'))
				event.preventDefault();
		});
	}

	private static InitializeDriverLicense(): void {
		$(() => {
			Participants._driverClasses = ($('#DriverLicenseClasses').val() as string)
				.split(',')
				.map(dc => dc.trim())
				.filter(dc => dc !== '');
			$('#DriverLicenseClasses').val(Participants._driverClasses.sort().join(','));

			$('.dc-check').prop('checked', false);
			Participants._driverClasses.forEach((dc) => {
				$(`.dc-check[value="${dc}"]`).prop('checked', true);
			});

			$('.dc-check').on('click', (event: JQuery.ClickEvent) => {
				const _self = $(event.currentTarget);
				const dc = _self.val() as string;
				if (_self.is(':checked')) {
					Participants._driverClasses.push(dc);
				}
				else {
					Participants._driverClasses = Participants._driverClasses.filter(c => c !== dc);
				}

				$('#DriverLicenseClasses').val(Participants._driverClasses.sort().join(','));
			});
		});
	}

	//#endregion Edit
}
