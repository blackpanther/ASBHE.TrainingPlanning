﻿class EmailBaseUrls {
	public get: string;
	public delete: string;
	public deleteMulti: string;
	public reSend: string;
}

class EmailUpdate {
	public id: string;
	public status: EmailStatus;
	public updatedAt: string;
}

class Emails {
	private static _baseUrls: EmailBaseUrls;
	private static _defaultSender: string;

	private static isEmailsLoading: boolean = false;

	public static Initialize(baseUrls: EmailBaseUrls, defaultSender: string): void {
		Emails._baseUrls = baseUrls;
		Emails._defaultSender = defaultSender;

		Emails.InitializeLoad();
		Emails.InitializeDetails();

		Emails.InitializeDelete();
		Emails.InitializeReSend();

		$('#load-entries').trigger('click');

		WS.RegisterEvent('EmailUpdate', Emails.UpdateStatus);
		WS.RegisterEvent('EmailDeleteMulti', Emails.DeleteMulti);
	}

	private static InitializeLoad(): void {
		$('#load-entries').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (Emails.isEmailsLoading)
				return;

			Emails.isEmailsLoading = true;
			try {
				let loadAction = $('#load-entries');
				const id = (loadAction.data('email-timestamp') as number) || 0;

				const response = await fetch(`${Emails._baseUrls.get}/${id}`);
				if (!response.ok) {
					const msg = await response.text();
					console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
					return;
				}

				const data = await response.json() as EmailMessage[];
				DomHelper.BuildTable('#emails .tp-template', '#emails .items', data, (tr, trData: EmailMessage) => {
					tr.data('id', trData.id);
					tr.data('message', trData);
					tr.find('.check-all-child').val(trData.id);

					let update = new EmailUpdate();
					update.id = trData.id;
					update.status = trData.status;
					update.updatedAt = trData.updatedAt;
					Emails.UpdateStatus(update);

					const timestamp = luxon.DateTime.fromISO(trData.sendAfterDate).toMillis();
					const entryTimestamp = parseInt(loadAction.data('email-timestamp'));

					if (isNaN(entryTimestamp) || timestamp < entryTimestamp)
						loadAction.data('email-timestamp', timestamp);
				});
			}
			catch (error: unknown) {
				console.error('Loading emails failed', error);
			}
			finally {
				Emails.isEmailsLoading = false;
			}
		});
	}

	private static InitializeDetails(): void {
		$('.details').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();

			const row = $(event.currentTarget).closest('tr');
			const message = row.data('message') as EmailMessage;

			let modal = $('#detail-modal');
			DomHelper.BindData(modal, message);

			modal.find('[data-value="status"]').val(Emails.GetStatusText(message.status));
			modal.find('[data-value="status"]').removeClass().addClass('form-control');
			switch (message.status) {
				case EmailStatus.Pending:
					modal.find('[data-value="status"]').addClass('text-warning');
					$('#dm-resend').prop('disabled', true);
					$('#dm-delete').prop('disabled', false);
					break;

				case EmailStatus.Sent:
					modal.find('[data-value="status"]').addClass('text-success');
					$('#dm-resend').prop('disabled', false);
					$('#dm-delete').prop('disabled', true);
					break;

				case EmailStatus.Error:
					modal.find('[data-value="status"]').addClass('text-danger');
					$('#dm-resend').prop('disabled', false);
					$('#dm-delete').prop('disabled', false);
					break;

				case EmailStatus.Blocked:
					modal.find('[data-value="status"]').addClass('text-info');
					$('#dm-resend').prop('disabled', true);
					$('#dm-delete').prop('disabled', true);
					break;
			}

			if (message.replyTo === null) {
				modal.find('[data-value="replyTo"]').val(Emails._defaultSender);
			}
			else {
				modal.find('[data-value="replyTo"]').val(Emails.GetAddressLine(message.replyTo));
			}

			modal.find('[data-value="recipients"]').val(message.recipients.map(r => Emails.GetAddressLine(r)).join(', '));
			modal.find('[data-value="recipientsVisible"]').val(message.recipientsVisible ? 'Ja' : 'Nein');

			switch (message.priority) {
				case EmailPriority.Low:
					modal.find('[data-value="priority"]').val('niedrig');
					break;

				case EmailPriority.Normal:
					modal.find('[data-value="priority"]').val('normal');
					break;

				case EmailPriority.High:
					modal.find('[data-value="priority"]').val('hoch');
					break;
			}

			if (message.isHtml) {
				$('#dm-message').removeAttr('style');
				$('#dm-message').html(message.message);
			}
			else {
				$('#dm-message').css({
					'white-space': 'pre-line',
					'font-family': 'monospace'
				});
				$('#dm-message').text(message.message);
			}

			$('#detail-modal').modal('show');
		});
	}

	private static InitializeDelete(): void {
		// Multi-Delete from table
		$('#delete').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Sollen die gewählten E-Mails wirklich gelöscht werden?'))
				return;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);

			$('#emails .items tr').each((_, el) => {
				let check = $(el).find('.check-all-child');
				if (check.is(':checked')) {
					formData.append('ids', check.val() as string);
				}
			});

			const response = await fetch(Emails._baseUrls.deleteMulti, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const deletedIds = (await response.json()) as string[];

			for (let id of deletedIds) {
				$('#emails .items tr').each((_, el) => {
					let row = $(el);
					if ((row.data('id') as string) === id) {
						row.remove();
					}
				});

				if (($('#detail-modal').find('[data-value="id"]').val() as string) === id) {
					$('#detail-modal').modal('hide');
				}
			}
		});

		// Single-Delete from modal
		$('#dm-delete').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Soll die E-Mail wirklich gelöscht werden?'))
				return;

			const id = $('#detail-modal').find('[data-value="id"]').val() as string;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', id);

			const response = await fetch(Emails._baseUrls.delete, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			$('#emails .items tr').each((_, el) => {
				let row = $(el);
				if ((row.data('id') as string) === id) {
					row.remove();
				}
			});

			$('#detail-modal').modal('hide');
		});
	}

	private static InitializeReSend(): void {
		// ReSend from table
		$('.resend').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Soll die E-Mail erneut versendet werden?'))
				return;

			let tr = $(event.currentTarget).closest('tr');
			const id = tr.data('id') as string;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', id);

			const response = await fetch(Emails._baseUrls.reSend, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const email = await response.json() as EmailMessage;
			let update = new EmailUpdate();
			update.id = email.id;
			update.status = email.status;
			update.updatedAt = email.updatedAt;

			Emails.UpdateStatus(update);
		});

		$('#dm-resend').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Soll die E-Mail erneut versendet werden?'))
				return;

			const id = $('#detail-modal').find('[data-value="id"]').val() as string;

			let tr = null;
			$('#emails .items tr').each((_, el) => {
				let jqEl = $(el);
				if ((jqEl.data('id') as string) === id) {
				}
			});

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', id);

			const response = await fetch(Emails._baseUrls.reSend, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const email = await response.json() as EmailMessage;
			let update = new EmailUpdate();
			update.id = email.id;
			update.status = email.status;
			update.updatedAt = email.updatedAt;

			Emails.UpdateStatus(update);
		});
	}

	private static GetStatusText(status: EmailStatus): string {
		switch (status) {
			case EmailStatus.Pending:
				return 'ausstehend';

			case EmailStatus.Sent:
				return 'versendet';

			case EmailStatus.Error:
				return 'fehlerhaft';

			case EmailStatus.Blocked:
				return 'blockiert (FakeFilter)';
		}
	}

	private static GetAddressLine(addr: EmailAddress): string {
		let name = TP.IsNullOrWhitespace(addr.name)
			? addr.address
			: addr.name.trim();
		return `${name} <${addr.address}>`;
	}

	private static UpdateStatus(update: EmailUpdate): void {
		const statusText = Emails.GetStatusText(update.status);

		// Update row in table
		$('#emails .items tr').each((_, el) => {
			let row = $(el);

			if ((row.data('id') as string) === update.id) {
				(row.data('message') as EmailMessage).status = update.status;
				(row.data('message') as EmailMessage).updatedAt = update.updatedAt;

				row.find('[data-text="status"]')
					.removeClass('text-success')
					.removeClass('text-warning')
					.removeClass('text-danger')
					.removeClass('text-info');

				row.find('[data-text="status"]').text(statusText);
				switch (update.status) {
					case EmailStatus.Pending:
						row.find('[data-text="status"]').addClass('text-warning');
						row.find('.check-all-child').prop('disabled', false);
						row.find('.resend').prop('disabled', true);
						break;

					case EmailStatus.Sent:
						row.find('[data-text="status"]').addClass('text-success');
						row.find('.check-all-child').prop('disabled', true).prop('checked', false);
						row.find('.resend').prop('disabled', false);
						break;

					case EmailStatus.Error:
						row.find('[data-text="status"]').addClass('text-danger');
						row.find('.check-all-child').prop('disabled', false);
						row.find('.resend').prop('disabled', false);
						break;

					case EmailStatus.Blocked:
						row.find('[data-text="status"]').addClass('text-info');
						row.find('.check-all-child').prop('disabled', true).prop('checked', false);
						row.find('.resend').prop('disabled', true);
						break;
				}

				if (update.updatedAt === undefined || update.updatedAt === null) {
					row.find('[data-text="updatedAt"]').text('');
				}
				else {
					const format = row.find('[data-text="updatedAt"]').data('format') as string;
					const updatedAt = luxon.DateTime.fromISO(update.updatedAt);
					row.find('[data-text="updatedAt"]').text(updatedAt.toFormat(format));
				}
			}
		});

		// Update modal
		let modal = $('#detail-modal');
		const detailId = modal.find('[data-value="id"]').val() as string;

		if (detailId !== update.id)
			return;

		modal.find('[data-value="status"]').val(statusText);
		modal.find('[data-value="status"]').removeClass().addClass('form-control');

		if (update.updatedAt === undefined || update.updatedAt === null) {
			modal.find('[data-value="updatedAt"]').text('');
		}
		else {
			const format = modal.find('[data-value="updatedAt"]').data('format') as string;
			const updatedAt = luxon.DateTime.fromISO(update.updatedAt);
			modal.find('[data-value="updatedAt"]').val(updatedAt.toFormat(format));
		}

		switch (update.status) {
			case EmailStatus.Pending:
				modal.find('[data-value="status"]').addClass('text-warning');
				$('#dm-resend').prop('disabled', true);
				$('#dm-delete').prop('disabled', false);
				break;

			case EmailStatus.Sent:
				modal.find('[data-value="status"]').addClass('text-success');
				$('#dm-resend').prop('disabled', false);
				$('#dm-delete').prop('disabled', true);
				break;

			case EmailStatus.Error:
				modal.find('[data-value="status"]').addClass('text-danger');
				$('#dm-resend').prop('disabled', false);
				$('#dm-delete').prop('disabled', false);
				break;

			case EmailStatus.Blocked:
				modal.find('[data-value="status"]').addClass('text-info');
				$('#dm-resend').prop('disabled', true);
				$('#dm-delete').prop('disabled', true);
				break;
		}
	}

	private static DeleteMulti(ids: string[]): void {
		// Delete row in table
		$('#emails .items tr').each((_, el) => {
			const row = $(el);
			if (ids.indexOf(row.data('id') as string) >= 0) {
				row.remove();
			}
		});

		// Close modal
		let modal = $('#detail-modal');
		const detailId = modal.find('[data-value="id"]').val() as string;

		if (ids.indexOf(detailId) >= 0) {
			modal.modal('hide');
		}
	}
}
