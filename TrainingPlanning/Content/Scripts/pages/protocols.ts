﻿class Protocols {
	private static _baseRequest: string;

	private static _isLoading: boolean = false;

	public static Initialize(baseRequest: string): void {
		Protocols._baseRequest = baseRequest;

		Protocols.InitializeLoad();
		Protocols.InitializeDetails();

		$('#load-entries').trigger('click');
	}

	private static InitializeLoad(): void {
		$('#load-entries').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (Protocols._isLoading)
				return;

			Protocols._isLoading = true;
			try {
				let loadAction = $('#load-entries');
				const id = (loadAction.data('log-id') as number) || 0;

				const response = await fetch(`${Protocols._baseRequest}/${id}`);
				if (!response.ok) {
					const msg = await response.text();
					console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
					return;
				}

				const data = await response.json() as SerilogEntry[];
				DomHelper.BuildTable('#log .tp-template', '#log .items', data, (tr, trData: SerilogEntry) => {
					tr.data('exception', trData.exception.trim());
					tr.data('properties', trData.properties);

					switch (trData.level) {
						case 'Fatal':
							tr.css({
								'color': 'var(--bs-light)',
								'background-color': 'var(--bs-danger)',
								'font-weight': 'bold'
							});
							tr.find('button')
								.removeClass('btn-outline-dark')
								.addClass('btn-outline-light');
							break;
						case 'Error':
							tr.find('[data-text="level"]').css({
								'color': 'var(--bs-danger)',
								'font-weight': 'bold'
							});
							break;
						case 'Warning':
							tr.find('[data-text="level"]').css('color', 'var(--bs-warning)');
							break;
						case 'Information':
							tr.find('[data-text="level"]').css('color', 'var(--bs-info)');
							break;
					}

					tr.find('[data-text="message"]').css({
						'white-space': 'pre-line',
						'max-width': '800px'
					});

					const entryId = parseInt(loadAction.data('log-id'));
					if (isNaN(entryId) || trData.id < entryId)
						loadAction.data('log-id', trData.id);
				});
			}
			catch (error: unknown) {
				console.error('Loading log entries failed', error);
			}
			finally {
				Protocols._isLoading = false;
			}
		});
	}

	private static InitializeDetails(): void {
		$('.details').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();

			const row = $(event.currentTarget).closest('tr');
			const exception = row.data('exception');
			const properties = row.data('properties');

			if (properties) {
				$('#properties').children().remove();
				$('#detail-modal').find('.properties').show();
				delete properties.newline;
				delete properties.error;

				for (let key in properties) {
					let val = properties[key];
					if (typeof val === 'object')
						val = JSON.stringify(val);

					$('<tr/>')
						.append($('<td/>').append($('<em/>').text(key)))
						.append($('<td/>').text(val))
						.appendTo($('#properties'));
				}
			}
			else {
				$('#detail-modal').find('.properties').hide();
			}

			if (exception !== '') {
				$('#detail-modal').find('.exception').show();
				$('#exception').text(exception);
			}
			else {
				$('#detail-modal').find('.exception').hide();
			}

			$('#detail-modal').modal('show');
		});
	}
}
