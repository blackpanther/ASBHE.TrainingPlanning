﻿class TrainingsBaseUrls {
	public get: string;
	public register: string;
	public cancel: string;
	public delete: string;
	public toggleEnable: string;
	public togglePublic: string;
}

class Trainings {
	private static _summernoteLanguage: string = 'de-DE';

	private static _baseUrls: TrainingsBaseUrls;
	private static _tpToast: TPToast;

	public static InitializeIndex(baseUrls: TrainingsBaseUrls): void {
		Trainings._baseUrls = baseUrls;
		Trainings.InitializeRegisterForm();

		WS.RegisterEvent('UpdateAvailableCount', (id: string, count: number) => {
			if (count === null) {
				$(`.available-count[data-id="${id}"]`).html('<span class="fa-solid fa-infinity"></span> freie Plätze');
			}
			else {
				let txt = count.toString();
				if (count === 1) {
					txt += ' freier Platz';
				}
				else {
					txt += ' freie Plätze'
				}
				$(`.available-count[data-id="${id}"]`).text(txt);
			}
		});

		$('.toggle-enable').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let button = $(event.currentTarget);
			let icon = button.find('.fa-solid');

			const id = button.data('id') as string;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', id);

			const response = await fetch(Trainings._baseUrls.toggleEnable, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = (await response.text()).replace("\n", "<br/>");
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				Trainings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Freigabe umschalten fehlgeschlagen:<br/>' + msg, 'danger', 15000);
				return;
			}

			icon.removeClass('fa-toggle-on');
			icon.removeClass('fa-toggle-off');

			const isEnabled: boolean = await response.json();
			if (isEnabled) {
				button.attr('title', 'freigegeben');
				icon.addClass('fa-toggle-on');
			}
			else {
				button.attr('title', 'gesperrt');
				icon.addClass('fa-toggle-off');
			}
		});

		$('.toggle-public').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let button = $(event.currentTarget);
			let icon = button.find('.fa-solid');

			const id = button.data('id') as string;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', id);

			const response = await fetch(Trainings._baseUrls.togglePublic, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = (await response.text()).replace("\n", "<br/>");
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				Trainings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Sichtbarkeit umschalten fehlgeschlagen:<br/>' + msg, 'danger', 15000);
				return;
			}

			icon.removeClass('fa-users');
			icon.removeClass('fa-shield');

			const isPublic: boolean = await response.json();
			if (isPublic) {
				button.attr('title', 'öffentlich');
				icon.addClass('fa-users');
			}
			else {
				button.attr('title', 'intern');
				icon.addClass('fa-shield');
			}
		});

		$('.cancel').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			const id = $(event.currentTarget).data('id') as string;

			const response = await fetch(`${Trainings._baseUrls.get}/${id}`);
			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const training = await response.json();
			$('#cancel-modal').data('training-id', training.id);

			const startDate = luxon.DateTime.fromISO(training.start);
			const endDate = luxon.DateTime.fromISO(training.end);
			$('#cm-training').val(`${training.title} (${startDate.toFormat("dd.MM.yyyy")} - ${endDate.toFormat("dd.MM.yyyy")})`);

			$('#cancel-modal').modal('show');
		});

		$('#cm-submit').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			if ($('#cm-reason').summernote('isEmpty'))
				return;

			let formData = new FormData();
			formData.append(TP.Token.FormName, TP.Token.Value);
			formData.append('id', $('#cancel-modal').data('training-id') as string);
			formData.append('reason', ($('#cm-reason').val() as string).trim());

			const response = await fetch(Trainings._baseUrls.cancel, {
				method: 'POST',
				body: formData
			});

			if (!response.ok) {
				const msg = (await response.text()).replace("\n", "<br/>");
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			window.location.reload();
		});

		$('.delete').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			if (!confirm('Soll der Lehrgang wirklich gelöscht werden?'))
				return;

			const id = $(event.currentTarget).data('id') as string;

			let data = new FormData();
			data.append(TP.Token.FormName, TP.Token.Value);
			data.append('id', id);

			const response = await fetch(Trainings._baseUrls.delete, {
				method: 'POST',
				body: data
			});

			if (response.ok) {
				$(event.currentTarget).closest('.card').remove();
			}
		});

		$('#cancel-modal').on('hidden.bs.modal', _ => {
			$('#cancel-modal').removeData('training');
			$('#cm-reason').summernote('reset');
		});

		$(() => {
			$('#cm-reason').summernote({
				lang: Trainings._summernoteLanguage,
				placeholder: 'Text für die E-Mail, die versendet wird',
				fontNames: TP.SummernoteFonts,
				height: 350,
			});

			// Fix a bug with summernote and bs5
			$('[data-toggle="dropdown"]').each((_, el) => {
				$(el).attr('data-bs-toggle', 'dropdown').removeAttr('data-toggle');
			});
		});
	}

	public static InitializeDetails(baseUrls: TrainingsBaseUrls): void {
		Trainings._baseUrls = baseUrls;
		Trainings.InitializeRegisterForm();

		WS.RegisterEvent('UpdateAvailableCount', (id: string, count: number) => {
			if (count === null) {
				$(`.available-count[data-id="${id}"]`).text('Keine Begrenzung');
			}
			else {
				let msg: string = `noch ${count}`;
				if (count === 1) {
					msg += ' Platz';
				}
				else {
					msg += ' Plätze';
				}
				msg += ' frei';

				$(`.available-count[data-id="${id}"]`).text(msg);
			}
		});
	}

	public static InitializeEdit(baseUrls: TrainingsBaseUrls): void {
		Trainings._baseUrls = baseUrls;

		$('#add-split').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let row = $('#split-table .tp-template').children().clone(true);

			const start = $('#split-table .items tr:last-child').find('[data-value="start"]').val() as string;
			const end = $('#split-table .items tr:last-child').find('[data-value="end"]').val() as string;

			row.find('[data-value="start"]').val(start);
			row.find('[data-value="end"]').val(end);

			$('#split-table .items').append(row);
		});

		$('.del-split').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			$(event.currentTarget).closest('tr').remove();
		});

		// Initialize summernote WYSIWYG editor
		// when DOM is ready (for jQuery)
		$(() => {
			$('#ShortDescription').summernote({
				lang: Trainings._summernoteLanguage,
				placeholder: 'Kurze Zusammenfassung für die Übersicht',
				fontNames: TP.SummernoteFonts,
				height: 50,
				toolbar: [
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
				]
			});

			$('#Description').summernote({
				lang: Trainings._summernoteLanguage,
				placeholder: 'Lehrgangsbeschreibung für die Detailseite',
				fontNames: TP.SummernoteFonts,
				height: 250,
				toolbar: [
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
					['style', ['style']],
					['fontname', ['fontname']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture']],
					['view', ['fullscreen', 'help']]
				]
			});

			$('#EntryRequirements').summernote({
				lang: Trainings._summernoteLanguage,
				placeholder: 'Auflistung der Zugangsvoraussetzungen',
				fontNames: TP.SummernoteFonts,
				height: 50,
				toolbar: [
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
					['para', ['ul', 'ol', 'paragraph']]
				]
			});

			$('#Hints').summernote({
				lang: Trainings._summernoteLanguage,
				placeholder: 'Hinweise zum Lehrgang',
				fontNames: TP.SummernoteFonts,
				height: 150,
				toolbar: [
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
				]
			});

			// Fix a bug with summernote and bs5
			$('[data-toggle="dropdown"]').each((_, el) => {
				$(el).attr('data-bs-toggle', 'dropdown').removeAttr('data-toggle');
			});
		});

		$('form').on('submit', _ => {
			if ($('#ShortDescription').summernote('isEmpty')) {
				$('#ShortDescription').val('');
			}

			if ($('#Description').summernote('isEmpty')) {
				$('#Description').val('');
			}

			if ($('#EntryRequirements').summernote('isEmpty')) {
				$('#EntryRequirements').val('');
			}

			if ($('#Hints').summernote('isEmpty')) {
				$('#Hints').val('');
			}

			let splits: TrainingSplit[] = [];
			$('#split-table .items tr').each((_, tr) => {
				const row = $(tr);
				let split = new TrainingSplit();

				split.name = (row.find('[data-value="name"]').val() as string).trim();
				split.start = row.find('[data-value="start"]').val() as string;
				split.end = row.find('[data-value="end"]').val() as string;
				split.description = (row.find('[data-value="description"]').val() as string).trim();

				if (split.name !== '')
					splits.push(split);
			});

			$('#SplitsJson').val(JSON.stringify(splits));
		});

		$('#delete').on('click', (event: JQuery.ClickEvent) => {
			if (!confirm('Soll der Lehrgang wirklich gelöscht werden?'))
				event.preventDefault();
		});

		const splits = JSON.parse($('#SplitsJson').val() as string) as TrainingSplit[];
		DomHelper.BuildTable('#split-table .tp-template', '#split-table .items', splits);
	}

	private static InitializeRegisterForm(): void {
		Trainings._tpToast = new TPToast();

		$('.register').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			const id = $(event.currentTarget).data('id') as string;

			const response = await fetch(`${Trainings._baseUrls.get}/${id}`);
			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const training = await response.json();
			$('#register-modal').data('training', training);

			const startDate = luxon.DateTime.fromISO(training.start);
			const endDate = luxon.DateTime.fromISO(training.end);
			$('#rm-training').val(`${training.title} (${startDate.toFormat("dd.MM.yyyy")} - ${endDate.toFormat("dd.MM.yyyy")})`);

			const json = localStorage.getItem('TP.RegisterData');
			if (json !== null) {
				const data = JSON.parse(json) as RegisterViewModel;
				console.log(data);

				$('#rm-firstname').val(data.firstName);
				$('#rm-lastname').val(data.lastName);
				$('#rm-email').val(data.emailAddress);
				$('#rm-phone').val(data.phone);
				$('#rm-birthday').val(data.birthday);
				$('#rm-pob').val(data.placeOfBirth);
				$('#rm-department').val(data.department);

				$('#rm-street').val(data.street);
				$('#rm-zipcode').val(data.zipCode);
				$('#rm-city').val(data.city);
				$('#rm-addressaddition').val(data.addressAddition);
				$('#rm-country').val(data.country);

				$('.dc-check').prop('checked', false);
				if (data.driverLicenseClasses) {
					const classes: string[] = data.driverLicenseClasses.split(',');
					classes.forEach((dc) => {
						$(`.dc-check[value="${dc}"]`).prop('checked', true);
					});
				}
				$('#rm-driverpermission').prop('checked', data.driverPermission);

				$('#rm-assistance').prop('checked', data.assistanceRequired);
				$('#rm-foodselection').val(data.foodSelection);
				$('#rm-allergies').val(data.allergies);

				if (data.assumptionOfCosts) {
					$('#rm-costs-takeover').prop('checked', true);
					$('#rm-costs-takeover').trigger('change');
				}
				else {
					$('#rm-costs-self').prop('checked', true);
					$('#rm-costs-self').trigger('change');
				}

				$('#rm-invoice-name').val(data.invoiceName);
				$('#rm-invoice-street').val(data.invoiceStreet);
				$('#rm-invoice-zipcode').val(data.invoiceZipCode);
				$('#rm-invoice-city').val(data.invoiceCity);

				$('#rm-save-local').prop('checked', true);
			}

			$('#register-modal').modal('show');
		});

		$('#rm-save-local').on('click', _ => {
			if (!$('#rm-save-local').is(':checked')) {
				localStorage.removeItem('TP.RegisterData');
			}
		});

		$('#rm-known-participant').on('input change', _ => {
			let input = $('#rm-known-participant');
			try {
				const participant = JSON.parse(input.val() as string) as Participant;
				console.log(participant);
				input.val('');

				$('#rm-firstname').val(participant.firstName);
				$('#rm-lastname').val(participant.lastName);
				$('#rm-email').val(participant.emailAddress);
				$('#rm-phone').val(participant.phoneNumber);
				$('#rm-birthday').val(participant.birthday.substring(0, participant.birthday.indexOf('T')));
				$('#rm-pob').val(participant.placeOfBirth);
				$('#rm-department').val(participant.department?.name);

				$('#rm-street').val(participant.street);
				$('#rm-zipcode').val(participant.zipCode);
				$('#rm-city').val(participant.city);
				$('#rm-addressaddition').val(participant.addressAddition);
				$('#rm-country').val(participant.country);

				$('.dc-check').prop('checked', false);
				if (participant.driverLicenseClasses) {
					const classes: string[] = participant.driverLicenseClasses.split(',');
					classes.forEach((dc) => {
						$(`.dc-check[value="${dc}"]`).prop('checked', true);
					});
				}

				$('#rm-driverpermission').prop('checked', participant.driverPermission);

				$('#rm-assistance').prop('checked', participant.assistanceRequired);
				$('#rm-hotel').val(0);
				$('#rm-foodselection').val(participant.foodSelection);
				$('#rm-allergies').val(participant.allergies);

				$('#rm-costs-self').prop('checked', true);
				$('#rm-costs-self').trigger('change');
			}
			catch (e) {
				// keep it quiet
			}
		});

		$('#register-modal').on('hidden.bs.modal', _ => {
			$('#register-modal').removeData('training');
			$('#rm-save-local').prop('checked', false);

			$(':input', '#register-modal')
				.not(':button, :submit, :reset, .dc-check')
				.val('')
				.prop('checked', false)
				.prop('selected', false);
			$('.dc-check').prop('checked', false);

			$('select', '#register-modal')
				.find('option')
				.first()
				.prop('selected', true);

			$('#rm-costs-self')
				.prop('checked', true)
				.trigger('changed');
		});

		$('[name=costs]').on('change', (event: JQuery.ChangeEvent) => {
			if ($('#rm-costs-takeover').is(':checked')) {
				$('#rm-invoice').slideDown();
			}
			else {
				$('#rm-invoice').slideUp();
			}
		}).trigger('change');

		$('#rm-submit').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			let data = new RegisterViewModel();
			data.trainingId = $('#register-modal').data('training').id as string;

			data.firstName = $('#rm-firstname').val() as string;
			data.lastName = $('#rm-lastname').val() as string;
			data.emailAddress = $('#rm-email').val() as string;
			data.phone = $('#rm-phone').val() as string;
			data.birthday = $('#rm-birthday').val() as string;
			data.placeOfBirth = $('#rm-pob').val() as string;
			data.department = $('#rm-department').val() as string;

			data.street = $('#rm-street').val() as string;
			data.zipCode = $('#rm-zipcode').val() as string;
			data.city = $('#rm-city').val() as string;
			data.addressAddition = $('#rm-addressaddition').val() as string;
			data.country = $('#rm-country').val() as string;
			data.driverLicenseClasses = null;

			let driverLicenseClasses: string[] = [];
			$('.dc-check').each((_, el) => {
				const checkbox = $(el);
				if (checkbox.is(':checked'))
					driverLicenseClasses.push(checkbox.val() as string);
			});
			if (driverLicenseClasses.length > 0)
				data.driverLicenseClasses = driverLicenseClasses.sort().join(',');

			data.driverPermission = $('#rm-driverpermission').is(':checked');

			data.assistanceRequired = $('#rm-assistance').is(':checked');
			data.hotelRequirement = Number($('#rm-hotel').val()) as HotelRequirement;
			data.foodSelection = (parseInt($('#rm-foodselection').val() as string) as FoodSelection) || null;
			data.allergies = $('#rm-allergies').val() as string;

			data.assumptionOfCosts = $('#rm-costs-takeover').is(':checked');
			data.invoiceName = $('#rm-invoice-name').val() as string;
			data.invoiceStreet = $('#rm-invoice-street').val() as string;
			data.invoiceZipCode = $('#rm-invoice-zipcode').val() as string;
			data.invoiceCity = $('#rm-invoice-city').val() as string;

			data.termsAccepted = $('#rm-accept-terms').is(':checked');

			let headers: any = {
				'Content-Type': 'application/json'
			};
			headers[TP.Token.HeaderName] = TP.Token.Value;

			const response = await fetch(Trainings._baseUrls.register, {
				method: 'POST',
				headers: headers,
				body: JSON.stringify(data)
			});

			if (!response.ok) {
				const msg = (await response.text()).replace("\n", "<br/>");
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				Trainings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Anmeldung ist fehlgeschlagen:<br/>' + msg, 'danger', 15000);
				return;
			}

			Trainings._tpToast.Create('<span class="fa-solid fa-fw fa-check"></span> Anmeldung ist eingegangen', 'success', 10000);

			if ($('#rm-save-local').is(':checked')) {
				delete data.trainingId;
				delete data.hotelRequirement;
				delete data.termsAccepted;

				localStorage.setItem('TP.RegisterData', JSON.stringify(data));
			}

			$('#register-modal').modal('hide');
		});
	}
}
