﻿class SettingsBaseUrls {
	public get: string;
	public set: string;
	public delete: string;
}

class Settings {
	private static _baseUrls: SettingsBaseUrls;
	private static _tpToast: TPToast;

	public static Initialize(baseUrls: SettingsBaseUrls): void {
		Settings._baseUrls = baseUrls;
		Settings._tpToast = new TPToast();

		Settings.InitializeSmtp();
		Settings.InitializeTerms();
		Settings.InitializeSignalR();
	}

	private static InitializeSmtp(): void {
		$('#smtp-tab').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			const response = await fetch(Settings._baseUrls.get + '?settingName=smtp');
			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const settings = (await response.json()) as SmtpSetting;
			if (settings === null) {
				$('#smtp-alert').show();
				$(':input', '#smtp-pane')
					.not(':button, :submit, :reset')
					.val('')
					.prop('checked', false)
					.prop('selected', false);
			}
			else {
				$('#smtp-alert').hide();
				DomHelper.BindData($('#smtp-pane'), settings);
			}
		});

		$('#smtp-save').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			let form = new FormData();
			form.append(TP.Token.FormName, TP.Token.Value);
			form.append('settingName', 'smtp');

			let settings = new SmtpSetting();
			$('[data-value]', '#smtp-pane').each((_, el) => {
				const input = $(el);
				const name = input.data('value') as string;

				switch (input.attr('type')) {
					case 'checkbox':
						(settings as any)[name] = input.is(':checked');
						break;

					case 'number':
						(settings as any)[name] = Number(input.val());
						break;

					default:
						(settings as any)[name] = input.val() as string;
						break;
				}
			});
			form.append('settingsJson', JSON.stringify(settings));

			const response = await fetch(Settings._baseUrls.set, {
				method: 'POST',
				body: form
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);

				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Die SMTP Einstellungen konnten nicht gespeichert werden', 'danger', 10000);
			}

			$('#smtp-tab').trigger('click');
			if (response.ok)
				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-check"></span> Die SMTP Einstellungen wurden gespeichert', 'success', 5000);
		});

		$('#smtp-delete').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			if (!confirm('Sollen die SMTP Einstellungen gelöscht werden?'))
				return;

			let form = new FormData();
			form.append(TP.Token.FormName, TP.Token.Value);
			form.append('settingName', 'smtp');

			const response = await fetch(Settings._baseUrls.delete, {
				method: 'DELETE',
				body: form
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);

				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Die SMTP Einstellungen konnten nicht gelöscht werden', 'warning', 10000);
			}

			$('#smtp-tab').trigger('click');
			if (response.ok)
				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-check"></span> Die SMTP Einstellungen wurden gelöscht', 'info', 5000);
		});
	}

	private static InitializeTerms(): void {
		$('#terms-tab').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			const response = await fetch(Settings._baseUrls.get + '?settingName=terms');
			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);
				alert(`Fehler ${response.status}: ${response.statusText}\n${msg}`);
				return;
			}

			const settings = (await response.json()) as string;
			$('#terms-content').val(settings);
		});

		$('#terms-save').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();

			let form = new FormData();
			form.append(TP.Token.FormName, TP.Token.Value);
			form.append('settingName', 'terms');

			let settings = $('#terms-content').val() as string;
			form.append('settingsJson', JSON.stringify(settings));

			const response = await fetch(Settings._baseUrls.set, {
				method: 'POST',
				body: form
			});

			if (!response.ok) {
				const msg = await response.text();
				console.error(`Error ${response.status}: ${response.statusText}\n${msg}`);

				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-times"></span> Die Teilnahmebedingungen konnten nicht gespeichert werden', 'danger', 10000);
			}

			$('#terms-tab').trigger('click');
			if (response.ok)
				Settings._tpToast.Create('<span class="fa-solid fa-fw fa-check"></span> Die Teilnahmebedingungen wurden gespeichert', 'success', 5000);
		});

		$(() => {
			$('#terms-content').summernote({
				lang: 'de-DE',
				height: 400,
				placeholder: 'Hier können die Teilnahmebedingungen formuliert werden',
				fontNames: TP.SummernoteFonts,
				fontNamesIgnoreCheck: ['Noto Sans', 'Noto Serif'],
				toolbar: [
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
					['style', ['style']],
					['fontname', ['fontname']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['table', ['table']],
					['insert', ['link', 'picture']],
					['view', ['fullscreen', 'help']]
				]
			});

			// Fix a bug with summernote and bs5
			$('[data-toggle="dropdown"]').each((_, el) => {
				$(el).attr('data-bs-toggle', 'dropdown').removeAttr('data-toggle');
			});
		});
	}

	private static InitializeSignalR(): void {
		$('#signalr-param-add').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			let row = $('#signalr-params .tp-template').children().clone(true);
			const id = `signalr-value-null-${Date.now()}`;

			row.find('.form-check-input').attr('id', id);
			row.find('.form-check-label').attr('for', id);

			$('#signalr-params .items').append(row);
		});

		$('.signalr-param-del').on('click', (event: JQuery.ClickEvent) => {
			event.preventDefault();
			$(event.currentTarget).closest('tr').remove();
		});

		$('.signalr-type').on('change', (event: JQuery.ChangeEvent) => {
			let row = $(event.currentTarget).closest('tr');
			switch ($(event.currentTarget).val()) {
				case 'number':
					row.find('.signalr-value').attr('type', 'number').val('');
					break;

				default:
					row.find('.signalr-value').attr('type', 'text').val('');
					break;
			}
		});

		$('.signalr-value-null').on('change', (event: JQuery.ChangeEvent) => {
			let row = $(event.currentTarget).closest('tr');
			row.find('.signalr-value').prop('disabled', $(event.currentTarget).is(':checked'));
		});

		$('#signalr-send').on('click', async (event: JQuery.ClickEvent) => {
			event.preventDefault();
			const methodName = $('#signalr-method-name').val() as string;
			if (TP.IsNullOrWhitespace(methodName))
				return;

			let params: any[] = [];
			$('#signalr-params .items tr').each((_, tr) => {
				const row = $(tr);
				if (row.find('.signalr-value-null').is(':checked')) {
					params.push(null);
				}
				else {
					const rawValue = row.find('.signalr-value').val() as string;
					switch (row.find('.signalr-type').val()) {
						case 'number':
							params.push(Number(rawValue));
							break;

						case 'array-string':
							params.push(rawValue.split(','));
							break;

						case 'array-number':
							params.push(rawValue.split(',').map(v => Number(v)));
							break;

						case 'object':
							params.push(JSON.parse(rawValue));
							break;

						default:
							params.push(rawValue);
							break;
					}
				}
			});
			await WS.SendAsync(methodName.trim(), ...params);
		});
	}
}
