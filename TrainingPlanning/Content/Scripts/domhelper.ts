﻿class DomHelper {
	/**
	 * Builds table rows from a template with the array of data.
	 *
	 * @param sourceElement The source HTML element to clone for each data row.
	 * @param targetElement The target HTML element to append the created row.
	 * @param dataRows The array of data.
	 * @param rowInit A callback function to modify the data row.
	 * @param doPrepend A value indicating to prepend the rows instead of appending.
	 */
	public static BuildTable(sourceElement: string, targetElement: string, dataRows: object[], rowInit: (tr: JQuery, dataRow: unknown) => void = null, doPrepend: boolean = false): void {
		if (sourceElement === undefined || sourceElement === null || sourceElement.trim().length === 0)
			return;

		if (targetElement === undefined || targetElement === null || targetElement.trim().length === 0)
			return;

		if (dataRows === undefined || dataRows === null || dataRows.length === 0)
			return;

		for (const row of dataRows) {
			let tableRow = $(sourceElement).children().clone(true); // deep clone with events
			DomHelper.BindData(tableRow, row);

			if (doPrepend) {
				$(targetElement).prepend(tableRow);
			}
			else {
				$(targetElement).append(tableRow);
			}

			if (!!rowInit)
				rowInit(tableRow, row);
		}
	}

	/**
	 * Binds data to a jQuery element.
	 * The binings are defined by data-* (text, html, title, value).
	 *
	 * @param jqElement The jQuery element to bind the data to.
	 * @param data The data object.
	 */
	public static BindData(jqElement: JQuery, data: unknown): void {
		if (jqElement.length === 0 || data === undefined || data === null)
			return;

		jqElement.find('[data-text]').each((_, element) => {
			let jqEl = $(element);

			const key = jqEl.data('text') as string;
			const val = DomHelper.GetProperty(key, data);
			let res: string;

			if (jqEl.data('type') === 'luxon' && !!val) {
				const format = jqEl.data('format') || 'yyyy-MM-dd HH:mm:ss';
				const dt = luxon.DateTime.fromISO(val);
				res = dt.toFormat(format);
			}
			else if (jqEl.data('type') === 'bytes') {
				res = DomHelper.AutoFileSize(Number(val));
			}
			else {
				res = val as string;
			}

			jqEl.text(res);
		});

		jqElement.find('[data-html]').each((_, element) => {
			let jqEl = $(element);

			const key = jqEl.data('html') as string;
			const val = DomHelper.GetProperty(key, data);
			let res: string;

			if (jqEl.data('type') === 'luxon' && !!val) {
				const format = jqEl.data('format') || 'yyyy-MM-dd HH:mm:ss';
				const dt = luxon.DateTime.fromISO(val);
				res = dt.toFormat(format);
			}
			else if (jqEl.data('type') === 'bytes') {
				res = DomHelper.AutoFileSize(Number(val));
			}
			else {
				res = val as string;
			}

			jqEl.html(res);
		});

		jqElement.find('[data-title]').each((_, element) => {
			let jqEl = $(element);

			const key = jqEl.data('title') as string;
			const val = DomHelper.GetProperty(key, data);
			let res: string;

			if (jqEl.data('type') === 'luxon' && !!val) {
				const format = jqEl.data('format') || 'yyyy-MM-dd HH:mm:ss';
				const dt = luxon.DateTime.fromISO(val);
				res = dt.toFormat(format);
			}
			else if (jqEl.data('type') === 'bytes') {
				res = DomHelper.AutoFileSize(Number(val));
			}
			else {
				res = val as string;
			}

			jqEl.attr('title', res);
		});

		jqElement.find('[data-value]').each((_, element) => {
			let jqEl = $(element);

			const key = jqEl.data('value') as string;
			const val = DomHelper.GetProperty(key, data);
			let res: string;

			if (jqEl.data('type') === 'luxon' && !!val) {
				const format = jqEl.data('format') || 'yyyy-MM-dd HH:mm:ss';
				const dt = luxon.DateTime.fromISO(val);
				res = dt.toFormat(format);
			}
			else if (jqEl.data('type') === 'bytes') {
				res = DomHelper.AutoFileSize(Number(val));
			}
			else {
				res = val as string;
			}

			if (jqEl.attr('type') === 'checkbox') {
				jqEl.prop('checked', val as boolean);
			}
			else {
				jqEl.val(res);
			}
		});
	}

	private static GetProperty(path: string, source: unknown): any {
		if (path === undefined || path === null || path.length === 0)
			throw Error('Argument not set: path');

		const sections = path.split('.');
		let result: any = source;
		for (let section of sections) {
			if (result === undefined || result === null)
				return result;

			result = result[section];
		}

		return result;
	}

	private static AutoFileSize(bytes: number): string {
		let scale: number = 1024 * 1024 * 1024 * 1024;

		if (bytes > scale)
			return (Math.round(bytes * 100 / scale) / 100) + ' TB';

		scale /= 1024;
		if (bytes > scale)
			return (Math.round(bytes * 100 / scale) / 100) + ' GB';

		scale /= 1024;
		if (bytes > scale)
			return (Math.round(bytes * 100 / scale) / 100) + ' MB';

		scale /= 1024;
		if (bytes > scale)
			return (Math.round(bytes * 100 / scale) / 100) + ' KB';

		return bytes + ' B';
	}
}
