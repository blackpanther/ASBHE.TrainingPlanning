﻿namespace AMWD.ASBHE.TrainingPlanning.Utils
{
	public class SmtpSettings
	{
		public string Host { get; set; }

		public int Port { get; set; }

		public bool StartTls { get; set; }

		public bool Ssl { get; set; }

		public bool ValidateCertificates { get; set; }

		public bool CheckRevocationList { get; set; }

		public string Username { get; set; }

		public string Password { get; set; }

		public string Sender { get; set; }

		public string Name { get; set; }

		public string SubjectTag { get; set; }

		public bool SendSelfOnBcc { get; set; }
	}
}
