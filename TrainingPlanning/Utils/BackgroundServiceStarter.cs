﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AMWD.ASBHE.TrainingPlanning.Utils
{
	/// <summary>
	/// Wrapper class to start a background service.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/Utilities/BackgroundServiceStarter.cs">AM.WD Common</see>.
	/// </remarks>
	/// <typeparam name="TService">The service type.</typeparam>
	internal class BackgroundServiceStarter<TService> : IHostedService
		where TService : class, IHostedService
	{
		private readonly TService _service;

		/// <summary>
		/// Initializes an new instance of the <see cref="BackgroundServiceStarter{TService}"/> class.
		/// </summary>
		/// <param name="backgroundService">The service to work in background.</param>
		public BackgroundServiceStarter(TService backgroundService)
		{
			_service = backgroundService;
		}

		/// <summary>
		/// Starts the service.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public Task StartAsync(CancellationToken cancellationToken)
		{
			return _service.StartAsync(cancellationToken);
		}

		/// <summary>
		/// Stops the service.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public Task StopAsync(CancellationToken cancellationToken)
		{
			return _service.StopAsync(cancellationToken);
		}
	}
}
