﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Services
{
	public class InitService : IHostedService
	{
		private readonly ILogger _logger;
		private readonly IServiceScopeFactory _serviceScopeFactory;

		public InitService(ILogger<InitService> logger, IServiceScopeFactory serviceScopeFactory)
		{
			_logger = logger;
			_serviceScopeFactory = serviceScopeFactory;
		}

		public bool IsDatabaseSuccess { get; private set; }

		public async Task StartAsync(CancellationToken cancellationToken)
		{
			try
			{
				_logger.LogDebug("Initializing FakeFilter");

				using var scope = _serviceScopeFactory.CreateScope();
				var fakeFilter = scope.ServiceProvider.GetRequiredService<ExtendedFakeFilterService>();
				if (await fakeFilter.UpdateData(cancellationToken))
				{
					_logger.LogInformation("Updating FakeFilter succeeded");
				}
				else
				{
					_logger.LogWarning("Updating FakeFilter failed");
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Updating FakeFilter failed: {ex.GetMessage()}");
			}

			try
			{
				_logger.LogDebug("Initializing Admin-User");

				using var scope = _serviceScopeFactory.CreateScope();
				var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();

				string adminMail = configuration.GetValue<string>("Init:AdminMail")?.Trim().ToLower();
				string adminPass = configuration.GetValue<string>("Init:AdminPass")?.Trim();

				if (string.IsNullOrWhiteSpace(adminMail))
					adminMail = "root@localhost";
				if (string.IsNullOrWhiteSpace(adminPass))
					adminPass = "admin123";

				if (!await dbContext.Users
					.Where(u => u.IsEnabled)
					.Where(u => u.IsAdmin)
					.AnyAsync(cancellationToken))
				{
					var admin = dbContext.Users
						.Where(u => u.EmailAddress == adminMail)
						.FirstOrDefault();
					if (admin == null)
					{
						admin = new User
						{
							Id = dbContext.CreateUserId(),
							EmailAddress = adminMail,
							PasswordHash = PasswordHelper.HashPassword(adminPass)
						};
						await dbContext.Users.AddAsync(admin, cancellationToken);

						_logger.LogInformation($"Creating administrator: {adminMail} / {adminPass}");
					}

					admin.IsEnabled = true;
					admin.IsAdmin = true;

					await dbContext.SaveChangesAsync(cancellationToken);
				}

				IsDatabaseSuccess = true;
			}
			catch (Exception ex)
			{
				_logger.LogCritical(ex, $"Initializing database failed: {ex.GetMessage()}");
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
			=> Task.CompletedTask;
	}
}
