﻿using System.Threading;
using System.Threading.Tasks;
using AMWD.Net.Api.FakeFilter;

namespace AMWD.ASBHE.TrainingPlanning.Services
{
	public class ExtendedFakeFilterService : FakeFilterService
	{
		public async Task<bool> IsEmailValid(string emailAddress, CancellationToken cancellationToken = default)
		{
			var result = await base.IsFakeEmail(emailAddress, useOnlyOfflineData: true, cancellationToken);
			return result.IsSuccess && !result.IsFakeDomain;
		}
	}
}
