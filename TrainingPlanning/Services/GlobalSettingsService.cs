﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using Microsoft.EntityFrameworkCore;

namespace AMWD.ASBHE.TrainingPlanning.Services
{
	public class GlobalSettingsService
	{
		private readonly ServerDbContext _dbContext;

		public GlobalSettingsService(ServerDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public T Get<T>(string name, T fallbackValue = default)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefault();
			if (setting == null)
				return fallbackValue;

			return setting.Value.DeserializeJson(fallbackValue);
		}

		public async Task<T> GetAsync<T>(string name, T fallbackValue = default, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = await _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefaultAsync(cancellationToken);
			if (setting == null)
				return fallbackValue;

			return setting.Value.DeserializeJson(fallbackValue);
		}

		public void Set<T>(string name, T value)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefault();
			if (setting == null)
			{
				setting = new GlobalSetting
				{
					Name = name,
					CreatedAt = DateTime.UtcNow,
				};
				_dbContext.GlobalSettings.Add(setting);
			}
			else
			{
				setting.UpdatedAt = DateTime.UtcNow;
			}

			setting.Value = value.SerializeJson();
			_dbContext.SaveChanges();
		}

		public async Task SetAsync<T>(string name, T value, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = await _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefaultAsync(cancellationToken);
			if (setting == null)
			{
				setting = new GlobalSetting
				{
					Name = name,
					CreatedAt = DateTime.UtcNow,
				};
				await _dbContext.GlobalSettings.AddAsync(setting, cancellationToken);
			}
			else
			{
				setting.UpdatedAt = DateTime.UtcNow;
			}

			setting.Value = value.SerializeJson();
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		public void Delete(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefault();
			if (setting == null)
				return;

			_dbContext.GlobalSettings.Remove(setting);
			_dbContext.SaveChanges();
		}

		public async Task DeleteAsync(string name, CancellationToken cancellationToken = default)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			name = name.Trim().ToLower();
			var setting = await _dbContext.GlobalSettings
				.Where(s => s.Name == name)
				.OrderByDescending(s => s.UpdatedAt)
				.ThenByDescending(s => s.UpdatedAt)
				.FirstOrDefaultAsync(cancellationToken);
			if (setting == null)
				return;

			_dbContext.GlobalSettings.Remove(setting);
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		public IReadOnlyCollection<string> GetKeys()
		{
			return _dbContext.GlobalSettings
				.Select(s => s.Name)
				.Distinct()
				.ToList();
		}

		public async Task<IReadOnlyCollection<string>> GetKeysAsync(CancellationToken cancellationToken = default)
		{
			return await _dbContext.GlobalSettings
				.Select(s => s.Name)
				.Distinct()
				.ToListAsync(cancellationToken);
		}
	}
}
