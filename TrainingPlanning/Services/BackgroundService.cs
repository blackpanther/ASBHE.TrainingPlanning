﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Services
{
	public class BackgroundService : IHostedService
	{
		private const int EMAIL_DELETE_AFTER_DAYS = 30;

		private readonly ILogger _logger;
		private readonly IServiceScopeFactory _serviceScopeFactory;

		private CancellationTokenSource _stopCts;

		private Timer _fakeFilterTimer;
		private Timer _emailCleanupTimer;

		public BackgroundService(ILogger<BackgroundService> logger, IServiceScopeFactory serviceScopeFactory)
		{
			_logger = logger;
			_serviceScopeFactory = serviceScopeFactory;
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			_stopCts = new CancellationTokenSource();

			var ffInterval = TimeSpan.FromHours(12);
			_fakeFilterTimer = new Timer(OnFakeFilterTimer, null, ffInterval.GetAlignedInterval(), ffInterval);

			var cleanupInterval = TimeSpan.FromHours(24);
			_emailCleanupTimer = new Timer(OnEmailCleanupTimer, null, cleanupInterval.GetAlignedInterval(), cleanupInterval);

			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_stopCts.Cancel();

			_fakeFilterTimer.Dispose();
			_emailCleanupTimer.Dispose();

			_stopCts.Dispose();

			return Task.CompletedTask;
		}

		private async void OnFakeFilterTimer(object _)
		{
			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var service = scope.ServiceProvider.GetRequiredService<ExtendedFakeFilterService>();

				if (!await service.UpdateData(_stopCts.Token))
					_logger.LogWarning("Updating FakeFilter failed");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Updating FakeFilter failed: {ex.GetMessage()}");
			}
		}

		private async void OnEmailCleanupTimer(object _)
		{
			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();
				var webHub = scope.ServiceProvider.GetService<IHubContext<WebClientHub>>();

				var dt = DateTime.UtcNow.AddDays(-EMAIL_DELETE_AFTER_DAYS);

				var emailsToDelete = await dbContext.EmailMessages
					.Where(e => e.SendAfterDate <= dt)
					.Where(e => e.UpdatedAt <= dt)
					.Where(e => e.Status != EmailMessageStatus.Pending)
					.ToListAsync(_stopCts.Token);

				if (!emailsToDelete.Any())
					return;

				var emailIds = emailsToDelete.Select(e => e.Id).ToList();

				using var transaction = await dbContext.BeginTransactionAsync(_stopCts.Token);

				dbContext.EmailAttachments.RemoveRange(dbContext.EmailAttachments.Where(a => emailIds.Contains(a.EmailMessageId)));
				dbContext.EmailMessages.RemoveRange(emailsToDelete);

				await dbContext.SaveChangesAsync(_stopCts.Token);
				await transaction.CommitAsync(_stopCts.Token);

				try
				{
					var hub = scope.ServiceProvider.GetService<IHubContext<WebClientHub>>();
					await hub.Clients.Group(WebClientHub.GROUP_ADMIN).SendAsync("EmailDeleteMulti", emailIds, _stopCts.Token);
				}
				catch
				{ /* keep things quiet */ }
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"E-mail cleanup failed: {ex.GetMessage()}");
			}
		}
	}
}
