﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Hubs;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Utils;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace AMWD.ASBHE.TrainingPlanning.Services
{
	public class EmailService : IHostedService
	{
		private readonly ILogger _logger;
		private readonly IServiceScopeFactory _serviceScopeFactory;

		private Timer _timer;
		private CancellationTokenSource _stopCts;
		private readonly SemaphoreSlim _timerMutex = new(1, 1);

		public EmailService(ILogger<EmailService> logger, IServiceScopeFactory serviceScopeFactory)
		{
			_logger = logger;
			_serviceScopeFactory = serviceScopeFactory;
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			_logger.LogTrace($"{nameof(EmailService)} starting...");
			var interval = TimeSpan.FromSeconds(60);

			_stopCts = new CancellationTokenSource();
			_timer = new Timer(OnTimer, null, interval.GetAlignedInterval(), interval);

			_logger.LogTrace($"{nameof(EmailService)} started");
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_logger.LogTrace($"{nameof(EmailService)} stopping...");

			_timer?.Dispose();
			_stopCts?.Cancel();

			_logger.LogTrace($"{nameof(EmailService)} stopped");
			return Task.CompletedTask;
		}

		private async void OnTimer(object _)
		{
			if (!_timerMutex.Wait(0))
				return;

			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
				var fakeFilter = scope.ServiceProvider.GetRequiredService<ExtendedFakeFilterService>();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();
				var globalSettings = scope.ServiceProvider.GetRequiredService<GlobalSettingsService>();

				var mails = dbContext.EmailMessages
					.Where(m => m.SendAfterDate <= DateTime.UtcNow)
					.Where(m => m.Status == EmailMessageStatus.Pending)
					.ToList();

				if (!mails.Any())
					return;

				var smtpSettings = await globalSettings.GetAsync<SmtpSettings>("SMTP", cancellationToken: _stopCts.Token);
				if (smtpSettings == null)
				{
					smtpSettings = new SmtpSettings
					{
						Host = configuration.GetValue<string>("SMTP:Host"),
						Port = configuration.GetValue("SMTP:Port", 25),
						StartTls = configuration.GetValue("SMTP:StartTLS", false),
						Ssl = configuration.GetValue("SMTP:SSL", false),
						ValidateCertificates = configuration.GetValue("SMTP:ValidateCertificates", true),
						CheckRevocationList = configuration.GetValue("SMTP:CheckRevocationList", true),

						Username = configuration.GetValue<string>("SMTP:Username"),
						Password = configuration.GetValue<string>("SMTP:Password"),

						Sender = configuration.GetValue("SMTP:Sender", "no-reply@localhost"),
						Name = configuration.GetValue("SMTP:Name", Program.Service),
						SubjectTag = configuration.GetValue<string>("SMTP:SubjectTag"),
						SendSelfOnBcc = configuration.GetValue("SMTP:SendSelfOnBCC", false)
					};
				}
				else
				{
					var crypto = scope.ServiceProvider.GetService<CryptographyHelper>();
					smtpSettings.Password = crypto.DecryptAes(smtpSettings.Password);
				}

				var sslOption = smtpSettings.Ssl ? SecureSocketOptions.SslOnConnect
					: smtpSettings.StartTls ? SecureSocketOptions.StartTls
					: SecureSocketOptions.None;

				using var smtpClient = new SmtpClient
				{
					CheckCertificateRevocation = smtpSettings.CheckRevocationList
				};

				if (!smtpSettings.ValidateCertificates)
				{
					smtpClient.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) =>
					{
						_logger.LogDebug($"SMTP certificate validation DISABLED! errors={sslPolicyErrors}, subject={certificate?.Subject}");
						return true;
					};
				}

				await smtpClient.ConnectAsync(
					smtpSettings.Host,
					smtpSettings.Port,
					sslOption,
					_stopCts.Token
				);

				if (!string.IsNullOrWhiteSpace(smtpSettings.Username))
				{
					await smtpClient.AuthenticateAsync(
						smtpSettings.Username,
						smtpSettings.Password,
						_stopCts.Token
					);
				}

				try
				{
					foreach (var mail in mails)
					{
						try
						{
							var message = new MimeMessage();
							var senderMailAddress = new MailboxAddress(smtpSettings.Name, smtpSettings.Sender);
							message.From.Add(senderMailAddress);

							if (mail.ReplyTo != null && await fakeFilter.IsEmailValid(mail.ReplyTo.Address, _stopCts.Token))
							{
								string name = string.IsNullOrWhiteSpace(mail.ReplyTo.Name)
									? mail.ReplyTo.Address
									: mail.ReplyTo.Name;
								message.ReplyTo.Add(new MailboxAddress(name, mail.ReplyTo.Address));
							}

							bool isAnyRecipient = false;
							foreach (var recipient in mail.Recipients)
							{
								if (!await fakeFilter.IsEmailValid(recipient.Address, _stopCts.Token))
									continue;

								isAnyRecipient = true;

								string name = string.IsNullOrWhiteSpace(recipient.Name)
									? recipient.Address
									: recipient.Name;
								if (mail.RecipientsVisible)
								{
									message.To.Add(new MailboxAddress(name, recipient.Address));
								}
								else
								{
									message.Bcc.Add(new MailboxAddress(name, recipient.Address));
								}
							}

							if (!isAnyRecipient)
							{
								_logger.LogWarning($"Prevent sending mail ({mail.Id}): Blocked e-mail addresses (FakeFilter API)");
								mail.Status = EmailMessageStatus.Blocked;
								mail.UpdatedAt = DateTime.UtcNow;
								await dbContext.SaveChangesAsync(_stopCts.Token);

								await PushEmailUpdate(mail, _stopCts.Token);
								continue;
							}

							if (!mail.RecipientsVisible && smtpSettings.SendSelfOnBcc)
								message.To.Add(senderMailAddress);

							message.Importance = mail.Priority;
							message.Date = mail.SendAfterDate;
							message.Subject = mail.Subject;

							if (!string.IsNullOrWhiteSpace(smtpSettings.SubjectTag))
								message.Subject = $"[{smtpSettings.SubjectTag.Trim()}] {message.Subject}";

							var bodyBuilder = new BodyBuilder();
							if (mail.IsHtml)
							{
								bodyBuilder.HtmlBody = mail.Message;
							}
							else
							{
								bodyBuilder.TextBody = mail.Message;
							}

							var attachments = await dbContext.EmailAttachments
								.Where(a => a.EmailMessageId == mail.Id)
								.ToListAsync(_stopCts.Token);
							foreach (var attachment in attachments)
							{
								var att = bodyBuilder.Attachments.Add(attachment.FileName, attachment.Content);
								att.ContentDisposition = new ContentDisposition(ContentDisposition.Attachment);
								att.Headers.Add(HeaderId.ContentType, attachment.ContentType);
							}

							message.Body = bodyBuilder.ToMessageBody();

							await smtpClient.SendAsync(message, _stopCts.Token);

							mail.Status = EmailMessageStatus.Sent;
							mail.UpdatedAt = DateTime.UtcNow;
						}
						catch (Exception ex)
						{
							mail.Status = EmailMessageStatus.Error;
							mail.UpdatedAt = DateTime.UtcNow;

							_logger.LogError(ex, $"Sending mail ({mail.Id}) failed: {ex.GetMessage()}");
						}

						await dbContext.SaveChangesAsync(_stopCts.Token);
						await PushEmailUpdate(mail, _stopCts.Token);
					}
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, $"Updating mail staus failed: {ex.GetMessage()}");
				}
				finally
				{
					await smtpClient.DisconnectAsync(quit: true, cancellationToken: _stopCts.Token);
				}
			}
			catch (OperationCanceledException) when (_stopCts.IsCancellationRequested)
			{
				// application shutting down
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Sending mails failed: {ex.GetMessage()}");
			}
			finally
			{
				_timerMutex.Release();
			}
		}

		private async Task PushEmailUpdate(EmailMessage mail, CancellationToken cancellationToken = default)
		{
			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var hub = scope.ServiceProvider.GetService<IHubContext<WebClientHub>>();
				await hub.Clients.Group(WebClientHub.GROUP_ADMIN).SendAsync("EmailUpdate", new
				{
					mail.Id,
					mail.Status,
					mail.UpdatedAt
				}, cancellationToken);
			}
			catch
			{ /* keep things quiet */ }
		}
	}
}
