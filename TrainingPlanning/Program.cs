using System;
using System.IO;
using System.Linq;
using System.Reflection;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace AMWD.ASBHE.TrainingPlanning
{
	public class Program
	{
		public static string Service => "Training Planning";

		public static string Version { get; private set; }

		public static DateTime StartupTime { get; private set; }

		private static int Main(string[] args)
		{
			StartupTime = DateTime.UtcNow;

			var configBuilder = new ConfigurationBuilder();
			SetConfiguration(configBuilder, args);
			var config = configBuilder.Build();

			Log.Logger = new LoggerConfiguration()
				.ReadFrom.Configuration(config)
				.CreateLogger();

			AppDomain.CurrentDomain.UnhandledException += (s, ea) =>
			{
				string terminating = ea.IsTerminating ? " (terminating)" : "";
				if (ea.ExceptionObject is Exception ex)
				{
					Log.Fatal(ex, $"Unhandled exception ({ex.GetType().Name}) in AppDomain{terminating}: {ex.GetMessage()}");
				}
				else
				{
					Log.Fatal($"Unhandled exception ({ea.ExceptionObject.GetType().Name}) in AppDomain{terminating}: {ea.ExceptionObject}");
				}
			};

			Version = Assembly.GetExecutingAssembly()
				.GetCustomAttribute<AssemblyInformationalVersionAttribute>()
				.InformationalVersion;

			try
			{
				Console.WriteLine($"{Service} v{Version} is starting...");
				var host = CreateHostBuilder(args).Build();
				try
				{
					host.Start();
					Console.WriteLine($"{Service} v{Version} started");

					host.WaitForShutdown();
					Console.WriteLine($"{Service} v{Version} is shut down");
				}
				finally
				{
					host.Dispose();
				}
				return 0;
			}
			catch (Exception ex)
			{
				Log.Error(ex, $"Unhandled exception in Main: {ex.GetRecursiveMessage()}");
				Console.WriteLine($"{Service} v{Version} is shut down uncleanly");
				return 1;
			}
			finally
			{
				Log.CloseAndFlush();
			}
		}

		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			var hostBuilder = Host.CreateDefaultBuilder(args);
			hostBuilder.ConfigureWebHostDefaults(builder =>
			{
				builder.ConfigureAppConfiguration((_, configuration) =>
				{
					SetConfiguration(configuration, args);
				});
				builder.UseDefaultServiceProvider((_, options) =>
				{
#if DEBUG
					options.ValidateScopes = true;
#endif
				});
				builder.UseKestrel((hostingContext, options) =>
				{
					options.AddServerHeader = false;
					options.ConfigureEndpointDefaults(opts => opts.Protocols = HttpProtocols.Http1AndHttp2);
				});
				builder.UseStartup<Startup>();
			});
			hostBuilder.ConfigureServices(services =>
			{
				services.AddOptions<HostOptions>()
					.Configure(options => options.ShutdownTimeout = TimeSpan.FromSeconds(60));
			});
			hostBuilder.UseSerilog((hostingContext, configuration) =>
			{
				configuration.ReadFrom.Configuration(hostingContext.Configuration);
			});

			if (OperatingSystem.IsWindows())
				hostBuilder.UseWindowsService(options => options.ServiceName = Service);

			if (OperatingSystem.IsLinux())
				hostBuilder.UseSystemd();

			return hostBuilder;
		}

		private static void SetConfiguration(IConfigurationBuilder configuration, string[] args)
		{
			string baseDirectory = AppContext.BaseDirectory;
			if (baseDirectory.Contains("/debug", StringComparison.OrdinalIgnoreCase) || baseDirectory.Contains("/release", StringComparison.OrdinalIgnoreCase) ||
				baseDirectory.Contains("\\debug", StringComparison.OrdinalIgnoreCase) || baseDirectory.Contains("\\release", StringComparison.OrdinalIgnoreCase))
			{
				var root = new DirectoryInfo(baseDirectory);
				while (root.Parent != null)
				{
					if (root.GetFiles().Where(fi => fi.Name.EndsWith(".csproj")).Any())
					{
						baseDirectory = root.FullName;
						break;
					}
					root = root.Parent;
				}
			}

			string commonConfigurationDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "TrainingPlanning");

			configuration.SetBasePath(baseDirectory);
			configuration.AddIniFile("appsettings.ini", optional: true, reloadOnChange: false);
			configuration.AddIniFile("/etc/trainingplanning/settings.ini", optional: true, reloadOnChange: false);
			configuration.AddIniFile(Path.Combine(commonConfigurationDir, "settings.ini"), optional: true, reloadOnChange: false);

			configuration.AddEnvironmentVariables();

			if (args?.Any() == true)
				configuration.AddCommandLine(args);
		}
	}
}
