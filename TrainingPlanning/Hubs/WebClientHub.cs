﻿using System;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Security;
using Microsoft.AspNetCore.SignalR;

namespace AMWD.ASBHE.TrainingPlanning.Hubs
{
	public class WebClientHub : Hub
	{
		public const string GROUP_AUTHENTICATED = "__tp_auth";
		public const string GROUP_ADMIN = "__tp_auth_admin";

		public override async Task OnConnectedAsync()
		{
			await base.OnConnectedAsync();

			if (!Context.User.Identity.IsAuthenticated)
				return;

			var user = Context.GetHttpContext().GetAuthUser();
			if (user == null)
				return;

			await Groups.AddToGroupAsync(Context.ConnectionId, GROUP_AUTHENTICATED, Context.ConnectionAborted);
			if (user.IsAdmin)
				await Groups.AddToGroupAsync(Context.ConnectionId, GROUP_ADMIN, Context.ConnectionAborted);
		}

		public override async Task OnDisconnectedAsync(Exception exception)
		{
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, GROUP_ADMIN, Context.ConnectionAborted);
			await Groups.RemoveFromGroupAsync(Context.ConnectionId, GROUP_AUTHENTICATED, Context.ConnectionAborted);

			await base.OnDisconnectedAsync(exception);
		}

		public string Ping() => "Pong";

		public Task UpdateAvailableCount(params object[] args)
			=> Clients.Others.SendCoreAsync(nameof(UpdateAvailableCount), args, Context.ConnectionAborted);
	}
}
