﻿using System;
using AMWD.ASBHE.TrainingPlanning.Enums;
using MiniExcelLibs.Attributes;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class ParticipantExportModel
	{
		[ExcelColumnName("Vorname")]
		public string FirstName { get; set; }

		[ExcelColumnName("Nachname")]
		public string LastName { get; set; }

		[ExcelColumn(Name = "Geburtsdatum", Format = "dd.MM.yyyy")]
		public DateTime Birthday { get; set; }

		[ExcelColumnName("Geburtsort")]
		public string PlaceOfBirth { get; set; }

		[ExcelColumnName("Bereich")]
		public string DepartmentName { get; set; }

		[ExcelColumnName("E-Mail Adresse")]
		public string EmailAddress { get; set; }

		[ExcelColumnName("Telefon")]
		public string PhoneNumber { get; set; }

		[ExcelColumnName("Straße + Haus Nr.")]
		public string Street { get; set; }

		[ExcelColumnName("Adress-Zusatz")]
		public string AddressAddition { get; set; }

		[ExcelColumnName("PLZ")]
		public string ZipCode { get; set; }

		[ExcelColumnName("Stadt")]
		public string City { get; set; }

		[ExcelColumnName("Land")]
		public string Country { get; set; }

		[ExcelColumnName("Führerscheinklassen")]
		public string DriverLicenseClasses { get; set; }

		[ExcelColumnName("Fahrfreigabe")]
		public string DriverPermission { get; set; }

		[ExcelColumnName("Assistenzbedarf")]
		public string Assistance { get; set; }

		[ExcelColumnName("Essenswunsch")]
		public FoodSelection? FoodSelection { get; set; }

		[ExcelColumnName("Allergien")]
		public string Allergies { get; set; }

		[ExcelColumnName("DELETE")]
		public string Delete { get; set; }
	}
}
