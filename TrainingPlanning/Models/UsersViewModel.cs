﻿using System.Collections.Generic;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class UsersViewModel
	{
		public List<User> Users { get; set; }

		public List<Department> Departments { get; set; }

		#region User Entity

		public int Id { get; set; }

		public string EmailAddress { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public bool IsEnabled { get; set; }

		public bool IsAdmin { get; set; }

		public int? DepartmentId { get; set; }

		#endregion User Entity
	}
}
