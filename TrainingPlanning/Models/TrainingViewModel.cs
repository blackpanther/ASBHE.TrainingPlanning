﻿using System;
using System.Collections.Generic;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class TrainingViewModel
	{
		public List<Training> Trainings { get; set; }

		public List<TrainingSplit> Splits { get; set; }

		#region Entity

		public string Id { get; set; }

		public string Title { get; set; }

		public DateTime Start { get; set; }

		public DateTime End { get; set; }

		public int? DeadlineDays { get; set; }

		public int? ReminderDays { get; set; }

		public string SplitsJson { get; set; }

		public string Location { get; set; }

		public string Description { get; set; }

		public string ShortDescription { get; set; }

		public int? MinParticipants { get; set; }

		public int? MaxParticipants { get; set; }

		public string TargetAudience { get; set; }

		public string EntryRequirements { get; set; }

		public string Lecturers { get; set; }

		public string Costs { get; set; }

		public bool IsPublic { get; set; }

		public bool IsEnabled { get; set; }

		public bool IsCancelled { get; set; }

		public string Hints { get; set; }

		public int PartipicantCount { get; set; }

		public int? AvailableCount { get; set; }

		public bool CanRegister { get; set; }

		#endregion Entity
	}
}
