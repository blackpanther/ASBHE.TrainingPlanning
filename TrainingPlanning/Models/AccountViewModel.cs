﻿using AMWD.ASBHE.TrainingPlanning.Database.Entities;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class AccountViewModel
	{
		public string ReturnUrl { get; set; }

		public string EmailAddress { get; set; }

		public string Password { get; set; }

		#region User settings

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public bool IsEnabled { get; set; }

		public bool IsAdmin { get; set; }

		public Department Department { get; set; }

		#endregion User settings
	}
}
