﻿using System.Collections.Generic;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class DepartmentViewModel
	{
		public List<Department> Departments { get; set; }

		#region Department Entity

		public int Id { get; set; }

		public string Name { get; set; }

		public string Contact { get; set; }

		public string EmailAddress { get; set; }

		public string PhoneNumber { get; set; }

		public string Street { get; set; }

		public string AddressAddition { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public bool IsDeleted { get; set; }

		#endregion Department Entity
	}
}
