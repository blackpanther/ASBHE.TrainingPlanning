namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class ErrorViewModel
	{
		public string Title { get; set; }

		public string Description { get; set; }

		public int ErrorCode { get; set; }

		public string Icon { get; set; }
	}
}
