﻿using System;
using System.Collections.Generic;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Enums;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class ParticipantsViewModel
	{
		public List<Department> Departments { get; set; }

		public List<Participant> Participants { get; set; }

		#region Participant

		public int Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime Birthday { get; set; }

		public string PlaceOfBirth { get; set; }

		public int? DepartmentId { get; set; }

		public string EmailAddress { get; set; }

		public string PhoneNumber { get; set; }

		public string Street { get; set; }

		public string AddressAddition { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public string DriverLicenseClasses { get; set; }

		public bool DriverPermission { get; set; }

		public string Allergies { get; set; }

		public FoodSelection? FoodSelection { get; set; }

		public bool IsEnabled { get; set; }

		public bool AssistanceRequired { get; set; }

		#endregion Participant
	}
}
