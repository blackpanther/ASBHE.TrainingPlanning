﻿using System;
using System.Collections.Generic;
using AMWD.ASBHE.TrainingPlanning.Enums;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class TrainingParticipantsViewModel
	{
		public string Id { get; set; }

		public string TrainingTitle { get; set; }

		public DateTime TrainingStart { get; set; }

		public DateTime TrainingEnd { get; set; }

		public List<TrainingParticipant> Participants { get; set; }
	}

	public class TrainingParticipant
	{
		#region Participant

		public int Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime Birthday { get; set; }

		public string PlaceOfBirth { get; set; }

		public string EmailAddress { get; set; }

		public string PhoneNumber { get; set; }

		public bool AssistanceRequired { get; set; }

		public FoodSelection? FoodSelection { get; set; }

		public string Allergies { get; set; }

		#endregion Participant

		#region Mapping

		public HotelRequirement HotelRequirement { get; set; }

		public bool AssumptionOfCosts { get; set; }

		public string InvoiceName { get; set; }

		public string InvoiceStreet { get; set; }

		public string InvoiceZipCode { get; set; }

		public string InvoiceCity { get; set; }

		public string InvoiceCountry { get; set; }

		public string Notes { get; set; }

		public ParticipantTrainingStatus Status { get; set; }

		#endregion Mapping

		#region Comparison

		public string ParticipantDepartmentName { get; set; }

		public string RegistrationDepartmentName { get; set; }

		public string ExternalDepartment { get; set; }

		public string ParticipantStreet { get; set; }

		public string RegistrationStreet { get; set; }

		public string ParticipantAddition { get; set; }

		public string RegistrationAddition { get; set; }

		public string ParticipantZipCode { get; set; }

		public string RegistrationZipCode { get; set; }

		public string ParticipantCity { get; set; }

		public string RegistrationCity { get; set; }

		public string ParticipantCountry { get; set; }

		public string RegistrationCountry { get; set; }

		public string ParticipantDriverLicenseClasses { get; set; }

		public string RegistrationDriverLicenseClasses { get; set; }

		public bool ParticipantDriverPermission { get; set; }

		public bool RegistrationDriverPermission { get; set; }

		#endregion Comparison
	}
}
