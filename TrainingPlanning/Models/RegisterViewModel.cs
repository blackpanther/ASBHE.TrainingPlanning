﻿using System;
using AMWD.ASBHE.TrainingPlanning.Enums;

namespace AMWD.ASBHE.TrainingPlanning.Models
{
	public class RegisterViewModel
	{
		public string TrainingId { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string EmailAddress { get; set; }

		public string Phone { get; set; }

		public DateTime Birthday { get; set; }

		public string PlaceOfBirth { get; set; }

		public string Department { get; set; }

		public string Street { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string AddressAddition { get; set; }

		public string Country { get; set; }

		public string DriverLicenseClasses { get; set; }

		public bool DriverPermission { get; set; }

		public bool AssistanceRequired { get; set; }

		public HotelRequirement HotelRequirement { get; set; }

		public FoodSelection? FoodSelection { get; set; }

		public string Allergies { get; set; }

		public bool AssumptionOfCosts { get; set; }

		public string InvoiceName { get; set; }

		public string InvoiceStreet { get; set; }

		public string InvoiceZipCode { get; set; }

		public string InvoiceCity { get; set; }

		public bool TermsAccepted { get; set; }
	}
}
