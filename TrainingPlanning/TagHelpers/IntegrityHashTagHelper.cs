﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.Extensions.Configuration;

namespace AMWD.ASBHE.TrainingPlanning.TagHelpers
{
	/// <summary>
	/// A tag helper to dynamically create integrity checks for linked sources.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/TagHelpers/IntegrityHashTagHelper.cs">AM.WD Common</see>.
	/// </remarks>
	[HtmlTargetElement("link")]
	[HtmlTargetElement("script")]
	public class IntegrityHashTagHelper : TagHelper
	{
		private readonly IWebHostEnvironment _env;
		private readonly string _hostUrl;

		/// <summary>
		/// Initializes a new instance of the <see cref="IntegrityHashTagHelper"/> class.
		/// </summary>
		/// <param name="env">The web host environment.</param>
		/// <param name="configuration">The application configuration.</param>
		public IntegrityHashTagHelper(IWebHostEnvironment env, IConfiguration configuration)
		{
			_env = env;
			_hostUrl = configuration.GetValue("ASPNETCORE_APPL_URL", "http://localhost/");
		}

		/// <summary>
		/// Gets or sets a value indicating whether the integrity should be calculated.
		/// </summary>
		[HtmlAttributeName("asp-integrity")]
		public bool IsIntegrityEnabled { get; set; }

		/// <summary>
		/// Gets or sets the hash strength to use.
		/// </summary>
		[HtmlAttributeName("asp-integrity-strength")]
		public int IntegrityStrength { get; set; }

		/// <inheritdoc/>
		public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
		{
			if (context.AllAttributes
				.Where(a => a.Name.Equals("integrity", StringComparison.OrdinalIgnoreCase))
				.Any())
				return;

			if (!IsIntegrityEnabled)
				return;

			string source = null;
			switch (context.TagName.ToLower())
			{
				case "link":
					var rel = context.AllAttributes.Where(a => a.Name.ToLower() == "rel").FirstOrDefault();
					if (rel == null || rel.Value.ToString().ToLower() == "stylesheet")
					{
						var href = context.AllAttributes.Where(a => a.Name.ToLower() == "href").FirstOrDefault();
						source = href?.Value?.ToString().Trim();
					}
					break;
				case "script":
					var src = context.AllAttributes.Where(a => a.Name.ToLower() == "src").FirstOrDefault();
					source = src?.Value?.ToString().Trim();
					break;
			}

			// no source given, no hash to calculate.
			if (string.IsNullOrWhiteSpace(source))
				return;

			byte[] fileBytes = null;
			if (source.StartsWith("http") || source.StartsWith("//"))
			{
				if (source.StartsWith("//"))
					source = $"http:{source}";

				try
				{
					using var client = new HttpClient();

					if (!string.IsNullOrWhiteSpace(_hostUrl))
						client.DefaultRequestHeaders.Referrer = new Uri(_hostUrl);

					var response = await client.GetAsync(source);
					fileBytes = await response.Content.ReadAsByteArrayAsync();
				}
				catch
				{
					return;
				}
			}
			else
			{
				if (source.StartsWith("~"))
					source = source[1..];

				if (source.StartsWith("/"))
					source = source[1..];

				if (source.Contains('?'))
					source = source[..source.IndexOf("?")];

				try
				{
					string path = Path.Combine(_env.WebRootPath, source);
					fileBytes = await File.ReadAllBytesAsync(path);
				}
				catch
				{
					return;
				}
			}

			string type;
			byte[] hashBytes = new byte[0];
			switch (IntegrityStrength)
			{
				case 512:
					type = "sha512";
					using (var sha = SHA512.Create())
					{
						hashBytes = sha.ComputeHash(fileBytes);
					}
					break;
				case 384:
					type = "sha384";
					using (var sha = SHA384.Create())
					{
						hashBytes = sha.ComputeHash(fileBytes);
					}
					break;
				default:   // 256
					type = "sha256";
					using (var sha = SHA256.Create())
					{
						hashBytes = sha.ComputeHash(fileBytes);
					}
					break;
			}
			string hash = Convert.ToBase64String(hashBytes);

			output.Attributes.RemoveAll("integrity");
			output.Attributes.RemoveAll("crossorigin");

			output.Attributes.Add(new TagHelperAttribute("integrity", new HtmlString($"{type}-{hash}")));
			output.Attributes.Add(new TagHelperAttribute("crossorigin", new HtmlString("anonymous")));
		}

		/// <inheritdoc/>
		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			// ensure leaving context to prevent a deadlock.
			var task = Task.Run(() => ProcessAsync(context, output));
			task.Wait();
		}
	}
}
