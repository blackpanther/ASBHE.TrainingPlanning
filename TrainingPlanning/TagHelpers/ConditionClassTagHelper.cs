﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace AMWD.ASBHE.TrainingPlanning.TagHelpers
{
	/// <summary>
	/// A tag helper that adds a CSS class attribute based on a condition.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/TagHelpers/ConditionClassTagHelper.cs">AM.WD Common</see>.
	/// </remarks>
	[HtmlTargetElement(Attributes = ClassPrefix + "*")]
	public class ConditionClassTagHelper : TagHelper
	{
		private const string ClassPrefix = "condition-class-";

		private IDictionary<string, bool> _classValues;

		/// <summary>
		/// Gets or sets the unconditional CSS class attribute value of the element.
		/// </summary>
		[HtmlAttributeName("class")]
		public string CssClass { get; set; }

		/// <summary>
		/// Gets or sets a dictionary containing all conditional class names and a boolean condition
		/// value indicating whether the class should be added to the element.
		/// </summary>
		[HtmlAttributeName("", DictionaryAttributePrefix = ClassPrefix)]
		public IDictionary<string, bool> ClassValues
		{
			get
			{
				return _classValues ??= new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);
			}
			set
			{
				_classValues = value;
			}
		}

		/// <summary>
		/// Synchronously executes the <see cref="T:Microsoft.AspNetCore.Razor.TagHelpers.TagHelper"/>
		/// with the given <paramref name="context"/> and <paramref name="output"/>.
		/// </summary>
		/// <param name="context">Contains information associated with the current HTML tag.</param>
		/// <param name="output">A stateful HTML element used to generate an HTML tag.</param>
		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			var items = _classValues
				.Where(e => e.Value)
				.Select(e => e.Key)
				.ToList();

			if (!string.IsNullOrEmpty(CssClass))
				items.Insert(0, CssClass);

			if (items.Any())
			{
				string classes = string.Join(" ", items.ToArray());
				output.Attributes.Add("class", classes);
			}
		}
	}
}
