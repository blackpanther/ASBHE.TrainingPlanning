﻿using System;
using System.Data.Common;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extensions for the <see cref="DatabaseFacade"/>.
	/// </summary>
	/// <remarks>
	/// Copied from
	/// <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.EntityFrameworkCore/Extensions/DatabaseFacadeExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	public static class DatabaseFacadeExtensions
	{
		/// <summary>
		/// Waits until the database connection is available.
		/// </summary>
		/// <param name="database">The database connection.</param>
		/// <param name="optionsAction">An action to set additional options.</param>
		/// <param name="cancellationToken">A cancellation token.</param>
		/// <returns>An awaitable task to wait until the database is available.</returns>
		public static async Task WaitAvailableAsync(this DatabaseFacade database, ILogger logger = null, CancellationToken cancellationToken = default)
		{
			if (database == null)
				throw new ArgumentNullException(nameof(database));

			if (database.GetProviderType() == DatabaseProvider.InMemory)
				return;

			logger?.LogInformation("Waiting for a database connection");
			var connection = database.GetDbConnection();
			while (!cancellationToken.IsCancellationRequested)
			{
				try
				{
					await connection.OpenAsync(cancellationToken);
					logger?.LogInformation("Database connection available");
					return;
				}
				catch
				{
					// keep things quiet
					try
					{
						await Task.Delay(TimeSpan.FromSeconds(3), cancellationToken);
					}
					catch (OperationCanceledException) when (cancellationToken.IsCancellationRequested)
					{
						return;
					}
					catch
					{
						// keep things quiet
					}
				}
				finally
				{
					await connection.CloseAsync();
				}
			}
		}

		internal static DatabaseProvider GetProviderType(this DatabaseFacade database)
			=> GetProviderType(database.ProviderName);

		private static DatabaseProvider GetProviderType(this DbConnection connection)
			=> GetProviderType(connection.GetType().FullName);

		private static DatabaseProvider GetProviderType(string provider)
		{
			if (provider.Contains("mysql", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.MySQL;
			if (provider.Contains("oracle", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.Oracle;
			if (provider.Contains("npgsql", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.PostgreSQL;
			if (provider.Contains("sqlite", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.SQLite;
			if (provider.Contains("sqlclient", StringComparison.OrdinalIgnoreCase)
				|| provider.Contains("sqlserver", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.SQLServer;
			if (provider.Contains("inmemory", StringComparison.OrdinalIgnoreCase))
				return DatabaseProvider.InMemory;

			throw new DatabaseProviderException($"The database provider '{provider}' is unknown");
		}

		internal enum DatabaseProvider
		{
			MySQL = 1,
			Oracle = 2,
			PostgreSQL = 3,
			SQLite = 4,
			SQLServer = 5,
			InMemory = 6,
		}
	}

	/// <summary>
	/// A DatabaseProvider specific exception.
	/// </summary>
	public class DatabaseProviderException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseProviderException"/> class.
		/// </summary>
		public DatabaseProviderException()
			: base()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseProviderException"/> class
		/// with a specified error message.
		/// </summary>
		/// <param name="message">The message that describes the error.</param>
		public DatabaseProviderException(string message)
			: base(message)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseProviderException"/> class
		/// with a specified error message and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference if no inner exception is specified.</param>
		public DatabaseProviderException(string message, Exception innerException)
			: base(message, innerException)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="DatabaseProviderException"/> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="ArgumentNullException">The info parameter is null.</exception>
		/// <exception cref="SerializationException">The class name is null or <see cref="Exception.HResult"/> is zero (0).</exception>
		protected DatabaseProviderException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{ }
	}
}
