﻿using System.Net;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extensions for the <see cref="HttpContext"/>.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/Extensions/HttpContextExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	public static class HttpContextExtensions
	{
		/// <summary>
		/// Retrieves the antiforgery token.
		/// </summary>
		/// <param name="httpContext">The current <see cref="HttpContext"/>.</param>
		/// <returns>Name and value of the token.</returns>
		public static (string FormName, string HeaderName, string Value) GetAntiforgeryToken(this HttpContext httpContext)
		{
			var af = httpContext.RequestServices.GetService<IAntiforgery>();
			var set = af?.GetAndStoreTokens(httpContext);

			return (set?.FormFieldName, set?.HeaderName, set?.RequestToken);
		}

		/// <summary>
		/// Returns the remote ip address.
		/// </summary>
		/// <param name="httpContext">The current <see cref="HttpContext"/>.</param>
		/// <param name="headerName">The name of the header to resolve the <see cref="IPAddress"/> when behind a proxy (Default: X-Forwarded-For).</param>
		/// <returns>The ip address of the client.</returns>
		public static IPAddress GetRemoteIpAddress(this HttpContext httpContext, string headerName = "X-Forwarded-For")
		{
			string forwardedHeader = httpContext.Request.Headers[headerName].ToString();
			if (!string.IsNullOrWhiteSpace(forwardedHeader) && IPAddress.TryParse(forwardedHeader, out var forwarded))
				return forwarded;

			return httpContext.Connection.RemoteIpAddress;
		}

		/// <summary>
		/// Returns whether the request was made locally.
		/// </summary>
		/// <param name="httpContext">The current <see cref="HttpContext"/>.</param>
		/// <param name="headerName">The name of the header to resolve the <see cref="IPAddress"/> when behind a proxy (Default: X-Forwarded-For).</param>
		/// <returns></returns>
		public static bool IsLocalRequest(this HttpContext httpContext, string headerName = "X-Forwarded-For")
		{
			var remoteIpAddress = httpContext.GetRemoteIpAddress(headerName);
			return httpContext.Connection.LocalIpAddress.Equals(remoteIpAddress);
		}

		/// <summary>
		/// Tries to retrieve the return url.
		/// </summary>
		/// <param name="httpContext">The current <see cref="HttpContext"/>.</param>
		/// <returns></returns>
		public static string GetReturnUrl(this HttpContext httpContext)
		{
			if (httpContext.Items.ContainsKey("OriginalRequest"))
				return httpContext.Items["OriginalRequest"].ToString();

			if (httpContext.Request.Query.ContainsKey("ReturnUrl"))
				return httpContext.Request.Query["ReturnUrl"].ToString();

			return null;
		}

		/// <summary>
		/// Clears a session when available.
		/// </summary>
		/// <param name="httpContext">The current <see cref="HttpContext"/>.</param>
		public static void ClearSession(this HttpContext httpContext)
			=> httpContext.Session?.Clear();
	}
}
