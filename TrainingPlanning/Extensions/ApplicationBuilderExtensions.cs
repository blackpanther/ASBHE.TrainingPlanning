﻿using System;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extensions for the <see cref="IApplicationBuilder"/>.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/Extensions/ApplicationBuilderExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	public static class ApplicationBuilderExtensions
	{
		/// <summary>
		/// Adds settings to run behind a reverse proxy (e.g. NginX).
		/// </summary>
		/// <remarks>
		/// A base path (e.g. running in a sub-directory /app) for the application is defined via <c>ASPNETCORE_APPL_PATH</c> environment variable.
		/// <br/>
		/// <br/>
		/// Additionally you can specify the proxy server by using <paramref name="address"/> or a <paramref name="network"/> when there are multiple proxy servers.
		/// <br/>
		/// When no <paramref name="address"/> oder <paramref name="network"/> is set, the default IPv4 private subnets are configured:<br/>
		/// - <c>10.0.0.0/8</c><br/>
		/// - <c>172.16.0.0/12</c><br/>
		/// - <c>192.168.0.0/16</c>
		/// </remarks>
		/// <param name="app">The application builder.</param>
		/// <param name="network">The <see cref="IPNetwork"/> where proxy requests are received from (optional).</param>
		/// <param name="address">The <see cref="IPAddress"/> where proxy requests are received from (optional).</param>
		public static void UseProxyHosting(this IApplicationBuilder app, IPNetwork network = null, IPAddress address = null)
		{
			string path = Environment.GetEnvironmentVariable("ASPNETCORE_APPL_PATH");
			if (!string.IsNullOrWhiteSpace(path))
				app.UsePathBase(new PathString(path));

			var options = new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All };
			options.KnownProxies.Clear();
			options.KnownNetworks.Clear();

			if (network == null && address == null)
			{
				options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("10.0.0.0"), 8));
				options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("172.16.0.0"), 12));
				options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("192.168.0.0"), 16));
			}

			if (network != null)
				options.KnownNetworks.Add(network);

			if (address != null)
				options.KnownProxies.Add(address);

			app.UseForwardedHeaders(options);
		}
	}
}
