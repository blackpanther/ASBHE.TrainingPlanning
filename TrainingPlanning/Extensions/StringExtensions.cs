﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// String extensions.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common/Extensions/StringExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	internal static class StringExtensions
	{
		private static readonly int _mxRecordType = 15;
		private static readonly IPEndPoint[] _dnsResolvers = new[]
		{
			new IPEndPoint(IPAddress.Parse("1.1.1.1"), 53),
			new IPEndPoint(IPAddress.Parse("8.8.8.8"), 53),
			new IPEndPoint(IPAddress.Parse("9.9.9.9"), 53),
		};

		/// <summary>
		/// Replaces the search substring with the replacement when it was found at the beginning of the string.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="search">The searched substring.</param>
		/// <param name="replacement">The replacement.</param>
		/// <returns></returns>
		public static string ReplaceStart(this string str, string search, string replacement)
		{
			if (str.StartsWith(search))
				return replacement + str[search.Length..];

			return str;
		}

		/// <summary>
		/// Replaces the search substring with the replacement when it was found at the end of the string.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="search">The searched substring.</param>
		/// <param name="replacement">The replacement.</param>
		/// <returns></returns>
		public static string ReplaceEnd(this string str, string search, string replacement)
		{
			if (str.EndsWith(search))
				return str[..^search.Length] + replacement;

			return str;
		}

		/// <summary>
		/// Checks whether the given <see cref="string"/> is a valid <see cref="MailAddress"/>.
		/// </summary>
		/// <remarks>
		/// You can enhance the check by requesting the MX record of the domain.
		/// </remarks>
		/// <param name="email">The email address as string to validate.</param>
		/// <param name="checkForDnsRecord">A value indicating whether to resolve a MX record (Google DNS is used).</param>
		/// <returns><c>true</c> when the email address is valid, other wise <c>false</c>.</returns>
		public static bool IsValidEmailAddress(this string email, bool checkForDnsRecord = false)
			=> email.IsValidEmailAddress(checkForDnsRecord ? _dnsResolvers : null);

		/// <summary>
		/// Checks whether the given <see cref="string"/> is a valid <see cref="MailAddress"/>.
		/// </summary>
		/// <remarks>
		/// The check is enhanced by a request for MX records on the defined <paramref name="nameservers"/>.
		/// <br/>
		/// The DNS resolution is only used when the DNS NuGet package is available.
		/// See: https://www.nuget.org/packages/DNS/7.0.0
		/// </remarks>
		/// <param name="emailAddress">The email address as string to validate.</param>
		/// <param name="nameservers">A list of <see cref="IPEndPoint"/>s of nameservers.</param>
		/// <returns><c>true</c> when the email address is valid, other wise <c>false</c>.</returns>
		public static bool IsValidEmailAddress(this string emailAddress, IEnumerable<IPEndPoint> nameservers)
		{
			try
			{
				var mailAddress = new MailAddress(emailAddress);
				bool isValid = mailAddress.Address == emailAddress;

				if (isValid && nameservers?.Any() == true)
				{
					var dnsClientType = Type.GetType("DNS.Client.DnsClient, DNS")
						?? throw new DllNotFoundException("The DNS NuGet package is required: https://www.nuget.org/packages/DNS/7.0.0");
					var recordTypeType = Type.GetType("DNS.Protocol.RecordType, DNS");
					var resolveMethodInfo = dnsClientType.GetMethod("Resolve", new[] { typeof(string), recordTypeType, typeof(CancellationToken) });

					bool exists = false;
					foreach (var nameserver in nameservers)
					{
						object dnsClient = Activator.CreateInstance(dnsClientType, new object[] { nameserver });

						var waitTask = Task.Run(async () => await resolveMethodInfo.InvokeAsync<object>(dnsClient, new object[] { mailAddress.Host, _mxRecordType, CancellationToken.None }));
						waitTask.Wait();

						object response = waitTask.Result;
						waitTask.Dispose();

						int responseCode = (int)response.GetType().GetProperty("ResponseCode").GetValue(response);
						if (responseCode != 0)
							continue;

						object list = response.GetType().GetProperty("AnswerRecords").GetValue(response);
						foreach (object item in list as IEnumerable)
						{
							int type = (int)item.GetType().GetProperty("Type").GetValue(item);
							if (type == _mxRecordType)
							{
								exists = true;
								break;
							}
						}

						if (exists)
							break;
					}

					isValid &= exists;
				}

				return isValid;
			}
			catch
			{
				return false;
			}
		}
	}
}
