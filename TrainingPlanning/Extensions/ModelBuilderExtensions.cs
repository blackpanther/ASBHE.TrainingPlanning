﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extensions for the <see cref="ModelBuilder"/> of entity framework core.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.EntityFrameworkCore/Extensions/ModelBuilderExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	internal static class ModelBuilderExtensions
	{
		/// <summary>
		/// Converts all table and column names to snake_case_names.
		/// </summary>
		/// <param name="builder">The database model builder.</param>
		/// <returns>A reference to this instance after the operation has completed.</returns>
		public static ModelBuilder ApplySnakeCase(this ModelBuilder builder)
		{
			foreach (var entityType in builder.Model.GetEntityTypes())
			{
				// skip conversion when table name is explicitly set
				if ((entityType.ClrType.GetCustomAttribute(typeof(TableAttribute), false) as TableAttribute) == null)
					entityType.SetTableName(ConvertToSnakeCase(entityType.GetTableName()));

				var identifier = StoreObjectIdentifier.Table(entityType.GetTableName(), entityType.GetSchema());
				foreach (var property in entityType.GetProperties())
				{
					// skip conversion when column name is explicitly set
					if ((entityType.ClrType.GetProperty(property.Name)?.GetCustomAttribute(typeof(ColumnAttribute), false) as ColumnAttribute) == null)
						property.SetColumnName(ConvertToSnakeCase(property.GetColumnName(identifier)));
				}
			}

			return builder;
		}

		/// <summary>
		/// Converts a string to its snake_case equivalent.
		/// </summary>
		/// <remarks>
		/// Code borrowed from Npgsql.NameTranslation.NpgsqlSnakeCaseNameTranslator.
		/// See <see href="https://github.com/npgsql/npgsql/blob/f2b2c98f45df6d2a78eec00ae867f18944d717ca/src/Npgsql/NameTranslation/NpgsqlSnakeCaseNameTranslator.cs#L76-L136" />.
		/// </remarks>
		/// <param name="value">The value to convert.</param>
		private static string ConvertToSnakeCase(string value)
		{
			var sb = new StringBuilder();
			var state = SnakeCaseState.Start;

			for (int i = 0; i < value.Length; i++)
			{
				if (value[i] == ' ')
				{
					if (state != SnakeCaseState.Start)
						state = SnakeCaseState.NewWord;
				}
				else if (char.IsUpper(value[i]))
				{
					switch (state)
					{
						case SnakeCaseState.Upper:
							bool hasNext = i + 1 < value.Length;
							if (i > 0 && hasNext)
							{
								char nextChar = value[i + 1];
								if (!char.IsUpper(nextChar) && nextChar != '_')
								{
									sb.Append('_');
								}
							}
							break;

						case SnakeCaseState.Lower:
						case SnakeCaseState.NewWord:
							sb.Append('_');
							break;
					}

					sb.Append(char.ToLowerInvariant(value[i]));
					state = SnakeCaseState.Upper;
				}
				else if (value[i] == '_')
				{
					sb.Append('_');
					state = SnakeCaseState.Start;
				}
				else
				{
					if (state == SnakeCaseState.NewWord)
						sb.Append('_');

					sb.Append(value[i]);
					state = SnakeCaseState.Lower;
				}
			}

			return sb.ToString();
		}

		private enum SnakeCaseState
		{
			Start,
			Lower,
			Upper,
			NewWord
		}
	}
}
