﻿using System;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Provides extension methods for date and time manipulation.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common/Extensions/DateTimeExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	internal static class TimeSpanExtensions
	{
		/// <summary>
		/// Aligns the <see cref="TimeSpan"/> to the local clock and respects daylight saving time.
		/// </summary>
		/// <param name="timeSpan">The timespan to align.</param>
		/// <param name="now">The reference time.</param>
		/// <param name="offset">A specific offset to the timespan.</param>
		/// <returns>The timespan until the aligned time.</returns>
		public static TimeSpan GetAlignedInterval(this TimeSpan timeSpan, DateTime? now = null, TimeSpan offset = default)
		{
			if (now == null)
				now = DateTime.UtcNow;

			var nowWithOffset = new DateTimeOffset(now.Value);

			var nextTime = new DateTime(nowWithOffset.Ticks / timeSpan.Ticks * timeSpan.Ticks, now.Value.Kind).Add(offset);
			var nextTimeWithOffset = new DateTimeOffset(nextTime);

			if (nextTimeWithOffset <= nowWithOffset)
				nextTimeWithOffset = nextTimeWithOffset.Add(timeSpan);

			if (now.Value.Kind == DateTimeKind.Local)
				return nextTimeWithOffset.LocalDateTime - nowWithOffset.LocalDateTime;

			return nextTimeWithOffset - nowWithOffset;
		}
	}
}
