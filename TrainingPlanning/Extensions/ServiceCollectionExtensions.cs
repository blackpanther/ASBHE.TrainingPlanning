﻿using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extensions for the <see cref="IServiceCollection"/>.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common.AspNetCore/Extensions/ServiceCollectionExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	internal static class ServiceCollectionExtensions
	{
		/// <summary>
		/// Adds a hosted service that is instanciated only once.
		/// </summary>
		/// <typeparam name="TService">The type of the service to add.</typeparam>
		/// <param name="services">The <see cref="IServiceCollection"/> to add the service to.</param>
		/// <returns>A reference to this instance after the operation has completed.</returns>
		public static IServiceCollection AddSingletonHostedService<TService>(this IServiceCollection services)
			where TService : class, IHostedService
		{
			services.AddSingleton<TService>();
			services.AddSingleton<IHostedService, BackgroundServiceStarter<TService>>();
			return services;
		}

		/// <summary>
		/// Adds a hosted service that is instanciated only once.
		/// </summary>
		/// <typeparam name="TService">The type of the service to add.</typeparam>
		/// <typeparam name="TImplementation">The type of the implementation of the service to add.</typeparam>
		/// <param name="services">The <see cref="IServiceCollection"/> to add the service to.</param>
		/// <returns>A reference to this instance after the operation has completed.</returns>
		public static IServiceCollection AddSingletonHostedService<TService, TImplementation>(this IServiceCollection services)
			where TService : class, IHostedService where TImplementation : class, TService
		{
			services.AddSingleton<TService, TImplementation>();
			services.AddSingleton<IHostedService, BackgroundServiceStarter<TService>>();

			return services;
		}
	}
}
