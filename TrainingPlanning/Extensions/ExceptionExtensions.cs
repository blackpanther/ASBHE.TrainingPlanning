﻿using System;
using System.Linq;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Provides extension methods for exceptions.
	/// </summary>
	/// <remarks>
	/// Copied from <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common/Extensions/ExceptionExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	internal static class ExceptionExtensions
	{
		/// <summary>
		/// Returns the message of the inner exception if exists otherwise the message of the exception itself.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <returns>The message of the inner exception or the exception itself.</returns>
		public static string GetMessage(this Exception exception)
			=> exception.InnerException?.Message ?? exception.Message;

		/// <summary>
		/// Returns the message of the exception and its inner exceptions.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <returns>The message of the <paramref name="exception"/> and all its inner exceptions.</returns>
		public static string GetRecursiveMessage(this Exception exception)
		{
			if (exception is AggregateException aggregateEx)
			{
				return aggregateEx.InnerExceptions
					.Take(3)
					.Select(ex => ex.GetRecursiveMessage())
					.Aggregate((a, b) => a + " " + b);
			}
			if (exception.InnerException != null)
			{
				string message = exception.Message;
				message = message.ReplaceEnd(" See the inner exception for details.", "");
				return message + " " + exception.InnerException.GetRecursiveMessage();
			}
			return exception.Message;
		}
	}
}
