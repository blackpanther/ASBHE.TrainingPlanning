﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AMWD.ASBHE.TrainingPlanning.Extensions
{
	/// <summary>
	/// Extend the enum values by attribute driven methods.
	/// </summary>
	/// <remarks>
	/// Copied from
	/// <see href="https://git.am-wd.de/am.wd/common/-/blob/cb133aea0c9baf9ddb7c62b3c1f447ee5b3cfc95/AMWD.Common/Extensions/EnumExtensions.cs">AM.WD Common</see>.
	/// </remarks>
	public static class EnumExtensions
	{
		/// <summary>
		/// Returns a list of specific attribute type from a enum-value.
		/// </summary>
		/// <typeparam name="TAttribute">The attribute type.</typeparam>
		/// <param name="value">The enum value.</param>
		/// <returns>The attributes or null.</returns>
		public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this Enum value)
		{
			var fieldInfo = value.GetType().GetField(value.ToString());
			if (fieldInfo == null)
				return Array.Empty<TAttribute>();

			return fieldInfo.GetCustomAttributes(typeof(TAttribute), inherit: false).Cast<TAttribute>();
		}

		/// <summary>
		/// Returns a specific attribute from a enum-value.
		/// </summary>
		/// <typeparam name="TAttribute">The attribute type.</typeparam>
		/// <param name="value">The enum value.</param>
		/// <returns>The attribute or null.</returns>
		public static TAttribute GetAttribute<TAttribute>(this Enum value)
			=> value.GetAttributes<TAttribute>().FirstOrDefault();

		/// <summary>
		/// Returns the description from <see cref="DescriptionAttribute"/>.
		/// </summary>
		/// <param name="value">The enum value.</param>
		/// <returns>The description or the string representation of the value.</returns>
		public static string GetDescription(this Enum value)
			=> value.GetAttribute<DescriptionAttribute>()?.Description ?? value.ToString();

		/// <summary>
		/// Returns the name from <see cref="DisplayAttribute"/>.
		/// </summary>
		/// <param name="value">The enum value.</param>
		/// <returns>The display name or the string representation of the value.</returns>
		public static string GetDisplayName(this Enum value)
			=> value.GetAttribute<DisplayAttribute>()?.Name ?? value.ToString();
	}
}
