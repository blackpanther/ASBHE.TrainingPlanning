﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Hubs;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AMWD.ASBHE.TrainingPlanning
{
	public class Startup
	{
		private readonly IConfiguration _configuration;

		internal static string WebRootPath { get; private set; }

		internal static string PersistentDataDirectory { get; private set; }

		internal static long? SignalRMaxMessageSize { get; private set; }

		public Startup(IWebHostEnvironment env, IConfiguration configuration)
		{
			_configuration = configuration;
			WebRootPath = env.WebRootPath;

			string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			PersistentDataDirectory = configuration.GetValue<string>("Hosting:PersistentDataDirectory");
			if (string.IsNullOrWhiteSpace(PersistentDataDirectory))
				PersistentDataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "TrainingPlanning");

			if (!Path.IsPathRooted(PersistentDataDirectory))
				PersistentDataDirectory = Path.Combine(dir, PersistentDataDirectory);

			if (!Directory.Exists(PersistentDataDirectory))
				Directory.CreateDirectory(PersistentDataDirectory);
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<ServerDbContext>(options =>
			{
#if DEBUG
				options.EnableSensitiveDataLogging();
#endif
				switch (_configuration.GetValue<string>("Database:Provider"))
				{
					case "InMemory":
						options.UseInMemoryDatabase(Program.Service);
						break;
					case "SQLite":
						options.UseSqlite(_configuration.GetValue<string>("Database:ConnectionString"));
						break;
					case "PostgreSQL":
						options.UseNpgsql(_configuration.GetValue<string>("Database:ConnectionString"));
						break;
					case "MySQL":
						options.UseMySql(_configuration.GetValue<string>("Database:ConnectionString"), ServerVersion.AutoDetect(_configuration.GetValue<string>("Database:ConnectionString")));
						break;
					case "SQLServer":
						options.UseSqlServer(_configuration.GetValue<string>("Database:ConnectionString"));
						break;
					default:
						throw new NotSupportedException($"The database provider {_configuration.GetValue<string>("Database:Provider")} is not supported");
				}
			});
			if (!string.IsNullOrWhiteSpace(LogDbContext.GetPath(_configuration)))
			{
				services.AddDbContext<LogDbContext>(options =>
				{
#if DEBUG
					options.EnableSensitiveDataLogging();
#endif
					options.UseSqlite(LogDbContext.GetConnectionString(_configuration));
				});
			}

			//			services.AddMemoryCache(options =>
			//			{
			//				options.SizeLimit = null;
			//				options.ExpirationScanFrequency = TimeSpan.FromSeconds(5);
			//			});

			services.AddResponseCompression(options =>
			{
				//options.EnableForHttps = true;
				options.MimeTypes = ResponseCompressionDefaults.MimeTypes
					.Concat(new[]
					{
						"image/svg",
						"image/svg+xml",
						"image/x-icon",
						"application/manifest+json"
					});
			});
			services.Configure<RouteOptions>(options =>
			{
				options.LowercaseUrls = true;
				options.LowercaseQueryStrings = true;
			});

			services.Configure<AntiforgeryOptions>(options =>
			{
				options.Cookie.Name = "TP.Security";
				options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
				options.Cookie.SameSite = SameSiteMode.Strict;
				options.FormFieldName = "__TP_SEC";
				options.HeaderName = "X-TP-SEC";
			});

			services.AddSingleton(_configuration);
			services.AddSingleton(new CryptographyHelper(Path.Combine(PersistentDataDirectory, "crypto.key")));

			services.AddSingleton<ExtendedFakeFilterService>();

			services.AddSingletonHostedService<InitService>();
			services.AddSingletonHostedService<EmailService>();

			services.AddSingletonHostedService<BackgroundService>();

			services.AddScoped<GlobalSettingsService>();

			string keyDir = Path.Combine(PersistentDataDirectory, "keys");
			if (!Directory.Exists(keyDir))
				Directory.CreateDirectory(keyDir);

			services.AddDataProtection()
				.PersistKeysToFileSystem(new DirectoryInfo(keyDir));

			services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
				.AddCookie(options =>
				{
					options.Cookie.Name = "TP.Auth";
					options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
					options.Cookie.SameSite = SameSiteMode.Strict;

					options.LoginPath = "/account/login";
					options.LogoutPath = "/account/logout";
					options.AccessDeniedPath = "/error/403";
					options.EventsType = typeof(ServerCookieAuthenticationEvents);
					options.SlidingExpiration = true;
					options.ExpireTimeSpan = TimeSpan.FromDays(28);
				});
			services.AddScoped<ServerCookieAuthenticationEvents>();

			//services.AddFido2(options =>
			//{
			//	string host = configuration.GetValue("ASPNETCORE_APPL_URL", "http://localhost:5000");
			//	string path = configuration.GetValue("ASPNETCORE_APPL_PATH", "");
			//	string url = $"{host}{path}";
			//	var uri = new Uri(url);
			//
			//	options.ServerDomain = uri.Host;
			//	options.ServerName = "AM.WD Control";
			//	options.ServerIcon = $"{url}/ico/apple-touch-icon.png";
			//
			//	options.Origins.Add(host);
			//	options.TimestampDriftTolerance = (int)TimeSpan.FromSeconds(3).TotalMilliseconds;
			//});

			services
				.AddSignalR(options =>
				{
#if DEBUG
					options.EnableDetailedErrors = true;
#endif
					options.MaximumParallelInvocationsPerClient = 2;
					options.MaximumReceiveMessageSize = 256 * 1024; // 256 KB
																	//options.MaximumReceiveMessageSize = 1 * 1024 * 1024; // 1 MB

					SignalRMaxMessageSize = options.MaximumReceiveMessageSize;
				});

			var mvc = services
				.AddControllersWithViews(options =>
				{
					options.RespectBrowserAcceptHeader = true;
				})
				.AddNewtonsoftJson();
#if DEBUG
			mvc.AddRazorRuntimeCompilation();
#endif
		}

		public void Configure(IApplicationBuilder app, IServiceScopeFactory serviceScopeFactory)
		{
			bool isDev = _configuration.GetValue("ASPNETCORE_ENVIRONMENT", "Production").Equals("Development", StringComparison.OrdinalIgnoreCase);

			app.UseProxyHosting();
			app.UseResponseCompression();

			app.Use(async (httpContext, next) =>
			{
				using var scope = serviceScopeFactory.CreateScope();
				var initService = scope.ServiceProvider.GetService<InitService>();

				string path = httpContext.Request.Path.ToString();
				if (path.StartsWith("/health", StringComparison.OrdinalIgnoreCase))
				{
					httpContext.Response.StatusCode = initService.IsDatabaseSuccess ? 200 : 500;
					httpContext.Response.ContentType = "text/plain; charset=utf-8";
					await httpContext.Response.WriteAsync($"{(initService.IsDatabaseSuccess ? "OK" : "ERROR")} | {DateTime.UtcNow:yyyy-MM-dd'T'HH:mm:ss.fff'Z'}");
					return;
				}

				if (!path.StartsWith("/error", StringComparison.OrdinalIgnoreCase))
					httpContext.Items["OriginalPath"] = path;

				if (!initService.IsDatabaseSuccess)
				{
					httpContext.Response.Clear();
					httpContext.Response.StatusCode = 500;
					httpContext.Response.ContentType = "text/plain; charset=utf-8";
					await httpContext.Response.WriteAsync(@$"Bei der Initialisierung der Anwendung ist ein Fehler aufgetreten. Weitere Details können dem Log entnommen werden.

Version: {Program.Version} | Serverzeit: {DateTime.Now:dd.MM.yyyy HH:mm:ss}");
					return;
				}
				await next();
			});

			if (isDev)
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/error");
			}
			app.UseStatusCodePagesWithReExecute("/error/{0}");

#if DEBUG
			app.UseStaticFiles();
			var _ = CultureInfo.InvariantCulture;
#else
			app.UseStaticFiles(new StaticFileOptions
			{
				OnPrepareResponse = context =>
				{
					string extension = Path.GetExtension(context.File.Name);
					switch (extension)
					{
						case "css":
						case "eot":
						case "js":
						case "svg":
						case "ttf":
						case "woff":
						case "woff2":
							context.Context.Response.Headers["Cache-Control"] = $"public,max-age={(int)TimeSpan.FromDays(7).TotalSeconds}";
							context.Context.Response.Headers["Expires"] = DateTime.UtcNow.AddDays(14).ToString("R", CultureInfo.InvariantCulture);
							break;
						case "gif":
						case "jpg":
						case "png":
						case "webp":
							context.Context.Response.Headers["Cache-Control"] = $"public,max-age={(int)TimeSpan.FromDays(14).TotalSeconds}";
							context.Context.Response.Headers["Expires"] = DateTime.UtcNow.AddDays(28).ToString("R", CultureInfo.InvariantCulture);
							break;
					}
				}
			});
#endif

			app.UseAuthentication();

			//app.UseRewriter(new RewriteOptions()
			//	.AddMfa(serviceScopeFactory)
			//	.AddUserChecks(serviceScopeFactory));
			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapHub<WebClientHub>("/ws/web", options => options.Transports = HttpTransportType.WebSockets | HttpTransportType.LongPolling);

				endpoints.MapControllerRoute(
					name: "areaDefaultNoAction",
					pattern: "{area:exists}/{controller=Home}/{id:int}",
					defaults: new { action = "Index" });
				endpoints.MapControllerRoute(
					name: "areaDefault",
					pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

				endpoints.MapControllerRoute(
					name: "defaultNoAction",
					pattern: "{controller=Trainings}/{id:int}",
					defaults: new { action = "Index" });
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Trainings}/{action=Index}/{id?}");
			});
		}
	}
}
