﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class TrainingSplit
	{
		[Key]
		public string Id { get; set; }

		[ForeignKey(nameof(Training))]
		public string TrainingId { get; set; }

		public virtual Training Training { get; set; }

		public DateTime Start { get; set; }

		public DateTime End { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }
	}
}
