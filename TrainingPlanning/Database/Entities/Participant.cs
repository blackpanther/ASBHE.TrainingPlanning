﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AMWD.ASBHE.TrainingPlanning.Enums;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class Participant
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public DateTime Birthday { get; set; }

		public string PlaceOfBirth { get; set; }

		[ForeignKey(nameof(Department))]
		public int? DepartmentId { get; set; }

		public virtual Department Department { get; set; }

		public string EmailAddress { get; set; }

		public string PhoneNumber { get; set; }

		public string Street { get; set; }

		public string AddressAddition { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public string DriverLicenseClasses { get; set; }

		public bool DriverPermission { get; set; }

		public bool AssistanceRequired { get; set; }

		public FoodSelection? FoodSelection { get; set; }

		public string Allergies { get; set; }

		public bool IsEnabled { get; set; }
	}
}
