﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using MimeKit;
using Newtonsoft.Json;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class EmailMessage
	{
		[Key]
		public string Id { get; set; } = Guid.NewGuid().ToString();

		[NotMapped]
		public EmailAddress ReplyTo { get; set; }

		[JsonIgnore]
		public string ReplyToJson
		{
			get => ReplyTo?.SerializeJson();
			set => ReplyTo = value?.DeserializeJson<EmailAddress>();
		}

		[NotMapped]
		public List<EmailAddress> Recipients { get; set; } = new();

		[JsonIgnore]
		public string RecipientsJson
		{
			get => Recipients?.SerializeJson();
			set => Recipients = value?.DeserializeJson<List<EmailAddress>>() ?? new();
		}

		public bool RecipientsVisible { get; set; } = true;

		public MessageImportance Priority { get; set; } = MessageImportance.Normal;

		public DateTime SendAfterDate { get; set; } = DateTime.UtcNow;

		public string Subject { get; set; }

		public string Message { get; set; }

		public bool IsHtml { get; set; }

		public EmailMessageStatus Status { get; set; } = EmailMessageStatus.Pending;

		public DateTime? UpdatedAt { get; set; }
	}

	public class EmailAddress
	{
		public string Name { get; set; }

		public string Address { get; set; }
	}
}
