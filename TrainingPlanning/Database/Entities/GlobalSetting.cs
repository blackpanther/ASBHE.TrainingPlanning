﻿using System;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class GlobalSetting
	{
		public string Id { get; set; } = Guid.NewGuid().ToString();

		public string Name { get; set; }

		public string Value { get; set; }

		public DateTime CreatedAt { get; set; }

		public DateTime? UpdatedAt { get; set; }
	}
}
