﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class Training
	{
		[Key]
		public string Id { get; set; }

		public string Title { get; set; }

		public DateTime Start { get; set; }

		public DateTime End { get; set; }

		public int? DeadlineDays { get; set; }

		public int? ReminderDays { get; set; }

		public string Location { get; set; }

		public string ShortDescription { get; set; }

		public string Description { get; set; }

		public int? MinParticipants { get; set; }

		public int? MaxParticipants { get; set; }

		public string TargetAudience { get; set; }

		public string EntryRequirements { get; set; }

		public string Lecturers { get; set; }

		public string Costs { get; set; }

		public string Hints { get; set; }

		public bool IsPublic { get; set; }

		public bool IsEnabled { get; set; }

		public bool IsCancelled { get; set; }

		[NotMapped]
		public int PartipicantCount { get; set; }

		[NotMapped]
		public int? AvailableCount { get; set; }

		[NotMapped]
		public bool CanRegister { get; set; }

		public virtual List<TrainingSplit> Splits { get; set; }
	}
}
