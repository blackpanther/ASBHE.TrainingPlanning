﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class User
	{
		[Key]
		public int Id { get; set; }

		public string EmailAddress { get; set; }

		public string PasswordHash { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public bool IsEnabled { get; set; }

		public bool IsAdmin { get; set; }

		[ForeignKey(nameof(Department))]
		public int? DepartmentId { get; set; }

		public virtual Department Department { get; set; }

		[NotMapped]
		public bool IsBasicAuth { get; set; }

		[NotMapped]
		public int? ImpersonatingId { get; set; }

		public string GetName()
		{
			string name = $"{FirstName} {LastName}".Trim();
			if (string.IsNullOrWhiteSpace(name))
				return EmailAddress;

			return name;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;

			if (obj is not User user)
				return false;

			return Id == user.Id
				&& EmailAddress == user.EmailAddress
				&& FirstName == user.FirstName
				&& LastName == user.LastName
				&& DepartmentId == user.DepartmentId
				&& IsAdmin == user.IsAdmin;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode()
				^ Id.GetHashCode()
				^ EmailAddress.GetHashCode()
				^ FirstName.GetHashCode()
				^ LastName.GetHashCode()
				^ DepartmentId?.GetHashCode() ?? 0
				^ IsAdmin.GetHashCode();
		}

		public override string ToString()
		{
			var list = new List<string> { $"User #{Id}{(IsBasicAuth ? " BASIC" : "")}{(ImpersonatingId.HasValue ? $" <{ImpersonatingId.Value}>" : "")}" };

			if (!string.IsNullOrWhiteSpace(EmailAddress))
				list.Add(EmailAddress);

			if (!string.IsNullOrWhiteSpace($"{FirstName} {LastName}"))
				list.Add($"{FirstName} {LastName}".Trim());

			if (!IsEnabled)
				list.Add("disabled");

			if (IsAdmin)
				list.Add("Admin");

			if (DepartmentId.HasValue)
			{
				if (Department == null)
				{
					list.Add($"Department #{DepartmentId.Value}");
				}
				else
				{
					list.Add($"{Department.Name} (#{Department.Id})");
				}
			}

			return string.Join(" | ", list);
		}
	}
}
