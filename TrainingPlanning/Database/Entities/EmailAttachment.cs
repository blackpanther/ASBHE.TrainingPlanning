﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class EmailAttachment
	{
		[Key]
		public string Id { get; set; } = Guid.NewGuid().ToString();

		[ForeignKey(nameof(EmailMessage))]
		public string EmailMessageId { get; set; }

		public virtual EmailMessage EmailMessage { get; set; }

		public string FileName { get; set; }

		public string ContentType { get; set; }

		public byte[] Content { get; set; }
	}
}
