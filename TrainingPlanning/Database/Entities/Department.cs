﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class Department
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string Name { get; set; }

		public string Contact { get; set; }

		public string EmailAddress { get; set; }

		public string PhoneNumber { get; set; }

		public string Street { get; set; }

		public string AddressAddition { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public bool IsDeleted { get; set; }
	}
}
