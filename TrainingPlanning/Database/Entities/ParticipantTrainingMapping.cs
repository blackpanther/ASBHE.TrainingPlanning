﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using AMWD.ASBHE.TrainingPlanning.Enums;

namespace AMWD.ASBHE.TrainingPlanning.Database.Entities
{
	public class ParticipantTrainingMapping
	{
		[ForeignKey(nameof(Participant))]
		public int ParticipantId { get; set; }

		public virtual Participant Participant { get; set; }

		[ForeignKey(nameof(Training))]
		public string TrainingId { get; set; }

		public virtual Training Training { get; set; }

		[ForeignKey(nameof(Department))]
		public int? DepartmentId { get; set; }

		public virtual Department Department { get; set; }

		public string ExternalDepartment { get; set; }

		public string DriverLicenseClasses { get; set; }

		public bool DriverPermission { get; set; }

		public HotelRequirement HotelRequirement { get; set; }

		public bool AssumptionOfCosts { get; set; }

		public string InvoiceName { get; set; }

		public string InvoiceStreet { get; set; }

		public string InvoiceZipCode { get; set; }

		public string InvoiceCity { get; set; }

		public string InvoiceCountry { get; set; }

		public string Notes { get; set; }

		public ParticipantTrainingStatus Status { get; set; }

		public string Street { get; set; }

		public string AddressAddition { get; set; }

		public string ZipCode { get; set; }

		public string City { get; set; }

		public string Country { get; set; }

		public DateTime CreatedAt { get; set; }
	}
}
