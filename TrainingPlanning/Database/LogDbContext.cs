﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AMWD.ASBHE.TrainingPlanning.Database
{
	/// <summary>
	/// The log database.
	/// </summary>
	public class LogDbContext : DbContext
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LogDbContext"/> class.
		/// </summary>
		/// <param name="options"></param>
		public LogDbContext(DbContextOptions<LogDbContext> options)
			: base(options)
		{ }

		/// <summary>
		/// Gets or sets the log entries.
		/// </summary>
		public DbSet<SerilogEntry> Entries { get; protected set; }

		/// <summary>
		/// Gets the path to the log database.
		/// </summary>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static string GetPath(IConfiguration configuration)
		{
			string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var logSection = configuration.GetSection("Serilog");
			if (logSection.Exists())
			{
				var writeTo = logSection.GetSection("WriteTo");
				if (writeTo.Exists())
				{
					foreach (var section in writeTo.GetChildren())
					{
						if (section.GetValue<string>("Name") == "SQLite")
						{
							string path = section.GetValue<string>("Args:sqliteDbPath");
							if (!string.IsNullOrWhiteSpace(path))
							{
								if (Path.IsPathRooted(path))
									return path;

								return Path.Combine(dir, path);
							}
						}
					}
				}
			}

			return null;
		}

		/// <summary>
		/// Returns the connection string.
		/// </summary>
		/// <param name="configuration"></param>
		/// <returns></returns>
		public static string GetConnectionString(IConfiguration configuration)
		{
			var builder = new SqliteConnectionStringBuilder
			{
				ForeignKeys = true,
				DataSource = GetPath(configuration)
			};
			return builder.ConnectionString;
		}
	}

	/// <summary>
	/// A <see cref="Serilog"/> entry.
	/// </summary>
	[Table("Logs")]
	public class SerilogEntry
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		[Key]
		public long Id { get; set; }

		/// <summary>
		/// Gets or sets the timestamp in the database.
		/// </summary>
		[JsonIgnore]
		[Column("Timestamp")]
		public string DbTimestamp { get; set; }

		/// <summary>
		/// Gets or sets the log level.
		/// </summary>
		[MaxLength(10)]
		public string Level { get; set; }

		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		public string Exception { get; set; }

		/// <summary>
		/// Gets or sets raw log message.
		/// </summary>
		[JsonIgnore]
		[Column("RenderedMessage")]
		public string RawMessage { get; set; }

		/// <summary>
		/// Gets or sets the properties as json.
		/// </summary>
		[JsonIgnore]
		[Column("Properties")]
		public string PropertiesJson { get; set; }

		/// <summary>
		/// Gets the timestamp.
		/// </summary>
		[NotMapped]
		[JsonProperty("timestampISO")]
		public DateTime Timestamp => DateTime.SpecifyKind(DateTime.Parse(DbTimestamp), DateTimeKind.Utc);

		/// <summary>
		/// Gets the rendered message.
		/// </summary>
		[NotMapped]
		public string Message => ToString();

		/// <summary>
		/// Gets the properties.
		/// </summary>
		[NotMapped]
		public JObject Properties => PropertiesJson?.DeserializeJson<JObject>();

		/// <inheritdoc/>
		public override string ToString()
		{
			string output = RawMessage;

			string pattern = @"\{([0-9a-zA-Z.:]+)\}";
			var matches = Regex.Matches(RawMessage, pattern);

			foreach (Match match in matches)
			{
				if (match.Success)
				{
					string key = match.Value.Replace("{", "").Replace("}", "");
					string format = null;
					if (key.Contains(':'))
					{
						string[] split = key.Split(':');
						key = split[0];
						format = split[1];
					}

					if (Properties.TryGetValue(key, out var token))
					{
						string replace = null;
						replace = token.Type switch
						{
							JTokenType.Float => string.IsNullOrWhiteSpace(format)
								? token.Value<decimal>().ToString()
								: token.Value<decimal>().ToString(format),
							JTokenType.Integer => string.IsNullOrWhiteSpace(format)
								? token.Value<long>().ToString()
								: token.Value<long>().ToString(format),
							_ => token.ToString(),
						};
						output = output.Replace(match.Value, replace);
					}
				}
			}

			return output.Trim();
		}
	}
}
