-- Initialize Database
-- 2023-04-16 <Andreas Müller>

create table [global_settings]
(
	[id] nvarchar(40) not null,
	[name] nvarchar(50) not null,
	[value] nvarchar(max) null,
	[created_at] datetime2(2) not null,
	[updated_at] datetime2(2) null,
	constraint [pk_global_settings]
		primary key ([id]),
	constraint [uq_global_settings_name]
		unique ([name])
)
go

create table [email_messages]
(
	[id] nvarchar(40) not null,
	[reply_to_json] nvarchar(max) null,
	[recipients_json] nvarchar(max) not null,
	[recipients_visible] bit not null,
	[priority] tinyint not null,
	[send_after_date] datetime2(2) not null,
	[subject] nvarchar(100) not null,
	[message] nvarchar(max) not null,
	[is_html] bit not null,
	[status] tinyint not null,
	[updated_at] datetime2(2) null,
	constraint [pk_email_messages]
		primary key ([id])
)
go

create table [email_attachments]
(
	[id] nvarchar(40) not null,
	[email_message_id] nvarchar(40) not null,
	[file_name] nvarchar(150) not null,
	[content_type] nvarchar(30) not null,
	[content] varbinary(max) not null,
	constraint [pk_email_attachments]
		primary key ([id]),
	constraint [fk_email_attachments_email_message]
		foreign key ([email_message_id])
		references [email_messages] ([id])
)
go

create table [departments]
(
	[id] int not null identity(1, 1),
	[name] nvarchar(100) not null,
	[contact] nvarchar(50) null,
	[email_address] nvarchar(250) null,
	[phone_number] nvarchar(50) null,
	[street] nvarchar(50) null,
	[address_addition] nvarchar(50) null,
	[zip_code] nvarchar(10) null,
	[city] nvarchar(50) null,
	[country] nvarchar(50) null,
	[is_deleted] bit not null,
	constraint [pk_departments]
		primary key ([id]),
	constraint [uq_department_name]
		unique ([name])
)
go

create table [users]
(
	[id] int not null,
	[email_address] nvarchar(250) not null,
	[password_hash] nvarchar(200) null,
	[first_name] nvarchar(50) null,
	[last_name] nvarchar(50) null,
	[is_enabled] bit not null,
	[is_admin] bit not null,
	[department_id] int null,
	constraint [pk_users]
		primary key ([id]),
	constraint [fk_users_department]
		foreign key ([department_id])
		references [departments] ([id])
)
go

create table [trainings]
(
	[id] nvarchar(40) not null,
	[title] nvarchar(100) not null,
	[start] datetime2(2) not null,
	[end] datetime2(2) not null,
	[deadline_days] int null,
	[reminder_days] int null,
	[location] nvarchar(100) null,
	[short_description] nvarchar(max) null,
	[description] nvarchar(max) null,
	[min_participants] int null,
	[max_participants] int null,
	[target_audience] nvarchar(max) null,
	[entry_requirements] nvarchar(max) null,
	[lecturers] nvarchar(max) null,
	[costs] nvarchar(max) null,
	[hints] nvarchar(max) null,
	[is_public] bit not null,
	[is_enabled] bit not null,
	[is_cancelled] bit not null,
	constraint [pk_trainings]
		primary key ([id])
)
go

create table [training_splits]
(
	[id] nvarchar(40) not null,
	[training_id] nvarchar(40) not null,
	[start] datetime2(2) not null,
	[end] datetime2(2) not null,
	[name] nvarchar(100) null,
	[description] nvarchar(max) null,
	constraint [pk_training_splits]
		primary key ([id]),
	constraint [fk_training_split_training]
		foreign key ([training_id])
		references [trainings] ([id])
)
go

create table [participants]
(
	[id] int not null identity(1, 1),
	[first_name] nvarchar(50) not null,
	[last_name] nvarchar(50) not null,
	[birthday] datetime2(0) not null,
	[place_of_birth] nvarchar(50) null,
	[department_id] int null,
	[email_address] nvarchar(150) not null,
	[phone_number] nvarchar(20) null,
	[street] nvarchar(50) null,
	[address_addition] nvarchar(50) null,
	[zip_code] nvarchar(10) null,
	[city] nvarchar(50) null,
	[country] nvarchar(50) null,
	[driver_license_classes] nvarchar(200) null,
	[driver_permission] bit not null,
	[assistance_required] bit not null,
	[food_selection] tinyint null,
	[allergies] nvarchar(max) null,
	[is_enabled] bit not null,
	constraint [pk_participants]
		primary key ([id]),
	constraint [fk_participants_department]
		foreign key ([department_id])
		references [departments] ([id])
)
go

create table [participant_training_mappings]
(
	[participant_id] int not null,
	[training_id] nvarchar(40) not null,
	[department_id] int null,
	[external_department] nvarchar(100),
	[driver_license_classes] nvarchar(200) null,
	[driver_permission] bit not null,
	[hotel_requirement] tinyint not null,
	[assumption_of_costs] bit not null,
	[invoice_name] nvarchar(100) null,
	[invoice_street] nvarchar(50) null,
	[invoice_zip_code] nvarchar(10) null,
	[invoice_city] nvarchar(50) null,
	[status] tinyint not null,
	[created_at] datetime2(2) not null,
	[notes] nvarchar(max) null,
	constraint [pk_participant_training_mappings]
		primary key ([participant_id], [training_id]),
	constraint [fk_mapping_participant]
		foreign key ([participant_id])
		references [participants] ([id]),
	constraint [fk_mapping_training]
		foreign key ([training_id])
		references [trainings] ([id]),
	constraint [fk_mapping_department]
		foreign key ([department_id])
		references [departments] ([id])
)
go
