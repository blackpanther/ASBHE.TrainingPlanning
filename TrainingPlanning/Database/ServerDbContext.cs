﻿using System;
using System.Linq;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AMWD.ASBHE.TrainingPlanning.Database
{
	public class ServerDbContext : DbContext
	{
		public ServerDbContext(DbContextOptions<ServerDbContext> options)
			: base(options)
		{ }

		public DbSet<Department> Departments { get; protected set; }

		public DbSet<EmailAttachment> EmailAttachments { get; protected set; }

		public DbSet<EmailMessage> EmailMessages { get; protected set; }

		public DbSet<GlobalSetting> GlobalSettings { get; protected set; }

		public DbSet<Participant> Participants { get; protected set; }

		public DbSet<ParticipantTrainingMapping> ParticipantTrainingMappings { get; protected set; }

		public DbSet<Training> Trainings { get; protected set; }

		public DbSet<TrainingSplit> TrainingSplits { get; protected set; }

		public DbSet<User> Users { get; protected set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			if (Database.IsNpgsql())
			{
				foreach (var entityType in builder.Model.GetEntityTypes())
				{
					var identifier = StoreObjectIdentifier.Table(entityType.GetTableName(), entityType.GetSchema());
					foreach (var property in entityType.GetProperties())
					{
						if (property.GetColumnName(identifier).EndsWith("Json") && property.ClrType == typeof(string))
							property.SetColumnType("jsonb");
					}
				}
			}

			if (Database.IsMySql())
			{
				foreach (var entityType in builder.Model.GetEntityTypes())
				{
					var identifier = StoreObjectIdentifier.Table(entityType.GetTableName(), entityType.GetSchema());
					foreach (var property in entityType.GetProperties())
					{
						if (property.GetColumnName(identifier).EndsWith("Json") && property.ClrType == typeof(string))
							property.SetColumnType("JSON");
					}
				}
			}

			builder.ApplySnakeCase();

			// composed keys
			builder.Entity<ParticipantTrainingMapping>()
				.HasKey(e => new { e.ParticipantId, e.TrainingId });
		}

		internal int CreateUserId()
		{
			int id = 0;
			var rand = new Random();
			do
			{
				id = rand.Next(100_000, 1_000_000);
			}
			while (Users.Any(u => u.Id == id));

			return id;
		}
	}
}
