﻿using System.Text;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	public class MetaController : Controller
	{
		private readonly IConfiguration _configuration;

		public MetaController(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public IActionResult Manifest()
		{
			string host = _configuration.GetValue("ASPNETCORE_APPL_URL", HttpContext.Request.Scheme + "://" + HttpContext.Request.Host);
			string path = _configuration.GetValue("ASPNETCORE_APPL_PATH", HttpContext.Request.PathBase.ToString());

			var manifest = new JObject
			{
				["name"] = "Katastrophenschut ASB Hessen",
				["short_name"] = "ASB HE KatS",
				["description"] = "Seminarprogramm und Anmeldung für den Katastrophenschutz des ASB Landesverband Hessen",
				["icons"] = new JArray
				{
					new JObject
					{
						["src"] = $"{path}/ico/android-chrome-192x192.png",
						["sizes"] = "192x192",
						["type"] = "image/png"
					},
					new JObject
					{
						["src"] = $"{path}/ico/android-chrome-512x512.png",
						["sizes"] = "512x512",
						["type"] = "image/png"
					}
				},
				["theme_color"] = "#fff",
				["background_color"] = "#ccc",
				["display"] = "standalone",
				["scope"] = $"{path}/",
				["start_url"] = $"{host}{path}"
			};

			Response.ContentType = "application/manifest+json";
			return Json(manifest);
		}

		public IActionResult BrowserConfig()
		{
			string path = _configuration.GetValue("ASPNETCORE_APPL_PATH", HttpContext.Request.PathBase.ToString());

			var sb = new StringBuilder();
			sb.AppendLine($"<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
			sb.AppendLine($"<browserconfig>");
			sb.AppendLine($"	<msapplication>");
			sb.AppendLine($"		<tile>");
			sb.AppendLine($"			<square70x70logo src=\"{path}/mstile-70x70.png\" />");
			sb.AppendLine($"			<TileColor>#ccc</TileColor>");
			sb.AppendLine($"		</tile>");
			sb.AppendLine($"	</msapplication>");
			sb.AppendLine($"</browserconfig>");

			return Content(sb.ToString(), "text/xml", Encoding.UTF8);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult JavaScript()
		{
			string host = _configuration.GetValue("ASPNETCORE_APPL_URL", HttpContext.Request.Scheme + "://" + HttpContext.Request.Host);
			string path = _configuration.GetValue("ASPNETCORE_APPL_PATH", HttpContext.Request.PathBase.ToString());

			var (formName, headerName, tokenValue) = HttpContext.GetAntiforgeryToken();

			var sb = new StringBuilder();

			sb.AppendLine($"TP.Url = '{host}{path}';");
			sb.AppendLine($"TP.Path = '{path}';");
			sb.AppendLine($"TP.Token.FormName = '{formName}';");
			sb.AppendLine($"TP.Token.HeaderName = '{headerName}';");
			sb.AppendLine($"TP.Token.Value = '{tokenValue}';");

			return Content(sb.ToString().Trim(), "text/javascript", Encoding.UTF8);
		}
	}
}
