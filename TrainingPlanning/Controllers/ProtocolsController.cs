﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class ProtocolsController : Controller
	{
		private readonly IServiceScopeFactory _serviceScopeFactory;

		public ProtocolsController(IServiceScopeFactory serviceScopeFactory)
		{
			_serviceScopeFactory = serviceScopeFactory;
		}

		public IActionResult Index()
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			using var scope = _serviceScopeFactory.CreateScope();
			var logContext = scope.ServiceProvider.GetService<LogDbContext>();

			ViewData["Title"] = "Server-Protokoll";
			ViewData["IsAvailable"] = logContext != null;
			return View();
		}

		public async Task<IActionResult> Get(long id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			using var scope = _serviceScopeFactory.CreateScope();
			var logContext = scope.ServiceProvider.GetService<LogDbContext>();

			if (logContext == null)
				return Json(Array.Empty<SerilogEntry>());

			try
			{
				var entries = await logContext.Entries
					.Where(e => e.Id < id || id == 0)
					.OrderByDescending(e => e.Id)
					.Take(50)
					.ToListAsync(cancellationToken);

				return Json(entries);
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}
	}
}
