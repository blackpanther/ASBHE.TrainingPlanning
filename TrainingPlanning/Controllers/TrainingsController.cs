﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Hubs;
using AMWD.ASBHE.TrainingPlanning.Models;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace TrainingPlanning.Controllers
{
	[Authorize]
	public class TrainingsController : Controller
	{
		private readonly ILogger<TrainingsController> _logger;
		private readonly ServerDbContext _dbContext;
		private readonly ExtendedFakeFilterService _fakeFilterService;
		private readonly IHubContext<WebClientHub> _webClientHub;

		public TrainingsController(ILogger<TrainingsController> logger, ServerDbContext dbContext, ExtendedFakeFilterService fakeFilterService, IHubContext<WebClientHub> webClientHub)
		{
			_logger = logger;
			_dbContext = dbContext;
			_fakeFilterService = fakeFilterService;
			_webClientHub = webClientHub;
		}

		[AllowAnonymous]
		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			bool isAuthenticated = authUser != null;
			bool isAdmin = authUser?.IsAdmin ?? false;

			var model = new TrainingViewModel
			{
				Trainings = await _dbContext.Trainings
					.Where(t => t.IsEnabled || isAdmin)
					.Where(t => t.IsPublic || isAuthenticated)
					.Where(t => t.Start >= DateTime.Now)
					.OrderBy(t => t.Start)
					.ToListAsync(cancellationToken)
			};

			foreach (var training in model.Trainings)
				await AddTrainingInformation(training, cancellationToken);

			if (isAuthenticated)
			{
				ViewData["Department"] = (await _dbContext.Departments
					.Where(d => d.Id == authUser.DepartmentId)
					.FirstOrDefaultAsync(cancellationToken))
					?.Name;
				ViewData["Participants"] = (await _dbContext.Participants
					.Include(p => p.Department)
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.ToListAsync(cancellationToken))
					.OrderBy(p => p.FirstName + " " + p.LastName)
					.ThenBy(p => p.EmailAddress)
					.ThenBy(p => p.Department?.Name ?? "-")
					.ToList();
			}

			ViewData["Title"] = "Übersicht";
			return View(model);
		}

		[AllowAnonymous]
		public async Task<IActionResult> Details(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			bool isAuthenticated = authUser != null;
			bool isAdmin = authUser?.IsAdmin ?? false;

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.Where(t => t.IsEnabled || isAdmin)
				.Where(t => t.IsPublic || isAuthenticated)
				.FirstOrDefaultAsync(cancellationToken);

			if (training == null)
				return NotFound();

			await AddTrainingInformation(training, cancellationToken);

			var model = new TrainingViewModel
			{
				Id = training.Id,
				Title = training.Title,
				Start = training.Start,
				End = training.End,
				DeadlineDays = training.DeadlineDays,
				Location = training.Location,
				ShortDescription = training.ShortDescription,
				Description = training.Description,
				Costs = training.Costs,
				EntryRequirements = training.EntryRequirements,
				Hints = training.Hints,
				IsCancelled = training.IsCancelled,
				Lecturers = training.Lecturers,
				MaxParticipants = training.MaxParticipants,
				MinParticipants = training.MinParticipants,
				TargetAudience = training.TargetAudience,
				CanRegister = training.CanRegister,
				PartipicantCount = training.PartipicantCount,
				AvailableCount = training.AvailableCount,
				Splits = await _dbContext.TrainingSplits
					.Where(s => s.TrainingId == training.Id)
					.ToListAsync(cancellationToken),
			};

			if (isAuthenticated)
			{
				ViewData["Department"] = (await _dbContext.Departments
					.Where(d => d.Id == authUser.DepartmentId)
					.FirstOrDefaultAsync(cancellationToken))
					?.Name;
				ViewData["Participants"] = (await _dbContext.Participants
					.Include(p => p.Department)
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.ToListAsync(cancellationToken))
					.OrderBy(p => p.FirstName + " " + p.LastName)
					.ThenBy(p => p.EmailAddress)
					.ThenBy(p => p.Department?.Name ?? "-")
					.ToList();
			}

			ViewData["Title"] = training.Title;
			return View(model);
		}

		public IActionResult Create()
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var model = new TrainingViewModel
			{
				DeadlineDays = 7 * 6, // 6 weeks
				ReminderDays = 7 * 4, // 4 weeks
				SplitsJson = Array.Empty<TrainingSplit>().SerializeJson()
			};

			ViewData["Title"] = "Neuer Lehrgang";
			ViewData["ViewTitle"] = "Neuer Lehrgang";
			return View(nameof(Edit), model);
		}

		public async Task<IActionResult> Edit(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.Where(t => t.Start >= DateTime.Now)
				.OrderBy(t => t.Start)
				.FirstOrDefaultAsync(cancellationToken);

			if (training == null)
				return NotFound();

			await AddTrainingInformation(training, cancellationToken);

			var model = new TrainingViewModel
			{
				Id = training.Id,
				Title = training.Title,
				Start = training.Start,
				End = training.End,
				DeadlineDays = training.DeadlineDays,
				ReminderDays = training.ReminderDays,
				Location = training.Location,
				ShortDescription = training.ShortDescription,
				Description = training.Description,
				Costs = training.Costs,
				EntryRequirements = training.EntryRequirements,
				Hints = training.Hints,
				IsCancelled = training.IsCancelled,
				IsEnabled = training.IsEnabled,
				IsPublic = training.IsPublic,
				Lecturers = training.Lecturers,
				MaxParticipants = training.MaxParticipants,
				MinParticipants = training.MinParticipants,
				TargetAudience = training.TargetAudience,
			};

			var splits = await _dbContext.TrainingSplits
				.Where(s => s.TrainingId == training.Id)
				.OrderBy(s => s.Start)
				.ToListAsync(cancellationToken);
			model.SplitsJson = splits.SerializeJson();

			ViewData["Title"] = "Lehrgang bearbeiten";
			ViewData["ViewTitle"] = "Lehrgang bearbeiten";
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(TrainingViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			ViewData["Title"] = string.IsNullOrWhiteSpace(model.Id) ? "Neuer Lehrgang" : "Lehrgang bearbeiten";
			ViewData["ViewTitle"] = string.IsNullOrWhiteSpace(model.Id) ? "Neuer Lehrgang" : "Lehrgang bearbeiten";

			if (string.IsNullOrWhiteSpace(model.Title))
				ModelState.AddModelError(nameof(model.Title), "Ein Titel ist erforderlich");

			if (model.Start <= DateTime.Now)
				ModelState.AddModelError(nameof(model.Start), "Der Lehrgang muss in der Zukunft liegen");

			if (model.End <= model.Start)
				ModelState.AddModelError(nameof(model.End), "Der Lehrgang kann nicht zu Ende sein, bevor er begonnen hat");

			if (string.IsNullOrWhiteSpace(model.Location))
				ModelState.AddModelError(nameof(model.Location), "Ein Lehrgangsort ist erforderlich");

			if (string.IsNullOrWhiteSpace(model.ShortDescription))
				ModelState.AddModelError(nameof(model.ShortDescription), "Eine Kurzbeschreibung ist erforderlich");

			if (string.IsNullOrWhiteSpace(model.Description))
				ModelState.AddModelError(nameof(model.Description), "Eine Detail-Beschreibung ist erforderlich");

			if (string.IsNullOrWhiteSpace(model.TargetAudience))
				ModelState.AddModelError(nameof(model.TargetAudience), "Eine Zielgruppe sollte definiert werden");

			if (string.IsNullOrWhiteSpace(model.Costs))
				ModelState.AddModelError(nameof(model.Costs), "Die Kosten sollten angegeben werden");

			if (model.MinParticipants.HasValue && model.MinParticipants < 1)
				ModelState.AddModelError(nameof(model.MinParticipants), "Die min. Zahl ist ungültig");

			if (model.MaxParticipants.HasValue && model.MaxParticipants < 1)
				ModelState.AddModelError(nameof(model.MinParticipants), "Die max. Zahl ist ungültig");

			if (model.DeadlineDays.HasValue && model.DeadlineDays < 0)
				ModelState.AddModelError(nameof(model.DeadlineDays), "Der Anmeldeschluss ist ungültig");

			if (model.ReminderDays.HasValue && model.ReminderDays < 0)
				ModelState.AddModelError(nameof(model.ReminderDays), "Die Erinnerung ist ungültig");

			if (!ModelState.IsValid)
				return View(model);

			string shortDescription = Regex.Replace(model.ShortDescription, "<[^>]*>", "").Trim();
			string description = Regex.Replace(model.Description, "<[^>]*>", "").Trim();

			if (string.IsNullOrWhiteSpace(shortDescription))
				ModelState.AddModelError(nameof(model.ShortDescription), "Eine Kurzbeschreibung ist erforderlich");

			if (string.IsNullOrWhiteSpace(description))
				ModelState.AddModelError(nameof(model.Description), "Eine Detail-Beschreibung ist erforderlich");

			if (model.MaxParticipants < model.MinParticipants)
				ModelState.AddModelError(nameof(model.MaxParticipants), "Die max. Zahl ist ungültig");

			if (!ModelState.IsValid)
				return View(model);

			using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);
			try
			{
				var training = await _dbContext.Trainings
					.Where(t => t.Id == model.Id)
					.FirstOrDefaultAsync(cancellationToken);
				if (training == null)
				{
					training = new Training
					{
						Id = Guid.NewGuid().ToString()
					};
					await _dbContext.Trainings.AddAsync(training, cancellationToken);
				}

				training.Title = model.Title.Trim();
				training.Location = model.Location.Trim();
				training.Start = model.Start;
				training.End = model.End;
				training.ShortDescription = model.ShortDescription.Trim();
				training.Description = model.Description.Trim();
				training.TargetAudience = model.TargetAudience.Trim();
				training.EntryRequirements = model.EntryRequirements.Trim();
				training.Costs = model.Costs.Trim();
				training.Lecturers = model.Lecturers.Trim();
				training.Hints = model.Hints?.Trim();
				training.MinParticipants = model.MinParticipants;
				training.MaxParticipants = model.MaxParticipants;

				training.DeadlineDays = model.DeadlineDays;
				training.ReminderDays = model.ReminderDays;
				training.IsEnabled = model.IsEnabled;
				training.IsPublic = model.IsPublic;

				var splits = model.SplitsJson.DeserializeJson<List<TrainingSplit>>();
				_dbContext.TrainingSplits.RemoveRange(_dbContext.TrainingSplits.Where(s => s.TrainingId == training.Id));
				foreach (var split in splits)
				{
					if (string.IsNullOrWhiteSpace(split.Name))
						continue;

					if (split.Start < training.Start || training.End < split.End)
						continue;

					await _dbContext.TrainingSplits.AddAsync(new TrainingSplit
					{
						Id = Guid.NewGuid().ToString(),
						Name = split.Name.Trim(),
						Start = split.Start,
						End = split.End,
						Description = split.Description?.Trim(),
						Training = training
					}, cancellationToken);
				}

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				int? availableCount = null;
				if (training.MaxParticipants.HasValue)
				{
					int participantCount = await _dbContext.ParticipantTrainingMappings
						.Where(m => m.TrainingId == training.Id)
						.CountAsync(cancellationToken);
					availableCount = Math.Max(0, training.MaxParticipants.Value - participantCount);
				}
				await _webClientHub.Clients.All.SendAsync(nameof(WebClientHub.UpdateAvailableCount), training.Id, availableCount, cancellationToken);

				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Saving training failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (training == null)
				return NotFound();

			using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);
			try
			{
				var splits = await _dbContext.TrainingSplits
					.Where(s => s.TrainingId == training.Id)
					.ToListAsync(cancellationToken);
				var mappings = await _dbContext.ParticipantTrainingMappings
					.Include(m => m.Participant)
					.Include(m => m.Participant.Department)
					.Where(m => m.TrainingId == training.Id)
					.ToListAsync(cancellationToken);

				_dbContext.TrainingSplits.RemoveRange(splits);
				_dbContext.ParticipantTrainingMappings.RemoveRange(mappings);
				_dbContext.Trainings.Remove(training);

				var sb = new StringBuilder();
				sb.AppendLine("+++++ INFORMATION START: Löschung Lehrgang +++++");
				sb.AppendLine();
				sb.AppendLine();
				sb.AppendLine("Daten Lehrgang");
				sb.AppendLine("==============");
				sb.AppendLine();

				sb.AppendLine($"Titel:            {training.Title}");

				if (training.Start.Date == training.End.Date)
					sb.AppendLine($"Gesamt-Zeitraum:  {training.Start:dd.MM.yyyy HH:mm} - {training.End:HH:mm}");
				else
					sb.AppendLine($"Gesamt-Zeitraum:  {training.Start:dd.MM.yyyy} - {training.End:dd.MM.yyyy}");

				sb.AppendLine($"Lehrgangsort:     {training.Location}");
				sb.AppendLine($"Dozenten:         {training.Lecturers}");
				sb.AppendLine();

				if (splits.Any())
				{
					sb.AppendLine("Termine");
					sb.AppendLine("-------");
					sb.AppendLine();

					foreach (var split in splits)
					{
						sb.AppendLine($"- {split.Name}");
						if (split.Start.Date == split.End.Date)
							sb.AppendLine($"  {split.Start:dd.MM.yyyy HH:mm} - {split.End:HH:mm}");
						else
							sb.AppendLine($"  {split.Start:dd.MM.yyyy} - {split.End:dd.MM.yyyy}");

						sb.AppendLine($"  {split.Description}");
						sb.AppendLine();
					}
					sb.AppendLine();
				}

				if (mappings.Any())
				{
					sb.AppendLine("Teilnehmer");
					sb.AppendLine("==========");
					sb.AppendLine();

					foreach (var mapping in mappings)
					{
						sb.AppendLine($"- {mapping.Participant.FirstName} {mapping.Participant.LastName}, {mapping.Participant.Birthday:dd.MM.yyyy}");
						sb.AppendLine($"  {mapping.Participant.EmailAddress}");
						if (mapping.Participant.Department != null)
							sb.AppendLine($"  {mapping.Participant.Department.Name}");

						sb.AppendLine();
					}
				}

				sb.AppendLine();
				sb.AppendLine("+++++ INFORMATION ENDE: Löschung Lehrgang +++++");

				var adminMails = await _dbContext.Users
					.Where(u => u.IsEnabled)
					.Where(u => u.IsAdmin)
					.Select(u => u.EmailAddress)
					.ToListAsync(cancellationToken);

				await _dbContext.EmailMessages.AddAsync(new EmailMessage
				{
					Message = sb.ToString(),
					Subject = "Lehrgang gelöscht",
					RecipientsVisible = true,
					Recipients = adminMails.Select(address => new EmailAddress { Address = address }).ToList()
				}, cancellationToken);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Delete training ({id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		public async Task<IActionResult> Participants(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (training == null)
				return NotFound();

			var mappings = await _dbContext.ParticipantTrainingMappings
				.Include(m => m.Department)
				.Include(m => m.Participant)
					.ThenInclude(p => p.Department)
				.Where(m => m.TrainingId == id)
				.ToListAsync(cancellationToken);

			var model = new TrainingParticipantsViewModel
			{
				Id = training.Id,
				TrainingTitle = training.Title,
				TrainingStart = training.Start,
				TrainingEnd = training.End,
				Participants = mappings.Select(m => new TrainingParticipant
				{
					// Participant
					Id = m.Participant.Id,
					FirstName = m.Participant.FirstName,
					LastName = m.Participant.LastName,
					Birthday = m.Participant.Birthday,
					PlaceOfBirth = m.Participant.PlaceOfBirth,
					EmailAddress = m.Participant.EmailAddress,
					PhoneNumber = m.Participant.PhoneNumber,
					AssistanceRequired = m.Participant.AssistanceRequired,
					FoodSelection = m.Participant.FoodSelection,
					Allergies = m.Participant.Allergies,

					// Mapping
					HotelRequirement = m.HotelRequirement,
					AssumptionOfCosts = m.AssumptionOfCosts,
					InvoiceName = m.InvoiceName,
					InvoiceStreet = m.InvoiceStreet,
					InvoiceZipCode = m.InvoiceZipCode,
					InvoiceCity = m.InvoiceCity,
					InvoiceCountry = m.InvoiceCountry,
					Notes = m.Notes,
					Status = m.Status,

					// Comparison
					ParticipantDepartmentName = m.Participant.Department?.Name,
					RegistrationDepartmentName = m.Department?.Name,
					ExternalDepartment = m.ExternalDepartment,
					ParticipantStreet = m.Participant.Street,
					RegistrationStreet = m.Street,
					ParticipantAddition = m.Participant.AddressAddition,
					RegistrationAddition = m.AddressAddition,
					ParticipantZipCode = m.Participant.ZipCode,
					RegistrationZipCode = m.ZipCode,
					ParticipantCity = m.Participant.City,
					RegistrationCity = m.City,
					ParticipantCountry = m.Participant.Country,
					RegistrationCountry = m.Country,
					ParticipantDriverLicenseClasses = m.Participant.DriverLicenseClasses,
					RegistrationDriverLicenseClasses = m.DriverLicenseClasses,
					ParticipantDriverPermission = m.Participant.DriverPermission,
					RegistrationDriverPermission = m.DriverPermission,
				}).ToList()
			};

			ViewData["Title"] = $"Teilnehmerliste für {training.Title} ({training.Start:dd.MM.yyyy} - {training.End:dd.MM.yyyy})";
			ViewData["ViewTitle"] = $"{training.Title} ({training.Start:dd.MM.yyyy} - {training.End:dd.MM.yyyy})";
			return View(model);
		}

		#region AJAX calls

		[AllowAnonymous]
		public async Task<IActionResult> Get(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			bool isAuthenticated = authUser != null;
			bool isAdmin = authUser?.IsAdmin ?? false;

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.Where(t => t.IsEnabled || isAdmin)
				.Where(t => t.IsPublic || isAuthenticated)
				.FirstOrDefaultAsync(cancellationToken);

			if (training == null)
				return NotFound();

			return Json(training);
		}

		public async Task<IActionResult> ToggleEnable(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (training == null)
				return NotFound("Lehrgang nicht gefunden");

			try
			{
				training.IsEnabled = !training.IsEnabled;
				await _dbContext.SaveChangesAsync(cancellationToken);
				return Json(training.IsEnabled);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Toggling enable status for {id} failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		public async Task<IActionResult> TogglePublic(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var training = await _dbContext.Trainings
				.Where(t => t.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (training == null)
				return NotFound("Lehrgang nicht gefunden");

			try
			{
				training.IsPublic = !training.IsPublic;
				await _dbContext.SaveChangesAsync(cancellationToken);
				return Json(training.IsPublic);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Toggling enable status for {id} failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Cancel(string id, string reason, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			if (string.IsNullOrWhiteSpace(reason))
				return BadRequest();

			using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);
			try
			{
				var training = await _dbContext.Trainings
					.Where(t => !t.IsCancelled)
					.Where(t => t.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (training == null)
					return NotFound();

				training.IsCancelled = true;

				var participants = await _dbContext.ParticipantTrainingMappings
					.Include(m => m.Participant)
					.Where(m => m.TrainingId == training.Id)
					.Select(m => m.Participant)
					.ToListAsync(cancellationToken);

				var email = new EmailMessage
				{
					Subject = $"Lehrgang-Absage: {training.Title}",
					Message = reason.Trim(),
					IsHtml = true,
					RecipientsVisible = false,
					ReplyTo = new EmailAddress
					{
						Address = authUser.EmailAddress,
						Name = $"{authUser.FirstName} {authUser.LastName}"
					},
					Recipients = participants.Select(p => new EmailAddress { Address = p.EmailAddress, Name = $"{p.FirstName} {p.LastName}" }).ToList()
				};
				await _dbContext.AddAsync(email, cancellationToken);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Cancel training with id {id} failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Register([FromBody] RegisterViewModel model, CancellationToken cancellationToken)
		{
			try
			{
				var authUser = HttpContext.GetAuthUser();

				if (model == null)
					return StatusCode(StatusCodes.Status500InternalServerError, "Die Anfrage konnte nicht gelesen werden");

				if (!model.TermsAccepted)
					return BadRequest("Die Teilnahmebedingungen wurden nicht akzeptiert");

				var training = await _dbContext.Trainings
					.Where(t => t.Id == model.TrainingId)
					.FirstOrDefaultAsync(cancellationToken);
				if (training == null)
					return NotFound("Kein Lehrgang gefunden");

				if (training.DeadlineDays.HasValue && DateTime.Now.AddDays(training.DeadlineDays.Value) >= training.Start)
					return BadRequest("Die Anmeldefrist ist bereits verstrichen");

				if (training.MaxParticipants.HasValue)
				{
					int participantCount = await _dbContext.ParticipantTrainingMappings
						.Where(m => m.TrainingId == training.Id)
						.CountAsync(cancellationToken);
					if (training.MaxParticipants <= participantCount)
						return Conflict("Die maximale Teilnehmerzahl ist bereits erreicht");
				}

				var errors = new List<string>();
				if (string.IsNullOrWhiteSpace(model.FirstName))
					errors.Add("Ein Vorname ist erforderlich");

				if (string.IsNullOrWhiteSpace(model.LastName))
					errors.Add("Ein Nachname ist erforderlich");

				if (string.IsNullOrWhiteSpace(model.EmailAddress))
					errors.Add("Eine E-Mail Adresse ist erforderlich");

				if (string.IsNullOrWhiteSpace(model.Phone))
					errors.Add("Eine Telefonnummer ist erforderlich");

				if (authUser != null && !authUser.IsAdmin)
				{
					var department = await _dbContext.Departments
						.Where(d => !d.IsDeleted)
						.Where(d => d.Id == authUser.DepartmentId)
						.FirstOrDefaultAsync(cancellationToken);

					// "normal" users can only register participants for their own department
					if (model.Department?.Trim().ToLower() != department?.Name.ToLower())
						errors.Add("Der Bereich ist ungültig");
				}

				if (string.IsNullOrWhiteSpace(model.Street) || string.IsNullOrWhiteSpace(model.ZipCode) || string.IsNullOrWhiteSpace(model.City))
					errors.Add("Eine Adresse ist erforderlich");

				if (!Enum.IsDefined(typeof(HotelRequirement), model.HotelRequirement))
					errors.Add("Die Übernachtung entspricht keiner Auswahl");

				if (model.FoodSelection.HasValue && !Enum.IsDefined(typeof(FoodSelection), model.FoodSelection.Value))
					errors.Add("Der Essenswunsch entspricht keiner Auswahl");

				if (model.AssumptionOfCosts)
				{
					if (string.IsNullOrWhiteSpace(model.InvoiceName))
						errors.Add("Der Name der Gliederung ist erforderlich");

					if (string.IsNullOrWhiteSpace(model.InvoiceStreet) || string.IsNullOrWhiteSpace(model.InvoiceZipCode) || string.IsNullOrWhiteSpace(model.InvoiceCity))
						errors.Add("Die Adresse der Gliederung ist erforderlich");
				}

				if (errors.Any())
					return BadRequest(string.Join("\n", errors));

				model.EmailAddress = model.EmailAddress.Trim().ToLower();
				model.Phone = model.Phone.Trim();

				if (!model.EmailAddress.IsValidEmailAddress(checkForDnsRecord: true))
					errors.Add("Die E-Mail Adresse ist nicht gültig");

				if (!await _fakeFilterService.IsEmailValid(model.EmailAddress, cancellationToken))
					errors.Add("Die E-Mail Adresse ist nicht zulässig");

				if (!Regex.IsMatch(model.Phone, @"^\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$"))
					errors.Add("Die Telefonnummer ist nicht gültig");

				if (model.Birthday >= DateTime.Now.Date)
					errors.Add("Das Geburtsdatum ist zu jung");

				if (errors.Any())
					return BadRequest(string.Join("\n", errors));

				// Try to get by e-mail address _and_ phone number
				var participant = await _dbContext.Participants
					.Where(p => p.EmailAddress == model.EmailAddress)
					.Where(p => p.PhoneNumber == model.Phone)
					.FirstOrDefaultAsync(cancellationToken);

				// Try to get by email address only
				if (participant == null && !string.IsNullOrWhiteSpace(model.EmailAddress))
				{
					int count = await _dbContext.Participants
						.Where(p => p.EmailAddress == model.EmailAddress)
						.CountAsync(cancellationToken);

					if (count == 1)
					{
						participant = await _dbContext.Participants
							.Where(p => p.EmailAddress == model.EmailAddress)
							.FirstOrDefaultAsync(cancellationToken);
					}
				}

				// Try to get by phone number only
				if (participant == null && !string.IsNullOrWhiteSpace(model.Phone))
				{
					int count = await _dbContext.Participants
						.Where(p => p.PhoneNumber == model.Phone)
						.CountAsync(cancellationToken);

					if (count == 1)
					{
						participant = await _dbContext.Participants
							.Where(p => p.PhoneNumber == model.Phone)
							.FirstOrDefaultAsync(cancellationToken);
					}
				}

				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				// No match possible - create new
				if (participant == null)
				{
					participant = new Participant
					{
						FirstName = model.FirstName.Trim(),
						LastName = model.LastName.Trim(),
						EmailAddress = model.EmailAddress,
						PhoneNumber = model.Phone,
						Birthday = model.Birthday,
						PlaceOfBirth = model.PlaceOfBirth?.Trim(),

						Street = model.Street.Trim(),
						AddressAddition = model.AddressAddition?.Trim(),
						ZipCode = model.ZipCode.Trim(),
						City = model.City.Trim(),
						Country = model.Country?.Trim(),

						IsEnabled = true
					};
					await _dbContext.Participants.AddAsync(participant, cancellationToken);
				}

				participant.AssistanceRequired = model.AssistanceRequired;
				participant.FoodSelection = model.FoodSelection;
				participant.Allergies = model.Allergies?.Trim();

				if (await _dbContext.ParticipantTrainingMappings
					.Where(m => m.TrainingId == training.Id)
					.Where(m => m.ParticipantId == participant.Id)
					.AnyAsync(cancellationToken))
				{
					errors.Add("Eine Anmeldung für den Lehrgang liegt bereits vor");
					return Conflict(string.Join("\n", errors));
				}

				string driverLicenseClasses = null;
				if (!string.IsNullOrWhiteSpace(model.DriverLicenseClasses))
				{
					var list = new List<string>();
					string[] classes = model.DriverLicenseClasses.Split(',', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
					foreach (string dc in classes)
					{
						if (Enum.TryParse<DriverLicense>(dc, out var license))
							list.Add(license.ToString());
					}

					if (list.Any())
						driverLicenseClasses = string.Join(",", list.OrderBy(e => e));
				}

				var mapping = new ParticipantTrainingMapping
				{
					CreatedAt = DateTime.UtcNow,
					Participant = participant,
					Training = training,
					AssumptionOfCosts = model.AssumptionOfCosts,
					DriverLicenseClasses = driverLicenseClasses,
					DriverPermission = model.DriverPermission,
					ExternalDepartment = model.Department,
					HotelRequirement = model.HotelRequirement,

					Street = model.Street,
					AddressAddition = model.AddressAddition,
					ZipCode = model.ZipCode,
					City = model.City,
					Country = model.Country,
				};

				if (model.AssumptionOfCosts)
				{
					mapping.InvoiceCity = model.InvoiceCity;
					mapping.InvoiceName = model.InvoiceName;
					mapping.InvoiceStreet = model.InvoiceStreet;
					mapping.InvoiceZipCode = model.InvoiceZipCode;
				}

				if (!string.IsNullOrWhiteSpace(model.Department))
				{
					var departments = await _dbContext.Departments
						.Where(d => !d.IsDeleted)
						.ToListAsync(cancellationToken);

					string cleanPattern = @"[^a-zA-Z0-9]";
					string cleanDepartmentName = Regex.Replace(model.Department, cleanPattern, "").Trim().ToLower();

					participant.DepartmentId = departments
						.Where(d => Regex.Replace(d.Name, cleanPattern, "").Trim().ToLower() == cleanDepartmentName)
						.FirstOrDefault()?.Id;
					mapping.DepartmentId = participant.DepartmentId;
				}

				if (authUser != null)
					mapping.Status |= ParticipantTrainingStatus.Confirmed;

				await _dbContext.ParticipantTrainingMappings.AddAsync(mapping, cancellationToken);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				int? availableCount = null;
				if (training.MaxParticipants.HasValue)
				{
					int participantCount = await _dbContext.ParticipantTrainingMappings
						.Where(m => m.TrainingId == training.Id)
						.CountAsync(cancellationToken);
					availableCount = Math.Max(0, training.MaxParticipants.Value - participantCount);
				}
				await _webClientHub.Clients.All.SendAsync(nameof(WebClientHub.UpdateAvailableCount), training.Id, availableCount, cancellationToken);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Registering participant ({model.EmailAddress}) to training ({model.TrainingId}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		#endregion AJAX calls

		private async Task AddTrainingInformation(Training training, CancellationToken cancellationToken = default)
		{
			training.CanRegister = !training.IsCancelled && (!training.DeadlineDays.HasValue || DateTime.Now.AddDays(training.DeadlineDays.Value) < training.Start);
			training.PartipicantCount = await _dbContext.ParticipantTrainingMappings.Where(m => m.TrainingId == training.Id).CountAsync(cancellationToken);

			if (training.MaxParticipants.HasValue)
			{
				training.AvailableCount = Math.Max(0, training.MaxParticipants.Value - training.PartipicantCount);
				if (training.AvailableCount == 0)
					training.CanRegister = false;
			}
		}
	}
}
