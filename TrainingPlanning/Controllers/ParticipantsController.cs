﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Models;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MimeKit;
using MiniExcelLibs;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class ParticipantsController : Controller
	{
		private readonly ILogger _logger;
		private readonly ServerDbContext _dbContext;
		private readonly ExtendedFakeFilterService _fakeFilterService;

		public ParticipantsController(ILogger<ParticipantsController> logger, ServerDbContext dbContext, ExtendedFakeFilterService fakeFilterService)
		{
			_logger = logger;
			_dbContext = dbContext;
			_fakeFilterService = fakeFilterService;
		}

		[HttpGet]
		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			var model = new ParticipantsViewModel
			{
				Departments = await _dbContext.Departments
					.OrderBy(d => d.Name)
					.ToListAsync(cancellationToken),
				Participants = await _dbContext.Participants
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.OrderByDescending(p => p.IsEnabled)
					.ThenBy(p => p.LastName)
					.ThenBy(p => p.FirstName)
					.ToListAsync(cancellationToken)
			};

			ViewData["Title"] = "Teilnehmer";
			return View(model);
		}

		[HttpGet]
		public async Task<IActionResult> Create(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			var model = new ParticipantsViewModel
			{
				Departments = await _dbContext.Departments
					.Where(d => !d.IsDeleted)
					.OrderBy(d => d.Name)
					.ToListAsync(cancellationToken),

				IsEnabled = true
			};

			ViewData["Title"] = "Neuer Teilnehmer";
			return View(nameof(Edit), model);
		}

		[HttpGet]
		public async Task<IActionResult> Edit(int id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			var participant = await _dbContext.Participants
				.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
				.Where(p => p.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (participant == null)
				return NotFound();

			var model = new ParticipantsViewModel
			{
				Departments = await _dbContext.Departments
					.Where(d => !d.IsDeleted)
					.OrderBy(d => d.Name)
					.ToListAsync(cancellationToken),

				Id = participant.Id,
				IsEnabled = participant.IsEnabled,
				DepartmentId = participant.DepartmentId,

				FirstName = participant.FirstName,
				LastName = participant.LastName,
				Birthday = participant.Birthday,
				PlaceOfBirth = participant.PlaceOfBirth,

				EmailAddress = participant.EmailAddress,
				PhoneNumber = participant.PhoneNumber,
				Street = participant.Street,
				AddressAddition = participant.AddressAddition,
				ZipCode = participant.ZipCode,
				City = participant.City,
				Country = participant.Country,

				Allergies = participant.Allergies,
				FoodSelection = participant.FoodSelection,
				DriverLicenseClasses = participant.DriverLicenseClasses,
				DriverPermission = participant.DriverPermission,
				AssistanceRequired = participant.AssistanceRequired,
			};

			ViewData["Title"] = "Teilnehmer bearbeiten";
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(ParticipantsViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			if (!authUser.IsAdmin)
				model.DepartmentId = authUser.DepartmentId;

			ViewData["Title"] = model.Id > 0 ? "Teilnehmer bearbeiten" : "Neuer Teilnehmer";
			model.Departments = await _dbContext.Departments
				.Where(d => !d.IsDeleted)
				.OrderBy(d => d.Name)
				.ToListAsync(cancellationToken);

			if (string.IsNullOrWhiteSpace(model.FirstName))
				ModelState.AddModelError(nameof(model.FirstName), "Ein Vorname ist erforderlich");

			if (string.IsNullOrWhiteSpace(model.LastName))
				ModelState.AddModelError(nameof(model.LastName), "Ein Nachname ist erforderlich");

			if (model.Birthday <= DateTime.Now.AddYears(-100) || model.Birthday >= DateTime.Now)
				ModelState.AddModelError(nameof(model.Birthday), "Das Geburtsdatum ist außerhalb der gültigen Werte");

			if (string.IsNullOrWhiteSpace(model.EmailAddress))
				ModelState.AddModelError(nameof(model.EmailAddress), "Eine E-Mail Adresse ist erforderlich");

			if (authUser.IsAdmin && model.DepartmentId.HasValue && !await _dbContext.Departments.Where(d => !d.IsDeleted).Where(d => d.Id == model.DepartmentId).AnyAsync(cancellationToken))
				ModelState.AddModelError(nameof(model.DepartmentId), "Der Bereich ist nicht verfügbar");

			if (!ModelState.IsValid)
				return View(model);

			model.EmailAddress = model.EmailAddress.Trim().ToLower();

			if (!model.EmailAddress.IsValidEmailAddress())
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist nicht gültig");
				return View(model);
			}

			if (!await _fakeFilterService.IsEmailValid(model.EmailAddress, cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Der E-Mail Anbieter wird als unsicher eingestuft (FakeFilter API)");
				return View(model);
			}

			if (await _dbContext.Participants.Where(p => p.Id != model.Id).Where(p => p.EmailAddress == model.EmailAddress).AnyAsync(cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist bereits in Verwendung");
				return View(model);
			}

			var participant = await _dbContext.Participants
				.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
				.Where(p => p.Id == model.Id)
				.FirstOrDefaultAsync(cancellationToken);
			if (participant == null)
			{
				participant = new Participant();
				await _dbContext.Participants.AddAsync(participant, cancellationToken);
			}

			if (authUser.IsAdmin)
				participant.DepartmentId = model.DepartmentId;

			participant.IsEnabled = model.IsEnabled;
			participant.FirstName = model.FirstName?.Trim();
			participant.LastName = model.LastName?.Trim();
			participant.Birthday = model.Birthday;
			participant.PlaceOfBirth = model.PlaceOfBirth?.Trim();

			participant.EmailAddress = model.EmailAddress;
			participant.PhoneNumber = model.PhoneNumber?.Trim();
			participant.Street = model.Street?.Trim();
			participant.AddressAddition = model.AddressAddition?.Trim();
			participant.ZipCode = model.ZipCode?.Trim();
			participant.City = model.City?.Trim();
			participant.Country = model.Country?.Trim();

			participant.Allergies = model.Allergies?.Trim();
			participant.FoodSelection = model.FoodSelection;
			participant.DriverPermission = model.DriverPermission;
			participant.AssistanceRequired = model.AssistanceRequired;

			participant.DriverLicenseClasses = null;
			if (!string.IsNullOrWhiteSpace(model.DriverLicenseClasses))
			{
				var list = new List<string>();
				string[] classes = model.DriverLicenseClasses.Split(",", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
				foreach (string dc in classes)
				{
					if (Enum.TryParse<DriverLicense>(dc, out var license))
						list.Add(license.ToString());
				}

				if (list.Any())
					participant.DriverLicenseClasses = string.Join(",", list.OrderBy(e => e));
			}

			try
			{
				await _dbContext.SaveChangesAsync(cancellationToken);
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Saving participant failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			var participant = await _dbContext.Participants
				.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
				.Where(p => p.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (participant == null)
				return NotFound();

			try
			{
				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				await EmailDeletedParticipant(participant, cancellationToken);

				var mappings = await _dbContext.ParticipantTrainingMappings
					.Where(m => m.ParticipantId == id)
					.ToListAsync(cancellationToken);

				_dbContext.ParticipantTrainingMappings.RemoveRange(mappings);
				_dbContext.Participants.Remove(participant);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Deleting participant failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteMulti(int[] ids, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			string url = Url.Action(nameof(Index), "Participants");

			try
			{
				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				var participants = await _dbContext.Participants
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.Where(p => ids.Contains(p.Id))
					.ToListAsync(cancellationToken);

				foreach (var participant in participants)
					await EmailDeletedParticipant(participant, cancellationToken);

				var pIds = participants.Select(p => p.Id).ToList();

				var mappings = await _dbContext.ParticipantTrainingMappings
					.Where(m => pIds.Contains(m.ParticipantId))
					.ToListAsync(cancellationToken);

				_dbContext.ParticipantTrainingMappings.RemoveRange(mappings);
				_dbContext.Participants.RemoveRange(participants);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return Ok(url);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Deleting participants failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		[HttpGet]
		public async Task<IActionResult> Export(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			try
			{
				var participants = _dbContext.Participants
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.Include(p => p.Department)
					.OrderBy(p => p.LastName)
					.ThenBy(p => p.FirstName)
					.Select(p => new ParticipantExportModel
					{
						FirstName = p.FirstName,
						LastName = p.LastName,
						Birthday = p.Birthday,
						PlaceOfBirth = p.PlaceOfBirth,
						DepartmentName = p.Department == null ? null : p.Department.Name,
						EmailAddress = p.EmailAddress,
						PhoneNumber = p.PhoneNumber,
						Street = p.Street,
						AddressAddition = p.AddressAddition,
						ZipCode = p.ZipCode,
						City = p.City,
						Country = p.Country,
						DriverLicenseClasses = p.DriverLicenseClasses,
						DriverPermission = p.DriverPermission ? "Ja" : "Nein",
						Assistance = p.AssistanceRequired ? "Ja" : "Nein",
						FoodSelection = p.FoodSelection,
						Allergies = p.Allergies,
					});

				using var ms = new MemoryStream();
				await ms.SaveAsAsync(participants, printHeader: true, sheetName: "Export", cancellationToken: cancellationToken);

				return File(fileContents: ms.ToArray(),
					contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
					fileDownloadName: "Teilnehmer_Export.xlsx",
					enableRangeProcessing: true);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Exporting participants failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Import(IFormFile file, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			try
			{
				using var ms = new MemoryStream();
				await file.CopyToAsync(ms, cancellationToken);

				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				foreach (dynamic row in ms.Query(useHeaderRow: true, sheetName: "Import"))
				{
					var jRow = ((object)row).ConvertToJObject();

					string emailAddress = jRow.GetValue<string>("E-Mail Adresse")?.Trim().ToLower();
					string phone = jRow.GetValue<string>("Telefon")?.Trim();

					// Try to get by e-mail address _and_ phone number
					var participant = await _dbContext.Participants
						.Where(p => p.EmailAddress == emailAddress)
						.Where(p => p.PhoneNumber == phone)
						.FirstOrDefaultAsync(cancellationToken);

					// Try to get by email address only
					if (participant == null && !string.IsNullOrWhiteSpace(emailAddress))
					{
						int count = await _dbContext.Participants
							.Where(p => p.EmailAddress == emailAddress)
							.CountAsync(cancellationToken);

						if (count == 1)
						{
							participant = await _dbContext.Participants
								.Where(p => p.EmailAddress == emailAddress)
								.FirstOrDefaultAsync(cancellationToken);
						}
					}

					// Try to get by phone number only
					if (participant == null && !string.IsNullOrWhiteSpace(phone))
					{
						int count = await _dbContext.Participants
							.Where(p => p.PhoneNumber == phone)
							.CountAsync(cancellationToken);

						if (count == 1)
						{
							participant = await _dbContext.Participants
								.Where(p => p.PhoneNumber == phone)
								.FirstOrDefaultAsync(cancellationToken);
						}
					}

					// Delete the listed participant?
					if (!string.IsNullOrWhiteSpace(jRow.GetValue<string>("DELETE")))
					{
						if (participant != null)
						{
							await EmailDeletedParticipant(participant, cancellationToken);

							var mappings = await _dbContext.ParticipantTrainingMappings
								.Where(m => m.ParticipantId == participant.Id)
								.ToListAsync(cancellationToken);

							_dbContext.ParticipantTrainingMappings.RemoveRange(mappings);
							_dbContext.Participants.Remove(participant);

							await _dbContext.SaveChangesAsync(cancellationToken);
						}
						continue;
					}

					// No match possible - create new
					if (participant == null)
					{
						participant = new Participant
						{
							IsEnabled = true
						};
						await _dbContext.Participants.AddAsync(participant, cancellationToken);
					}

					participant.FirstName = jRow.GetValue<string>("Vorname");
					participant.LastName = jRow.GetValue<string>("Nachname");
					participant.Birthday = jRow.GetValue<DateTime>("Geburtsdatum");
					participant.PlaceOfBirth = jRow.GetValue<string>("Geburtsort");

					string departmentName = jRow.GetValue<string>("Bereich")?.Trim();
					var department = await _dbContext.Departments
						.Where(d => !d.IsDeleted)
						.Where(d => d.Name == departmentName)
						.FirstOrDefaultAsync(cancellationToken);

					participant.DepartmentId = department?.Id;

					participant.EmailAddress = emailAddress;
					participant.PhoneNumber = phone;
					participant.Street = jRow.GetValue<string>("Straße + Haus Nr.");
					participant.AddressAddition = jRow.GetValue<string>("Adress-Zusatz");
					participant.ZipCode = jRow.GetValue<string>("PLZ");
					participant.City = jRow.GetValue<string>("Stadt");
					participant.Country = jRow.GetValue<string>("Land");
					participant.DriverLicenseClasses = null;

					string[] classes = (jRow.GetValue<string>("Führerscheinklassen") ?? "").Split(',', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
					var list = new List<string>();
					foreach (string dc in classes)
					{
						if (Enum.TryParse<DriverLicense>(dc, out var license))
							list.Add(license.ToString());
					}
					if (list.Any())
						participant.DriverLicenseClasses = string.Join(",", list.OrderBy(e => e));

					participant.DriverPermission = jRow.GetValue<string>("Fahrfreigabe")?.Equals("Ja", StringComparison.OrdinalIgnoreCase) == true;

					participant.AssistanceRequired = jRow.GetValue<string>("Assistenzbedarf")?.Equals("Ja", StringComparison.OrdinalIgnoreCase) == true;
					participant.FoodSelection = null;
					participant.Allergies = jRow.GetValue<string>("Allergien");

					string foodSelection = jRow.GetValue<string>("Essenswunsch");
					foreach (var enumValue in Enum.GetValues<FoodSelection>())
					{
						if (enumValue.GetDescription().Equals(foodSelection, StringComparison.OrdinalIgnoreCase))
							participant.FoodSelection = enumValue;
					}

					await _dbContext.SaveChangesAsync(cancellationToken);
				}
				await transaction.CommitAsync(cancellationToken);

				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				if (ex.Source == "MiniExcel")
				{
					_logger.LogWarning(ex, $"No participants imported: {ex.GetMessage()}");
					return RedirectToAction(nameof(Index));
				}

				_logger.LogError(ex, $"Importing participants failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Get(int[] ids, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();

			try
			{
				var participants = await _dbContext.Participants
					.Include(p => p.Department)
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.Where(p => ids.Contains(p.Id))
					.ToListAsync(cancellationToken);

				participants.ForEach(p =>
				{
					if (p.Department?.IsDeleted == true)
						p.Department = null;
				});

				return Json(participants);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Reading participants failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Merge(string idList, string data, CancellationToken cancellationToken)
		{
			try
			{
				int[] ids = idList.Split(",", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).Select(i => int.Parse(i)).OrderBy(i => i).ToArray();
				var participant = data.DeserializeJson<Participant>();

				int keepId = ids.First();
				int[] deleteIds = ids.Except(new[] { keepId }).ToArray();

				var authUser = HttpContext.GetAuthUser();

				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				var participantToKeep = await _dbContext.Participants
					.Where(p => p.Id == keepId)
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.FirstOrDefaultAsync(cancellationToken);
				if (participantToKeep == null)
					return NotFound("Higher permissions required");

				var participantsToDelete = await _dbContext.Participants
					.Where(p => deleteIds.Contains(p.Id))
					.Where(p => p.DepartmentId == authUser.DepartmentId || authUser.IsAdmin)
					.ToListAsync(cancellationToken);
				if (participantsToDelete.Count != deleteIds.Length)
					return Conflict("Higher permissions required");

				participantToKeep.FirstName = participant.FirstName;
				participantToKeep.LastName = participant.LastName;
				participantToKeep.EmailAddress = participant.EmailAddress;
				participantToKeep.PhoneNumber = participant.PhoneNumber;
				participantToKeep.Birthday = participant.Birthday;
				participantToKeep.PlaceOfBirth = participant.PlaceOfBirth;
				participantToKeep.DepartmentId = participant.DepartmentId;
				participantToKeep.Street = participant.Street;
				participantToKeep.AddressAddition = participant.AddressAddition;
				participantToKeep.ZipCode = participant.ZipCode;
				participantToKeep.City = participant.City;
				participantToKeep.Country = participant.Country;
				participantToKeep.DriverLicenseClasses = participant.DriverLicenseClasses;
				participantToKeep.DriverPermission = participant.DriverPermission;
				participantToKeep.AssistanceRequired = participant.AssistanceRequired;
				participantToKeep.FoodSelection = participant.FoodSelection;
				participantToKeep.Allergies = participant.Allergies;
				participantToKeep.IsEnabled = participant.IsEnabled;

				var bookedTrainings = new HashSet<string>();
				(await _dbContext.ParticipantTrainingMappings
					.Where(m => m.ParticipantId == participantToKeep.Id)
					.Select(m => m.TrainingId)
					.ToListAsync(cancellationToken))
						.ForEach(id => bookedTrainings.Add(id));

				foreach (var participantToDelete in participantsToDelete)
				{
					var mappingsToMove = await _dbContext.ParticipantTrainingMappings
						.Where(m => m.ParticipantId == participantToDelete.Id)
						.ToListAsync(cancellationToken);
					foreach (var mapping in mappingsToMove)
					{
						_dbContext.ParticipantTrainingMappings.Remove(mapping);
						if (bookedTrainings.Add(mapping.TrainingId))
						{
							await _dbContext.ParticipantTrainingMappings.AddAsync(new ParticipantTrainingMapping
							{
								TrainingId = mapping.TrainingId,
								ParticipantId = participantToKeep.Id,
								DepartmentId = participantToKeep.DepartmentId,

								Street = participantToKeep.Street,
								AddressAddition = participantToKeep.AddressAddition,
								ZipCode = participantToKeep.ZipCode,
								City = participantToKeep.City,
								Country = participantToKeep.Country,

								DriverLicenseClasses = mapping.DriverLicenseClasses,
								DriverPermission = mapping.DriverPermission,
								HotelRequirement = mapping.HotelRequirement,

								AssumptionOfCosts = mapping.AssumptionOfCosts,
								InvoiceName = mapping.InvoiceName,
								InvoiceStreet = mapping.InvoiceStreet,
								InvoiceZipCode = mapping.InvoiceZipCode,
								InvoiceCity = mapping.InvoiceCity,

								Notes = mapping.Notes,
								Status = mapping.Status,
								CreatedAt = mapping.CreatedAt,
							}, cancellationToken);
						}
					}
					_dbContext.Participants.Remove(participantToDelete);
					await _dbContext.SaveChangesAsync(cancellationToken);
				}

				await transaction.CommitAsync(cancellationToken);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Merging participants failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		private async Task EmailDeletedParticipant(Participant participant, CancellationToken cancellationToken = default)
		{
			var trainingIds = await _dbContext.ParticipantTrainingMappings
				.Where(m => m.ParticipantId == participant.Id)
				.Select(m => m.TrainingId)
				.ToListAsync(cancellationToken);
			if (!trainingIds.Any())
				return;

			var department = await _dbContext.Departments
				.Where(d => d.Id == participant.DepartmentId)
				.FirstOrDefaultAsync(cancellationToken);

			var futureTrainings = await _dbContext.Trainings
				.Where(t => trainingIds.Contains(t.Id))
				.Where(t => t.Start >= DateTime.Now)
				.ToListAsync(cancellationToken);

			var sb = new StringBuilder();
			sb.AppendLine("+++++ INFORMATION START: Löschung Teilnehmer +++++");
			sb.AppendLine();
			sb.AppendLine();
			sb.AppendLine("Daten Teilnehmer");
			sb.AppendLine("----------------");
			sb.AppendLine();

			sb.AppendLine($"Vorname:      {participant.FirstName}");
			sb.AppendLine($"Nachname:     {participant.LastName}");
			sb.AppendLine($"Geburtsdatum: {participant.Birthday:dd.MM.yyyy}");
			sb.AppendLine($"Geburtsort:   {participant.PlaceOfBirth}");
			sb.AppendLine($"Bereich:      {department?.Name ?? "-"}");
			sb.AppendLine();

			sb.AppendLine($"E-Mail Adresse: {participant.EmailAddress}");
			sb.AppendLine($"Telefonnummer:  {participant.PhoneNumber}");
			sb.AppendLine();

			sb.AppendLine($"Adresse:");
			sb.AppendLine($"{participant.Street}");
			if (!string.IsNullOrWhiteSpace(participant.AddressAddition))
				sb.AppendLine($"{participant.AddressAddition}");
			sb.AppendLine($"{participant.ZipCode} {participant.City}");
			if (!string.IsNullOrWhiteSpace(participant.Country))
				sb.AppendLine($"{participant.Country}");
			sb.AppendLine();

			sb.AppendLine($"Essenswunsch:    {participant.FoodSelection?.GetDisplayName() ?? "-"}");
			sb.AppendLine($"Allergien:       {participant.Allergies}");
			sb.AppendLine($"Assistenzbedarf: {(participant.AssistanceRequired ? "Ja" : "Nein")}");
			sb.AppendLine();
			sb.AppendLine($"Führerscheinklassen:   {participant.DriverLicenseClasses}");
			sb.AppendLine($"Fahrberechtigung KatS: {(participant.DriverPermission ? "Ja" : "Nein")}");
			sb.AppendLine();

			if (futureTrainings.Any())
			{
				sb.AppendLine();
				sb.AppendLine("Stornierte Lehrgänge");
				sb.AppendLine("--------------------");
				sb.AppendLine();
				foreach (var training in futureTrainings)
					sb.AppendLine($"- {training.Title} ({training.Start:dd.MM.yyyy HH:mm} - {training.End:dd.MM.yyyy HH:mm})");

				sb.AppendLine();
			}

			sb.AppendLine();
			sb.AppendLine("+++++ INFORMATION ENDE: Löschung Teilnehmer +++++");

			var emailMessage = new EmailMessage
			{
				Message = sb.ToString(),
				Subject = "Teilnehmer gelöscht",
				Priority = futureTrainings.Any() ? MessageImportance.High : MessageImportance.Normal,
				RecipientsVisible = false
			};

			var users = await _dbContext.Users
				.Where(u => u.IsEnabled)
				.Where(u => u.IsAdmin || u.DepartmentId == participant.DepartmentId)
				.ToListAsync(cancellationToken);

			foreach (var user in users)
			{
				emailMessage.Recipients.Add(new EmailAddress
				{
					Address = user.EmailAddress,
					Name = user.GetName()
				});
			}

			await _dbContext.EmailMessages.AddAsync(emailMessage, cancellationToken);
			await _dbContext.SaveChangesAsync(cancellationToken);
		}
	}
}
