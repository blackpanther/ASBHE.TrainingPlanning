﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Models;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class UsersController : Controller
	{
		private readonly ILogger<UsersController> _logger;
		private readonly ServerDbContext _dbContext;
		private readonly ExtendedFakeFilterService _fakeFilterService;

		public UsersController(ILogger<UsersController> logger, ServerDbContext dbContext, ExtendedFakeFilterService fakeFilterService)
		{
			_logger = logger;
			_dbContext = dbContext;
			_fakeFilterService = fakeFilterService;
		}

		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var model = new UsersViewModel
			{
				Users = await _dbContext.Users
					.OrderByDescending(u => u.IsEnabled)
					.ThenBy(u => u.EmailAddress)
					.ToListAsync(cancellationToken),
				Departments = await _dbContext.Departments
					.OrderBy(d => d.IsDeleted)
					.ThenBy(d => d.Name)
					.ToListAsync(cancellationToken)
			};

			ViewData["Title"] = "Benutzer";
			return View(model);
		}

		public async Task<IActionResult> Create(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var model = new UsersViewModel
			{
				Departments = await _dbContext.Departments
					.OrderBy(d => d.IsDeleted)
					.ThenBy(d => d.Name)
					.ToListAsync(cancellationToken),
				IsEnabled = true
			};

			ViewData["Title"] = "Neuer Benutzer";
			return View(nameof(Edit), model);
		}

		public async Task<IActionResult> Edit(int id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var user = await _dbContext.Users
				.Where(u => u.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (user == null)
				return NotFound();

			var model = new UsersViewModel
			{
				Departments = await _dbContext.Departments
					.OrderBy(d => d.IsDeleted)
					.ThenBy(d => d.Name)
					.ToListAsync(cancellationToken),
				Id = user.Id,
				EmailAddress = user.EmailAddress,
				FirstName = user.FirstName,
				LastName = user.LastName,
				IsAdmin = user.IsAdmin,
				IsEnabled = user.IsEnabled,
				DepartmentId = user.DepartmentId
			};

			ViewData["Title"] = "Benutzer bearbeiten";
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(UsersViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			model.Departments = await _dbContext.Departments
					.OrderBy(d => d.IsDeleted)
					.ThenBy(d => d.Name)
					.ToListAsync(cancellationToken);
			ViewData["Title"] = model.Id > 0 ? "Benutzer bearbeiten" : "Neuer Benutzer";

			if (string.IsNullOrWhiteSpace(model.EmailAddress))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Eine E-Mail Adresse ist erforderlich");
			}

			model.EmailAddress = model.EmailAddress.Trim().ToLower();

			if (!model.EmailAddress.IsValidEmailAddress())
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist nicht gültig");
				return View(model);
			}

			if (!await _fakeFilterService.IsEmailValid(model.EmailAddress, cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Der E-Mail Anbieter wird als unsicher eingestuft (FakeFilter API)");
				return View(model);
			}

			if (await _dbContext.Users.Where(u => u.Id != model.Id).Where(u => u.EmailAddress == model.EmailAddress).AnyAsync(cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist bereits in Verwendung");
				return View(model);
			}

			try
			{
				var user = await _dbContext.Users
					.Where(u => u.Id == model.Id)
					.FirstOrDefaultAsync(cancellationToken);
				if (user == null)
				{
					user = new User
					{
						Id = _dbContext.CreateUserId()
					};
					await _dbContext.Users.AddAsync(user, cancellationToken);
				}

				user.EmailAddress = model.EmailAddress;
				user.FirstName = model.FirstName;
				user.LastName = model.LastName;
				user.IsEnabled = model.IsEnabled;
				user.IsAdmin = model.IsAdmin;
				user.DepartmentId = model.DepartmentId;

				// You're not allowed to downgrade yourself.
				if (user.Id == authUser.Id)
				{
					user.IsEnabled = true;
					user.IsAdmin = true;
				}

				await _dbContext.SaveChangesAsync(cancellationToken);
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Saving user (#{model.Id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Impersonate(int id, CancellationToken cancellationToken)
		{
			try
			{
				var authUser = HttpContext.GetAuthUser();
				if (!authUser.IsAdmin)
					return Forbid();

				// You're not allowed to impersonate yourself.
				if (authUser.Id == id)
					return BadRequest();

				// Already impersonating
				if (authUser.ImpersonatingId.HasValue)
					return BadRequest();

				var user = await _dbContext.Users
					.Where(u => u.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (user == null)
					return NotFound();

				user.ImpersonatingId = authUser.Id;
				await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user.GetPrincipal(), new AuthenticationProperties
				{
					AllowRefresh = true,
					IsPersistent = true
				});

				return RedirectToAction("Index", "Trainings");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Deleting user (#{id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
		{
			try
			{
				var authUser = HttpContext.GetAuthUser();
				if (!authUser.IsAdmin)
					return Forbid();

				// You're not allowed to delete yourself.
				if (authUser.Id == id)
					return BadRequest();

				var user = await _dbContext.Users
					.Where(u => u.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (user == null)
					return NotFound();

				_dbContext.Users.Remove(user);
				await _dbContext.SaveChangesAsync(cancellationToken);

				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Deleting user (#{id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> GeneratePassword(int id, CancellationToken cancellationToken)
		{
			try
			{
				var authUser = HttpContext.GetAuthUser();
				if (!authUser.IsAdmin)
					return Forbid();

				// You're not allowed to send yourself a new password.
				if (authUser.Id == id)
					return BadRequest();

				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				var user = await _dbContext.Users
					.Where(u => u.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (user == null)
					return NotFound();

				string password = CryptographyHelper.GetRandomString(14);

				var sb = new StringBuilder();
				sb.AppendLine($"Hallo {user.GetName()},");
				sb.AppendLine();
				sb.AppendLine("Es wurde ein neues Kennwort generiert.");
				sb.AppendLine("Das neue Kennwort ist ab sofort gültig.");
				sb.AppendLine();
				sb.AppendLine($"Es lautet: {password}");
				sb.AppendLine();
				sb.AppendLine("Sollte kein neues Kennwort angefordert worden sein, so wäre eine Meldung beim Administrator sinnvoll.");
				sb.AppendLine();

				var msg = new EmailMessage
				{
					Priority = MimeKit.MessageImportance.High,
					Subject = "Neues Kennwort",
					Message = sb.ToString(),
					IsHtml = false,
				};
				msg.Recipients.Add(new EmailAddress { Name = user.GetName(), Address = user.EmailAddress });
				await _dbContext.EmailMessages.AddAsync(msg, cancellationToken);

				user.PasswordHash = PasswordHelper.HashPassword(password);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Generating new password for user (#{id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}
	}
}
