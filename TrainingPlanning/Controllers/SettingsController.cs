﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class SettingsController : Controller
	{
		private readonly GlobalSettingsService _globalSettings;
		private readonly CryptographyHelper _crypto;

		public SettingsController(GlobalSettingsService globalSettings, CryptographyHelper crypto)
		{
			_globalSettings = globalSettings;
			_crypto = crypto;
		}

		public IActionResult Index()
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			ViewData["Title"] = "Einstellungen";
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> Get(string settingName, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			switch (settingName?.ToLower())
			{
				case "smtp":
					var smtpSettings = await _globalSettings.GetAsync<SmtpSettings>(settingName, cancellationToken: cancellationToken);
					return Json(smtpSettings);

				case "terms":
					string terms = await _globalSettings.GetAsync<string>(settingName, cancellationToken: cancellationToken);
					return Json(terms);

				default:
					throw new NotImplementedException();
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Set(string settingName, string settingsJson, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			switch (settingName?.ToLower())
			{
				case "smtp":
					var smtpSettings = settingsJson.DeserializeJson<SmtpSettings>();

					// There is a password, check whether we need to encrypt it
					// (decrypt will fail on plain password)
					if (!string.IsNullOrWhiteSpace(smtpSettings.Password))
					{
						try
						{
							string test = _crypto.DecryptAes(smtpSettings.Password);
						}
						catch
						{
							smtpSettings.Password = _crypto.EncryptAes(smtpSettings.Password);
						}
					}

					await _globalSettings.SetAsync(settingName, smtpSettings, cancellationToken);
					break;

				case "terms":
					string terms = settingsJson.DeserializeJson<string>();
					await _globalSettings.SetAsync(settingName, terms?.Trim(), cancellationToken);
					break;

				default:
					throw new NotImplementedException();
			}

			return Ok();
		}

		[HttpDelete]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(string settingName, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			await _globalSettings.DeleteAsync(settingName, cancellationToken);
			return Ok();
		}
	}
}
