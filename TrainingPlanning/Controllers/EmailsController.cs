﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Enums;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class EmailsController : Controller
	{
		private readonly IServiceScopeFactory _serviceScopeFactory;

		public EmailsController(IServiceScopeFactory serviceScopeFactory)
		{
			_serviceScopeFactory = serviceScopeFactory;
		}

		public IActionResult Index()
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			ViewData["Title"] = "E-Mail Versand";
			return View();
		}

		public async Task<IActionResult> Get(long id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			using var scope = _serviceScopeFactory.CreateScope();
			var dbContext = scope.ServiceProvider.GetService<ServerDbContext>();

			if (dbContext == null)
				return Json(Array.Empty<EmailMessage>());

			try
			{
				var timestamp = DateTime.UnixEpoch.AddMilliseconds(id);
				var emails = await dbContext.EmailMessages
					.Where(m => id == 0 || m.SendAfterDate <= timestamp)
					.OrderByDescending(m => m.SendAfterDate)
					.Take(20)
					.ToListAsync(cancellationToken);

				return Json(emails);
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetMessage());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Delete(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();

				var email = await dbContext.EmailMessages
					.Where(em => em.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (email == null)
					return NotFound("E-Mail konnte nicht gefunden werden");

				if (email.Status != EmailMessageStatus.Pending && email.Status != EmailMessageStatus.Error)
					return BadRequest($"Nur E-Mails im Status '{EmailMessageStatus.Pending.GetDisplayName()}' oder '{EmailMessageStatus.Error.GetDisplayName()}' können gelöscht werden");

				using var transaction = await dbContext.BeginTransactionAsync(cancellationToken);

				dbContext.EmailAttachments.RemoveRange(dbContext.EmailAttachments.Where(ea => ea.EmailMessageId == email.Id));
				dbContext.EmailMessages.Remove(email);

				await dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return Ok();
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetRecursiveMessage());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteMulti(string[] ids, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			if (ids == null || ids.Length == 0)
				return BadRequest();

			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var logger = scope.ServiceProvider.GetRequiredService<ILogger<EmailsController>>();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();

				var deletedIds = new List<string>();
				foreach (string id in ids)
				{
					var email = await dbContext.EmailMessages
							.Where(em => em.Id == id)
							.FirstOrDefaultAsync(cancellationToken);
					if (email == null)
						continue;

					if (email.Status != EmailMessageStatus.Pending && email.Status != EmailMessageStatus.Error)
						continue;

					try
					{
						using var transaction = await dbContext.BeginTransactionAsync(cancellationToken);

						dbContext.EmailAttachments.RemoveRange(dbContext.EmailAttachments.Where(ea => ea.EmailMessageId == email.Id));
						dbContext.EmailMessages.Remove(email);

						await dbContext.SaveChangesAsync(cancellationToken);
						await transaction.CommitAsync(cancellationToken);
						deletedIds.Add(email.Id);
					}
					catch (Exception ex)
					{
						logger.LogWarning(ex, $"Deleting email ({email.Id}) failed: {ex.GetMessage()}");
					}
				}

				return Json(deletedIds);
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetRecursiveMessage());
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ReSend(string id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			try
			{
				using var scope = _serviceScopeFactory.CreateScope();
				var dbContext = scope.ServiceProvider.GetRequiredService<ServerDbContext>();

				var email = await dbContext.EmailMessages
					.Where(em => em.Id == id)
					.FirstOrDefaultAsync(cancellationToken);
				if (email == null)
					return NotFound("E-Mail konnte nicht gefunden werden");

				if (email.Status != EmailMessageStatus.Sent && email.Status != EmailMessageStatus.Error)
					return BadRequest($"Nur E-Mails im Status '{EmailMessageStatus.Sent.GetDisplayName()}' oder '{EmailMessageStatus.Error.GetDisplayName()}' können erneut versendet werden");

				email.Status = EmailMessageStatus.Pending;
				email.UpdatedAt = DateTime.UtcNow;

				await dbContext.SaveChangesAsync(cancellationToken);
				return Json(email);
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetRecursiveMessage());
			}
		}
	}
}
