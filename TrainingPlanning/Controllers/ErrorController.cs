﻿using System.Diagnostics;
using AMWD.ASBHE.TrainingPlanning.Models;
using Microsoft.AspNetCore.Mvc;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Route("Error")]
	public class ErrorController : Controller
	{
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Index()
			=> Index(500);

		[Route("{errorCode}")]
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Index(int errorCode)
		{
			if (errorCode <= 0)
				errorCode = 500;

			ViewData["Title"] = $"Fehler {errorCode}";

			string requestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
			string originalPath = HttpContext.Items["OriginalPath"]?.ToString()?.Trim();
			if (!string.IsNullOrWhiteSpace(originalPath))
				originalPath = $" (<code>{originalPath}</code>)";

			var model = new ErrorViewModel
			{
				Description = $"Es ist ein Fehler aufgetreten. Sollte dies häufiger passieren, kontaktieren Sie einen Administrator. Anfrage-ID: <code>{requestId}</code>.",
				ErrorCode = errorCode,
				Icon = "fa-solid fa-triangle-exclamation",
				Title = $"Fehler {errorCode}",
			};

			switch (errorCode)
			{
				case 400:
					model.Title = "Ungültige Anfrage";
					model.Description = "Sie haben eine ungültige oder unvollständige Anfrage gesendet, die nicht verarbeitet werden kann.";
					break;

				case 401:
					model.Icon = "fa-solid fa-hand";
					model.Title = "Authorisierung erforderlich";
					model.Description = "Es konnte nicht geprüft werden, ob Sie zum Zugriff auf das Dokument berechtigt sind. Bitte melden Sie sich an.";
					break;

				case 403:
					model.Icon = "fa-solid fa-ban";
					model.Title = "Verboten";
					model.Description = "Sie verfügen nicht über die nötigen Berechtigungen, um auf das Dokument zuzugreifen.";
					break;

				case 404:
					model.Icon = "fa-solid fa-magnifying-glass";
					model.Title = "Nicht gefunden";
					model.Description = $"Das angeforderte Dokument{originalPath} wurde nicht gefunden.";
					break;

				case 405:
					model.Icon = "fa-solid fa-outlet";
					model.Title = "Methode nicht erlaubt";
					model.Description = $"Das Dokument{originalPath} wurde mit der falschen Methode angefragt (z.B. GET statt POST).";
					break;

				case 410:
					model.Icon = "fa-solid fa-shoe-prints";
					model.Title = "Vergangen";
					model.Description = "Das angefragte Dokument ist nicht mehr verfügbar.";
					break;

				case 500:
					model.Icon = "fa-solid fa-server";
					model.Title = "Interner Serverfehler";
					model.Description = "Der Server hat einen internen Fehler oder eine fehlerhafte Konfiguration festgestellt.";
					break;
			}

			return View(model);
		}
	}
}
