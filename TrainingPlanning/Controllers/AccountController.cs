﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Models;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using AMWD.ASBHE.TrainingPlanning.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly ILogger<AccountController> _logger;
		private readonly ServerDbContext _dbContext;
		private readonly ExtendedFakeFilterService _fakeFilterService;

		public AccountController(ILogger<AccountController> logger, ServerDbContext dbContext, ExtendedFakeFilterService fakeFilterService)
		{
			_logger = logger;
			_dbContext = dbContext;
			_fakeFilterService = fakeFilterService;
		}

		#region Profile

		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			ViewData["Title"] = "Profil";
			var authUser = HttpContext.GetAuthUser(_dbContext);

			var model = new AccountViewModel
			{
				EmailAddress = authUser.EmailAddress,
				FirstName = authUser.FirstName,
				LastName = authUser.LastName,
				Department = await _dbContext.Departments
					.Where(d => d.Id == authUser.DepartmentId)
					.FirstOrDefaultAsync(cancellationToken)
			};

			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Index(AccountViewModel model, CancellationToken cancellationToken)
		{
			ViewData["Title"] = "Profil";
			var authUser = HttpContext.GetAuthUser(_dbContext);

			if (authUser.ImpersonatingId.HasValue)
				return BadRequest();

			model.Department = await _dbContext.Departments
				.Where(d => d.Id == authUser.DepartmentId)
				.FirstOrDefaultAsync(cancellationToken);

			if (string.IsNullOrWhiteSpace(model.EmailAddress))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Eine E-Mail Adresse ist erforderlich");
				return View(model);
			}

			model.EmailAddress = model.EmailAddress.Trim().ToLower();

			if (!model.EmailAddress.IsValidEmailAddress())
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist nicht gültig");
				return View(model);
			}

			if (!await _fakeFilterService.IsEmailValid(model.EmailAddress, cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Der E-Mail Anbieter wird als unsicher eingestuft");
				return View(model);
			}

			if (await _dbContext.Users.Where(u => u.Id != authUser.Id).Where(u => u.EmailAddress == model.EmailAddress).AnyAsync(cancellationToken))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist bereits in Verwendung");
				return View(model);
			}

			authUser.EmailAddress = model.EmailAddress;
			authUser.FirstName = model.FirstName?.Trim();
			authUser.LastName = model.LastName?.Trim();

			if (!string.IsNullOrWhiteSpace(model.Password))
				authUser.PasswordHash = PasswordHelper.HashPassword(model.Password.Trim());

			try
			{
				await _dbContext.SaveChangesAsync(cancellationToken);
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Saving profile ({authUser.Id}) failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		#endregion Profile

		#region Login

		[HttpGet]
		[AllowAnonymous]
		public IActionResult Login(string returnUrl)
		{
			if (string.IsNullOrWhiteSpace(returnUrl))
				returnUrl = Url.Action("Index", "Trainings");

			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectReturnUrl(returnUrl);

			ViewData["Title"] = "Anmelden";
			return View(new AccountViewModel { ReturnUrl = returnUrl });
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(AccountViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectReturnUrl(model.ReturnUrl);

			ViewData["Title"] = "Anmelden";

			if (string.IsNullOrWhiteSpace(model.EmailAddress))
				ModelState.AddModelError(nameof(model.EmailAddress), "Eine E-Mail Adresse ist erforderlich");

			if (string.IsNullOrWhiteSpace(model.Password))
				ModelState.AddModelError(nameof(model.Password), "Ein Kennwort ist erforderlich");

			if (!ModelState.IsValid)
				return View(model);

			model.EmailAddress = model.EmailAddress.Trim().ToLower();
			model.Password = model.Password.Trim();

			var user = await _dbContext.Users
				.Where(u => u.IsEnabled)
				.Where(u => u.EmailAddress == model.EmailAddress)
				.FirstOrDefaultAsync(cancellationToken);
			if (user == null)
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist unbekannt");
				return View(model);
			}

			if (!PasswordHelper.VerifyPassword(model.Password, user.PasswordHash, out bool rehashNeeded))
			{
				ModelState.AddModelError(nameof(model.Password), "Das Kennwort ist nicht korrekt");
				return View(model);
			}

			if (rehashNeeded)
			{
				try
				{
					user.PasswordHash = PasswordHelper.HashPassword(model.Password);
					await _dbContext.SaveChangesAsync(cancellationToken);
				}
				catch
				{ }
			}

			await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user.GetPrincipal(), new AuthenticationProperties
			{
				AllowRefresh = true,
				IsPersistent = true
			});

			return RedirectReturnUrl(model.ReturnUrl);
		}

		[AllowAnonymous]
		public async Task<IActionResult> Logout(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return RedirectToAction("Index", "Trainings");

			if (authUser.ImpersonatingId.HasValue)
			{
				var user = await _dbContext.Users
					.Where(u => u.IsEnabled)
					.Where(u => u.Id == authUser.ImpersonatingId.Value)
					.FirstOrDefaultAsync(cancellationToken);

				if (user != null)
				{
					await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user.GetPrincipal(), new AuthenticationProperties
					{
						AllowRefresh = true,
						IsPersistent = true
					});

					return RedirectToAction("Index", "Users");
				}
			}

			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			return RedirectToAction("Index", "Trainings");
		}

		#endregion Login

		#region Lost Password

		[HttpGet]
		[AllowAnonymous]
		public IActionResult LostPassword(string returnUrl)
		{
			if (string.IsNullOrWhiteSpace(returnUrl))
				returnUrl = Url.Action("Index", "Trainings");

			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectReturnUrl(returnUrl);

			ViewData["Title"] = "Kennwort vergessen";
			return View(new AccountViewModel { ReturnUrl = returnUrl });
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> LostPassword(AccountViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectReturnUrl(model.ReturnUrl);

			ViewData["Title"] = "Kennwort vergessen";

			if (string.IsNullOrWhiteSpace(model.EmailAddress))
			{
				ModelState.AddModelError(nameof(model.EmailAddress), "Eine E-Mail Adresse ist erforderlich");
				return View(model);
			}

			model.EmailAddress = model.EmailAddress.Trim().ToLower();
			ViewData["IsSuccess"] = true;

			try
			{
				using var transaction = await _dbContext.BeginTransactionAsync(cancellationToken);

				var user = await _dbContext.Users
				.Where(u => u.EmailAddress == model.EmailAddress)
				.FirstOrDefaultAsync(cancellationToken);

				if (user == null)
					return View(model);

				string password = CryptographyHelper.GetRandomString(14);

				var sb = new StringBuilder();
				sb.AppendLine($"Hallo {user.GetName()},");
				sb.AppendLine();
				sb.AppendLine("Es wurde ein neues Kennwort angefordert.");
				sb.AppendLine("Das neue Kennwort wurde nun gesetzt und ist ab sofort gültig.");
				sb.AppendLine();
				sb.AppendLine($"Das neue Kennwort lautet: {password}");
				sb.AppendLine();
				sb.AppendLine("Sollte kein neues Kennwort angefordert worden sein, so wäre eine Meldung beim Administrator sinnvoll.");
				sb.AppendLine();

				var msg = new EmailMessage
				{
					Priority = MimeKit.MessageImportance.High,
					Subject = "Kennwort vergessen",
					Message = sb.ToString(),
					IsHtml = false,
				};
				msg.Recipients.Add(new EmailAddress { Name = user.GetName(), Address = user.EmailAddress });
				await _dbContext.EmailMessages.AddAsync(msg, cancellationToken);

				user.PasswordHash = PasswordHelper.HashPassword(password);

				await _dbContext.SaveChangesAsync(cancellationToken);
				await transaction.CommitAsync(cancellationToken);

				return View(new AccountViewModel { ReturnUrl = model.ReturnUrl });
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Resetting password failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		#endregion Lost Password

		private IActionResult RedirectReturnUrl(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
				return Redirect(returnUrl);

			return RedirectToAction("Index", "Trainings");
		}
	}
}
