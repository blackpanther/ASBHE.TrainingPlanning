﻿using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Services;
using Microsoft.AspNetCore.Mvc;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	public class TermsController : Controller
	{
		private readonly GlobalSettingsService _globalSettings;

		public TermsController(GlobalSettingsService globalSettings)
		{
			_globalSettings = globalSettings;
		}

		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			string terms = await _globalSettings.GetAsync("Terms", "", cancellationToken);
			ViewData["Title"] = "Teilnahmebedingungen";
			return View((object)terms);
		}
	}
}
