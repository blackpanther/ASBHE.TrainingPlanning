﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AMWD.ASBHE.TrainingPlanning.Database;
using AMWD.ASBHE.TrainingPlanning.Database.Entities;
using AMWD.ASBHE.TrainingPlanning.Extensions;
using AMWD.ASBHE.TrainingPlanning.Models;
using AMWD.ASBHE.TrainingPlanning.Security;
using AMWD.ASBHE.TrainingPlanning.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AMWD.ASBHE.TrainingPlanning.Controllers
{
	[Authorize]
	public class DepartmentsController : Controller
	{
		private readonly ILogger _logger;
		private readonly ServerDbContext _dbContext;
		private readonly ExtendedFakeFilterService _fakeFilterService;

		public DepartmentsController(ILogger<DepartmentsController> logger, ServerDbContext dbContext, ExtendedFakeFilterService fakeFilterService)
		{
			_logger = logger;
			_dbContext = dbContext;
			_fakeFilterService = fakeFilterService;
		}

		public async Task<IActionResult> Index(CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var model = new DepartmentViewModel
			{
				Departments = await _dbContext.Departments
					.OrderBy(d => d.IsDeleted)
					.ThenBy(d => d.Name)
					.ToListAsync(cancellationToken)
			};

			ViewData["Title"] = "Bereiche";
			return View(model);
		}

		public IActionResult Create()
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			ViewData["Title"] = "Neuer Bereich";
			return View(nameof(Edit), new DepartmentViewModel());
		}

		public async Task<IActionResult> Edit(int id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			var department = await _dbContext.Departments
				.Where(d => d.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (department == null)
				return NotFound();

			var model = new DepartmentViewModel
			{
				Id = department.Id,
				AddressAddition = department.AddressAddition,
				Contact = department.Contact,
				City = department.City,
				Country = department.Country,
				EmailAddress = department.EmailAddress,
				IsDeleted = department.IsDeleted,
				Name = department.Name,
				PhoneNumber = department.PhoneNumber,
				Street = department.Street,
				ZipCode = department.ZipCode
			};

			ViewData["Title"] = "Bereich bearbeiten";
			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(DepartmentViewModel model, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid();

			ViewData["Title"] = model.Id > 0 ? "Bereich bearbeiten" : "Neuer Bereich";

			if (string.IsNullOrWhiteSpace(model.Name))
				ModelState.AddModelError(nameof(model.Name), "Ein Name ist erforderlich");
			else
			{
				model.Name = model.Name.Trim();
				if (await _dbContext.Departments.Where(d => !d.IsDeleted).Where(d => d.Id != model.Id).Where(d => d.Name.ToLower() == model.Name.ToLower()).AnyAsync(cancellationToken))
					ModelState.AddModelError(nameof(model.Name), "Der Name ist bereits vorhanden");
			}

			if (!string.IsNullOrWhiteSpace(model.EmailAddress))
			{
				model.EmailAddress = model.EmailAddress.Trim().ToLower();

				if (!model.EmailAddress.IsValidEmailAddress())
					ModelState.AddModelError(nameof(model.EmailAddress), "Die E-Mail Adresse ist nicht gültig");
				else if (!await _fakeFilterService.IsEmailValid(model.EmailAddress, cancellationToken))
					ModelState.AddModelError(nameof(model.EmailAddress), "Der E-Mail Anbieter wird als unsicher eingestuft");
			}

			if (!ModelState.IsValid)
				return View(model);

			var department = await _dbContext.Departments
				.Where(d => d.Id == model.Id)
				.FirstOrDefaultAsync(cancellationToken);
			if (department == null)
			{
				department = new Department();
				await _dbContext.Departments.AddAsync(department, cancellationToken);
			}

			department.Name = model.Name;
			department.Contact = model.Contact?.Trim();
			department.PhoneNumber = model.PhoneNumber?.Trim();
			department.EmailAddress = model.EmailAddress;
			department.Street = model.Street?.Trim();
			department.AddressAddition = model.AddressAddition?.Trim();
			department.ZipCode = model.ZipCode?.Trim();
			department.City = model.City?.Trim();
			department.Country = model.Country?.Trim();
			department.IsDeleted = model.IsDeleted;

			try
			{
				await _dbContext.SaveChangesAsync(cancellationToken);
				return RedirectToAction(nameof(Index));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"Saving department failed: {ex.GetMessage()}");
				return StatusCode(StatusCodes.Status500InternalServerError);
			}
		}

		// This method is a REST style endpoint to provide additional functionality
		[HttpPost]
		public async Task<IActionResult> HardDelete(int id, CancellationToken cancellationToken)
		{
			var authUser = HttpContext.GetAuthUser();
			if (!authUser.IsAdmin)
				return Forbid("Admin permission required");

			var department = await _dbContext.Departments
				.Where(d => d.IsDeleted)
				.Where(d => d.Id == id)
				.FirstOrDefaultAsync(cancellationToken);
			if (department == null)
				return NotFound("No department with this id available to hard delete");

			int userCount = await _dbContext.Users
				.Where(u => u.DepartmentId == department.Id)
				.CountAsync(cancellationToken);
			if (userCount > 0)
				return BadRequest("Some users are still mapped to this department");

			int participantCount = await _dbContext.Participants
				.Where(p => p.DepartmentId == department.Id)
				.CountAsync(cancellationToken);
			if (participantCount > 0)
				return BadRequest("Some participants are still mapped to this department");

			try
			{
				_dbContext.Departments.Remove(department);
				await _dbContext.SaveChangesAsync(cancellationToken);

				_logger.LogWarning($"User #{authUser.Id} ({authUser.EmailAddress}) dropped department '{department.Name}'");
				return Ok();
			}
			catch (Exception ex)
			{
				return StatusCode(StatusCodes.Status500InternalServerError, ex.GetRecursiveMessage());
			}
		}
	}
}
