# Lehrgangsverwaltung

Lehrgänge planen und Teilnehmer verwalten.

1) Lehrgang wird festgelegt
2) Lehrgang wird freigegeben
3) Anmeldungen erfolgen
   - Der Teilnehmer wird über die Führungskraft angemeldet
   - Der Teilnehmer meldet sich selbst an, dann muss eine Führungskraft noch die Freigabe erteilen
4) Lehrgang wird geplant und Teilnehmer informiert

Dies soll durch das System abgebildet und unterstützt werden.

---

**LICENSE:** [MIT](LICENSE.txt)
